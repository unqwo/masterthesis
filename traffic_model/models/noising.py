import copy
import torch
import numpy as np

class Noiser4():
    def __init__(self, config):
        super(Noiser4, self).__init__()
        self.beta_min = config.sde.x.beta_min
        self.beta_max = config.sde.x.beta_max
        self.T = config.sde.x.num_scales
        self.sde = VPSDE(beta_min=self.beta_min, beta_max=self.beta_max, N=self.T)
        self.eps=1e-5
    
    def diffuse_into_noise(self, x_0_dict, edge_index_dict, edge_attr_0_dict, batch_dict):
        #count graphs in batch
        num_graphs = 0
        for node_type, value in batch_dict.items():
            num_graphs = max(num_graphs, value.max())       
        num_graphs +=1                                                                                         #(1)
        device = num_graphs.device
        #sample t and tmin1
        t = torch.floor(torch.rand(num_graphs, device=num_graphs.device)*self.T).long()       # for VPSDE aka GeoLDM
        tmin1 = torch.max(torch.zeros_like(t), t-1)                                                              # for VPSDE aka GeoLDM

        # Step i: noise X_0
        x_t_dict = copy.deepcopy(x_0_dict)
        self.std_x_dict = {}
        self.z_x_dict = {}
        for node_type, value in x_t_dict.items():
            z_x = torch.randn_like(value)
            t_b = t[batch_dict[node_type]]                                                                       #(bs*n, dx)
            std_x = self.sde.sqrt_1m_alphas_cumprod.to(device)[t_b]                                                              #(bs*n)
            x_t_dict[node_type] = self.sde.sqrt_alphas_cumprod.to(device)[t_b][:, None]*value + std_x[:, None] * z_x

            self.std_x_dict[node_type] = std_x
            self.z_x_dict[node_type] = z_x
        
        # Step ii: noise E_0
        edge_attr_t_dict = copy.deepcopy(edge_attr_0_dict)
        alpha_tmin_bar = {}    # for Step iii
        alpha_t_bar = {}    # for Step iii
        self.std_edge_attr_dict = {}
        self.z_edge_attr_dict = {}
        for edge_type, value in edge_attr_t_dict.items():
            batch_src = batch_dict[edge_type[0]]
            batch_edge = batch_src[edge_index_dict[edge_type][0]]

            z_edge_attr = torch.randn_like(value)
            t_b = t[batch_edge]
            std_edge_attr = self.sde.sqrt_1m_alphas_cumprod.to(device)[t_b]  
            edge_attr_t_dict[edge_type] = self.sde.sqrt_alphas_cumprod.to(device)[t_b][:, None] * value  + std_edge_attr[:, None] * z_edge_attr

            self.std_edge_attr_dict[edge_type] = std_edge_attr
            self.z_edge_attr_dict[edge_type] = z_edge_attr

            #for Step iii
            alpha_t_bar[edge_type] = self.sde.alphas_cumprod.to(device)[t_b]
            alpha_tmin_bar[edge_type] = self.sde.alphas_cumprod.to(device)[tmin1[batch_edge]]

        # Step iii: noise existence of edges, i.e. first sample A_tmin1, then A_t, then mask gone edges from A_tmin1 to A_t
        edge_index_t_dict = copy.deepcopy(edge_index_dict)
        A_t_dict = {}
        delta_Atmin1_to_At_dict = {}
        mask_active_nodes_dict = {}
        for edge_type, alpha_tmin_bar_value in alpha_tmin_bar.items():
            A_tmin1 = torch.bernoulli(alpha_tmin_bar_value).long().bool()     #(num_edges_0) 
            prob_tmin1_to_t = alpha_t_bar[edge_type]/alpha_tmin_bar_value
            A_t_prob = A_tmin1*prob_tmin1_to_t
            A_t = torch.bernoulli(A_t_prob).long().bool()     #(num_edges_0)
            
            A_t_dict[edge_type] = A_t.bool()
            delta_Atmin1_to_At_dict[edge_type] = (A_tmin1 ^ A_t).bool()

            edge_index_t_dict[edge_type]=edge_index_t_dict[edge_type][:,A_t.bool()]    #(2, num_edges_t)
            edge_attr_t_dict[edge_type]=edge_attr_t_dict[edge_type][A_t.bool()]        #(num_edges_t, de) , mask only existent edges in t

            src_active_index=torch.unique(edge_index_dict[edge_type][0][delta_Atmin1_to_At_dict[edge_type]])
            mask_src_active = torch.zeros(x_0_dict[edge_type[0]].shape[0], device=x_0_dict[edge_type[0]].device)
            mask_src_active[src_active_index] = 1
            dst_active_index=torch.unique(edge_index_dict[edge_type][1][delta_Atmin1_to_At_dict[edge_type]])
            mask_dst_active = torch.zeros(x_0_dict[edge_type[2]].shape[0], device=x_0_dict[edge_type[2]].device)
            mask_dst_active[dst_active_index] = 1

            mask_active_nodes_dict[edge_type] = [mask_src_active.squeeze().bool(), mask_dst_active.squeeze().bool()]
                    
        return x_t_dict, edge_index_t_dict, edge_attr_t_dict, A_t_dict, delta_Atmin1_to_At_dict, mask_active_nodes_dict, t/self.T

class VPSDE():
  def __init__(self, beta_min=0.1, beta_max=20, N=1000):
    """Construct a Variance Preserving SDE.
    Args:
      beta_min: value of beta(0)
      beta_max: value of beta(1)
      N: number of discretization steps
    """
    self.beta_0 = beta_min
    self.beta_1 = beta_max
    self.N = N
    self.discrete_betas = torch.linspace(beta_min / N, beta_max / N, N)     #schedule from EDGE
    self.alphas = 1. - self.discrete_betas
    self.alphas_cumprod = torch.cumprod(self.alphas, dim=0)
    #self.alphas_cumprod = self.cosine_beta_schedule(N)        #(N)--> changed
    self.sqrt_alphas_cumprod = torch.sqrt(self.alphas_cumprod)
    self.sqrt_1m_alphas_cumprod = torch.sqrt(1. - self.alphas_cumprod)

  

  def discretize(self, x, t, batch):
    """DDPM discretization."""
    timestep = (t * (self.N - 1) / self.T).long()
    beta = self.discrete_betas.to(x.device)[timestep]
    alpha = self.alphas.to(x.device)[timestep]
    sqrt_beta = torch.sqrt(beta)
    f = torch.sqrt(alpha[batch])[:, None] * x - x
    G = sqrt_beta
    return f, G

  
  def cosine_beta_schedule(timesteps, s=0.008, raise_to_power: float = 1):  #from GeoLDM
    """
    cosine schedule
    as proposed in https://openreview.net/forum?id=-NEXDKk8gZ
    """
    steps = timesteps + 2
    x = np.linspace(0, steps, steps)
    alphas_cumprod = np.cos(((x / steps) + s) / (1 + s) * np.pi * 0.5) ** 2
    alphas_cumprod = alphas_cumprod / alphas_cumprod[0]
    betas = 1 - (alphas_cumprod[1:] / alphas_cumprod[:-1])
    betas = np.clip(betas, a_min=0, a_max=0.999)
    alphas = 1. - betas
    alphas_cumprod = np.cumprod(alphas, axis=0)

    if raise_to_power != 1:
        alphas_cumprod = np.power(alphas_cumprod, raise_to_power)

    return alphas_cumprod
