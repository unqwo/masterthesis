from models.encoder import HGT_Transformer_enc
from models.decoder import HGT_Transformer_dec, HGT_Transformer_enc_dec
import torch
import torch.nn as nn
from torch_geometric.utils import coalesce, degree
import models.HGTE_layers as HGTE_layers
from models.noising import Noiser, Noiser3, Noiser4
from utils.misc import match_masking, count_occurence_node, tuple_to_string, string_to_tuple, insert_tensor_in_list


class Autoencoder(nn.Module):
    def __init__(self, config, in_dim_nodes, in_dim_edges, metadata):
        super(Autoencoder, self).__init__()
        self.encoder_hgt = HGT_Transformer_enc(config, in_dim_nodes, in_dim_edges, metadata=metadata)
        self.decoder_hgt = HGT_Transformer_dec(config, in_dim_nodes, in_dim_edges, metadata=metadata)
    
    def forward(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict):

        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict = self.encoder_hgt(x_dict, edge_index_dict, edge_attr_dict)

        gen_nodes_dict, gen_edge_index_dict, gen_edge_attr_dict = self.decoder_hgt(latent_nodes_dict, edge_index_dict, latent_edge_attr_dict)

        return gen_nodes_dict, edge_index_dict, gen_edge_attr_dict, ptr_dict, batch_dict

class Variationalautoencoder(nn.Module):
    def __init__(self, config, in_dim_nodes, in_dim_edges, metadata):
        super(Variationalautoencoder, self).__init__()
        self.encoder_hgt = HGT_Transformer_enc(config, in_dim_nodes, in_dim_edges, metadata=metadata)
        self.encoder_variational= HGTE_layers.Prior_Layer(config,dim=config.model.n_hid, metadata=metadata)
        self.decoder_hgt = HGT_Transformer_dec(config, in_dim_nodes, in_dim_edges, metadata=metadata)
    
    def forward(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict):
        # encoder
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict = self.encoder_hgt(x_dict, edge_index_dict, edge_attr_dict)
        # to mu, std - return new latents
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, *misc = self.encoder_variational(latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict)
        self.misc = misc
        # decoder
        gen_nodes_dict, gen_edge_index_dict, gen_edge_attr_dict = self.decoder_hgt(latent_nodes_dict, edge_index_dict, latent_edge_attr_dict)

        return gen_nodes_dict, edge_index_dict, gen_edge_attr_dict, ptr_dict, batch_dict
    
    def sample(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict):
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict = self.encoder_hgt(x_dict, edge_index_dict, edge_attr_dict)
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, *misc = self.encoder_variational(latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict)
        
        latent_nodes_dict = {node_type: torch.randn_like(value) for node_type, value in latent_nodes_dict.items()}
        latent_edge_attr_dict = {edge_type: torch.randn_like(value) for edge_type, value in latent_edge_attr_dict.items()}
        gen_nodes_dict, gen_edge_index_dict, gen_edge_attr_dict = self.decoder_hgt(latent_nodes_dict, edge_index_dict, latent_edge_attr_dict)
        return gen_nodes_dict, edge_index_dict, gen_edge_attr_dict, ptr_dict, batch_dict
        
class Latent_Variationalautoencoder(nn.Module):
    def __init__(self, config, in_dim_nodes, in_dim_edges, metadata):
        super(Latent_Variationalautoencoder, self).__init__()
        #in_dim_nodes_w_c = {k:v+config.model.dc_var for k,v in in_dim_nodes.items()}
        #in_dim_edges_w_c = {k:v+config.model.dc_var for k,v in in_dim_edges.items()}
        latent_dim = config.model.latent_dim

        #self.condition_emb = nn.Sequential(nn.Linear(config.data.condition_dim, config.model.dc_var),nn.LeakyReLU())
        self.encoder_hgt = HGT_Transformer_enc_dec(config, in_dim_nodes, in_dim_edges, latent_dim*2, latent_dim*2, metadata=metadata)
        #self.encoder_variational= HGTE_layers.Prior_Layer(config, latent_dim, metadata=metadata)
        self.decoder_hgt = HGT_Transformer_enc_dec(config, latent_dim, latent_dim, in_dim_nodes, in_dim_edges, metadata=metadata)
    
    def forward(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict, y):
        #c_embedded = self.condition_emb(y)
        #x_dict, edge_index_dict, edge_attr_dict = self.add_emb(x_dict, edge_index_dict, edge_attr_dict, batch_dict, c_embedded)
        
        first_val=next(iter(ptr_dict.values()))
        y = torch.zeros(first_val.size(0)-1, device=first_val.device)
        # encoder
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict = self.encoder_hgt(x_dict, edge_index_dict, edge_attr_dict, batch_dict, y)
        # to mu, std - return new latents
        #latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, *misc = self.encoder_variational(latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict)
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, *misc = self.sample_z(latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict)
        self.misc = misc
        # decoder
        gen_nodes_dict, gen_edge_index_dict, gen_edge_attr_dict = self.decoder_hgt(latent_nodes_dict, edge_index_dict, latent_edge_attr_dict, batch_dict, y)

        return gen_nodes_dict, edge_index_dict, gen_edge_attr_dict, ptr_dict, batch_dict

    def sample(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict, y):
        first_val=next(iter(ptr_dict.values()))
        y = torch.zeros(first_val.size(0)-1, device=first_val.device)
        
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict = self.encoder_hgt(x_dict, edge_index_dict, edge_attr_dict,batch_dict, y)
        #latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, *misc = self.encoder_variational(latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict)
        
        latent_nodes_dict = {node_type: torch.randn_like(value[:, :4]) for node_type, value in latent_nodes_dict.items()}
        latent_edge_attr_dict = {edge_type: torch.randn_like(value[:, :4]) for edge_type, value in latent_edge_attr_dict.items()}
        gen_nodes_dict, gen_edge_index_dict, gen_edge_attr_dict = self.decoder_hgt(latent_nodes_dict, edge_index_dict, latent_edge_attr_dict, batch_dict, y)
        return gen_nodes_dict, edge_index_dict, gen_edge_attr_dict, ptr_dict, batch_dict
    
    def sample_z(self, latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict):
        mu_nodes, logvar_nodes, mu_edges, logvar_edges={}, {}, {}, {}

        for node_type, value in latent_nodes_dict.items():
            mean, logvar = torch.chunk(value, 2, dim=1)
            logvar = torch.clamp(logvar, -30.0, 20.0)
            std_nodes = torch.exp(0.5 * logvar)
            eps = torch.randn_like(std_nodes)

            latent_nodes_dict[node_type] = mean + eps*std_nodes
            mu_nodes[node_type] = mean
            logvar_nodes[node_type] = logvar
        
        for edge_type, value in latent_edge_attr_dict.items():
            mean, logvar = torch.chunk(value, 2, dim=1)
            logvar = torch.clamp(logvar, -30.0, 20.0)
            std_edges = torch.exp(0.5 * logvar)
            eps = torch.randn_like(std_edges)

            latent_edge_attr_dict[edge_type] = mean + eps*std_edges
            mu_edges[edge_type] = mean
            logvar_edges[edge_type] = logvar

        misc = mu_nodes, logvar_nodes, mu_edges, logvar_edges

        return latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, misc

    def sample_mu(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict, y):

        first_val=next(iter(ptr_dict.values()))
        y = torch.zeros(first_val.size(0)-1, device=first_val.device)
        # encoder
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict = self.encoder_hgt(x_dict, edge_index_dict, edge_attr_dict, batch_dict, y)

        mu_nodes, logvar_nodes, mu_edges, logvar_edges={}, {}, {}, {}

        for node_type, value in latent_nodes_dict.items():
            mean, logvar = torch.chunk(value, 2, dim=1)
            logvar = torch.clamp(logvar, -30.0, 20.0)
            std_nodes = torch.exp(0.5 * logvar)
            eps = torch.zeros_like(std_nodes)

            latent_nodes_dict[node_type] = mean + eps*std_nodes
            mu_nodes[node_type] = mean
            logvar_nodes[node_type] = logvar
        
        for edge_type, value in latent_edge_attr_dict.items():
            mean, logvar = torch.chunk(value, 2, dim=1)
            logvar = torch.clamp(logvar, -30.0, 20.0)
            std_edges = torch.exp(0.5 * logvar)
            eps = torch.zeros_like(std_edges)

            latent_edge_attr_dict[edge_type] = mean + eps*std_edges
            mu_edges[edge_type] = mean
            logvar_edges[edge_type] = logvar

        misc = mu_nodes, logvar_nodes, mu_edges, logvar_edges

        return latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, misc
    
    def sample_mu_and_sig(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict, y):
        #c_embedded = self.condition_emb(y)
        #x_dict, edge_index_dict, edge_attr_dict = self.add_emb(x_dict, edge_index_dict, edge_attr_dict, batch_dict, c_embedded)
        
        first_val=next(iter(ptr_dict.values()))
        y = torch.zeros(first_val.size(0)-1, device=first_val.device)
        # encoder
        latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict = self.encoder_hgt(x_dict, edge_index_dict, edge_attr_dict, batch_dict, y)

        mu_nodes, logvar_nodes, mu_edges, logvar_edges={}, {}, {}, {}

        for node_type, value in latent_nodes_dict.items():
            mean, logvar = torch.chunk(value, 2, dim=1)
            logvar = torch.clamp(logvar, -30.0, 20.0)
            std_nodes = torch.exp(0.5 * logvar)
            eps = torch.randn_like(std_nodes)

            latent_nodes_dict[node_type] = mean + eps*std_nodes
            mu_nodes[node_type] = mean
            logvar_nodes[node_type] = logvar
        
        for edge_type, value in latent_edge_attr_dict.items():
            mean, logvar = torch.chunk(value, 2, dim=1)
            logvar = torch.clamp(logvar, -30.0, 20.0)
            std_edges = torch.exp(0.5 * logvar)
            eps = torch.randn_like(std_edges)

            latent_edge_attr_dict[edge_type] = mean + eps*std_edges
            mu_edges[edge_type] = mean
            logvar_edges[edge_type] = logvar

        misc = mu_nodes, logvar_nodes, mu_edges, logvar_edges

        return latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, misc

    def add_emb(self, x_dict, edge_index_dict, edge_attr_dict, batch_dict, embedding):
        x_emb_dict = {}
        edge_attr_emb_dict={}
        for node_type, value in x_dict.items():
            emb_batch = embedding[batch_dict[node_type]]
            x_emb_dict[node_type] = torch.cat([value, emb_batch], dim=1)
        
        for edge_type, value in edge_attr_dict.items():
            if edge_index_dict[edge_type].shape[1] > 0:
                batch_src = batch_dict[edge_type[0]]
                batch_edge = batch_src[edge_index_dict[edge_type][0]]
                emb_batch = embedding[batch_edge]
                edge_attr_emb_dict[edge_type] = torch.cat([value, emb_batch], dim=1)
            else:
                edge_attr_emb_dict[edge_type] = torch.cat([value, torch.empty((0,embedding.size(1)), device=value.device)], dim=1)
        
        return x_emb_dict, edge_index_dict, edge_attr_emb_dict
       

class Diffusionmodel(nn.Module):
    def __init__(self, config, in_dim_nodes, in_dim_edges, metadata):
        super(Diffusionmodel, self).__init__()
        self.config_m = config.model
        size_time_emb = config.model.size_time_emb
        dc = config.model.dc
        size_degree_emb = config.model.size_deg_emb     #len(metadata[1])*2
        self.condition_dim = config.data.condition_dim
        #self.condition_dim = len(metadata[0])*sum(in_dim_nodes.values())
        self.occurence_nodes = {node_type:count_occurence_node(node_type, metadata[1]) for node_type in metadata[0]}
        self.metadata=metadata
        self.rnn_layers = config.model.rnn.num_layers
        self.out_dim_nodes = in_dim_nodes
        self.out_dim_edges = in_dim_edges
        in_dim_nodes = {k:v+size_time_emb+dc+size_degree_emb for k,v in in_dim_nodes.items()} #for time embedding
        in_dim_edges = {k:v+size_time_emb+dc for k,v in in_dim_edges.items()}

        self.forward_diffuser = Noiser4(config)

        self.mlp_total_number_nodes = nn.Sequential(
            nn.Linear(dc, int(config.model.n_hid/2)),
            nn.LeakyReLU(),
            nn.Linear(int(config.model.n_hid/2), 2),
            nn.LeakyReLU()
        )

        self.mlp_number_nodes = nn.Sequential(
            nn.Linear(1+dc, config.model.n_hid),
            nn.LeakyReLU(),
            nn.Linear(config.model.n_hid, len(metadata[0])*2),
            nn.LeakyReLU()
        )

        self.degree_dist_rnn=nn.ModuleDict({node_type: HGTE_layers.RNN_Layer(config, self.occurence_nodes[node_type], self.rnn_layers ) for node_type in metadata[0]})
        self.degree_dist_condition_emb=nn.ModuleDict({node_type: nn.Sequential(nn.Linear(dc, self.occurence_nodes[node_type]),nn.LeakyReLU()) for node_type in metadata[0]})

        self.condition_emb_1 = nn.Sequential(nn.Linear(self.condition_dim, dc),nn.LeakyReLU())
        self.condition_emb_2 = nn.Sequential(nn.Linear(self.condition_dim, dc),nn.LeakyReLU())
        self.condition_emb_3 = nn.Sequential(nn.Linear(self.condition_dim, dc),nn.LeakyReLU())
        #self.condition_emb_4 = nn.Sequential(nn.Linear(self.condition_dim, dc),nn.LeakyReLU())
        self.time_emb = nn.Sequential(nn.Linear(1, size_time_emb),nn.LeakyReLU())
        self.degree_emb_1 = nn.Sequential(nn.Linear(len(metadata[1])*2*2, size_degree_emb), nn.LeakyReLU())
        self.degree_emb_2 = nn.Sequential(nn.Linear(len(metadata[1])*2*2, size_degree_emb), nn.LeakyReLU())

        self.first_mpnn = HGT_Transformer_enc_dec(config, in_dim_nodes, in_dim_edges, config.model.n_hid, config.model.n_hid, metadata=metadata)

        #self.mlp_active_nodes_src = nn.ModuleDict({tuple_to_string(edge_type): HGTE_layers.Node_Classification(config) for edge_type in metadata[1]})    # for every edgetype
        #self.mlp_active_nodes_dst = nn.ModuleDict({tuple_to_string(edge_type): HGTE_layers.Node_Classification(config) for edge_type in metadata[1]})    # for every edgetype

        self.mlp_link_prediction_src = nn.ModuleDict({tuple_to_string(edge_type): HGTE_layers.Node_MLP(config) for edge_type in metadata[1]})
        self.mlp_link_prediction_dst = nn.ModuleDict({tuple_to_string(edge_type): HGTE_layers.Node_MLP(config) for edge_type in metadata[1]})
        self.sigmoid = nn.Sigmoid()

        self.score_net = HGT_Transformer_enc_dec(config, in_dim_nodes, in_dim_edges, self.out_dim_nodes, self.out_dim_edges, metadata=metadata)
  
    def forward(self, x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict, y):
        self.device = x_dict[next(iter(x_dict))].device

        # Step1: diffuse into noise --> contains no learnable parameters
        x_t_dict, edge_index_t_dict, edge_attr_t_dict, A_t_dict, delta_Atmin1_to_At_dict, mask_active_nodes_dict, t = (
            self.forward_diffuser.diffuse_into_noise(x_dict, edge_index_dict, edge_attr_dict, batch_dict)
        )   # A_tmin1_dict and delta_Atmin1_to_At_dict are masks with respect to edge_index_dict

        #y = self.graph_aggregation(x_dict, ptr_dict, batch_dict)
        c_embedded = self.condition_emb_1(y)
        #c_embedded = torch.zeros_like(c_embedded)
        t_embedded = self.time_emb(t.unsqueeze(1))
        #----------------------------------------------------------rediffuse----------------------------------------
        #Step 2a: learn total number of nodes per graph
        bs = len(ptr_dict[next(iter(ptr_dict))])-1
        #c_embedded = torch.randn((bs, self.config_m.dc), device=self.device)
        n_total = self.mlp_total_number_nodes(c_embedded)   #mu and sigma

        # Step 2b: learn number of nodes
        num_nodes_diff = torch.stack(list(ptr_dict.values()), dim=1).diff(dim=0).float()    #(bs, num_nt)
        total_num_nodes = torch.sum(num_nodes_diff, dim=1)
        #total_num_nodes = (torch.sum(num_nodes_diff, dim=1)-911)/320
        learned_num_nodes = self.mlp_number_nodes(torch.cat([total_num_nodes.unsqueeze(1), c_embedded], dim=1))
        #learned_num_nodes = self.mlp_number_nodes(total_num_nodes.unsqueeze(1))
        pred_num_nodes_dict = {node_type: learned_num_nodes[:,2*i:(2*i+2)] for i, node_type in enumerate(ptr_dict.keys())}
        #pred_num_nodes_dict = {node_type: learned_num_nodes[:,i] for i, node_type in enumerate(ptr_dict.keys())}

        # Step2c learn degree distribution d_0 with input graph_size
        #c_embedded = self.condition_emb_4(y)
        num_nodes_diff={node_type: value.diff().float() for node_type, value in ptr_dict.items()}
        degree_dict_list = self.compute_degree_dist(num_nodes_diff)#, c_embedded)

        # Step3: perform first message passing layer
        t_embedded_pseudo = torch.zeros_like(t_embedded)
        x_emb_dict, edge_index_t_dict, edge_attr_emb_dict = self.add_emb(x_t_dict, edge_index_t_dict, edge_attr_t_dict, batch_dict, t_embedded_pseudo)
        c_embedded = self.condition_emb_2(y)
        x_emb_dict, edge_index_t_dict, edge_attr_emb_dict = self.add_emb(x_emb_dict, edge_index_t_dict, edge_attr_emb_dict, batch_dict, c_embedded)
        degree_0_dict_list = self.compute_d_0(edge_index_dict, ptr_dict)
        x_emb_dict = self.add_degree_emb(x_emb_dict, edge_index_t_dict, degree_0_dict_list, self.degree_emb_1)
        x_t_hat_dict,_,_ = self.first_mpnn(x_emb_dict, edge_index_t_dict, edge_attr_emb_dict, batch_dict, t)

        # Step4: link prediction only for active nodes, for every edge type
        # teacher forcing: use mask_active_nodes_dict instead of mask_pred_active_nodes_dict
        active_sum = {edge_type: [value[0].sum(), value[1].sum()] for edge_type, value in mask_active_nodes_dict.items()}                
        emerge_edge_index_dict, link_predictions_prob_dict = self.link_prediction(x_t_hat_dict, edge_index_dict, batch_dict, mask_active_nodes_dict)


        # Step5: initialize edge_attr of new edges with zero-vector
        # teacher forcing: use edge_index_dict masked by delta_Atmin1_to_At_dict instead of emerge_edge_index_dict
        updated_edge_index_dict={}
        updated_edge_attr_dict={}
        for edge_type, edge_index in edge_index_t_dict.items():
            emerge_edge_index = edge_index_dict[edge_type][:, delta_Atmin1_to_At_dict[edge_type]]      #teacher forcing
            emerge_edge_attr = torch.zeros((emerge_edge_index.shape[1], edge_attr_t_dict[edge_type].shape[1]), device=emerge_edge_index.device) #initialize with zero vector
            
            updated_edge_index_dict[edge_type] = torch.cat([edge_index, emerge_edge_index], dim=1)
            updated_edge_attr_dict[edge_type] = torch.cat([edge_attr_t_dict[edge_type], emerge_edge_attr], dim=0)

        # Step6: predict scores of X and edge_attr
        c_embedded = self.condition_emb_3(y)
        x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict = self.add_emb(x_t_dict, updated_edge_index_dict, updated_edge_attr_dict, batch_dict, t_embedded)
        x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict = self.add_emb(x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict, batch_dict, c_embedded)
        x_emb_dict = self.add_degree_emb(x_emb_dict, edge_index_t_dict, degree_0_dict_list, self.degree_emb_2)
        x_score_dict,_, edge_attr_score_dict = self.score_net(x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict, batch_dict, t)
        # -----------------------------------------------------done---------------------------------------------------------

        # for loss 
        self.misc = {}
        #self.misc['for loss_active'] = (mask_active_nodes_dict, active_prob_dict)
        self.misc['for loss_link_predicition'] = (edge_index_t_dict, A_t_dict, delta_Atmin1_to_At_dict, link_predictions_prob_dict)
        self.misc['for num_nodes_learning'] = pred_num_nodes_dict
        self.misc['for degree_dist_learning'] = degree_dict_list
        self.misc['for n_total'] = n_total

        return x_score_dict, updated_edge_index_dict, edge_attr_score_dict, ptr_dict, batch_dict

    def create_block_diag(self, features, ptr):
        blocks = [features[ptr[i]:ptr[i + 1]] for i in range(len(ptr) - 1)]
        return torch.block_diag(*blocks)

    def get_block_diag(self, node_type, x_t, mask, batch, layer):
        active_tensor = x_t[node_type][mask]        # during train -> teacher forcing
        active_batch = batch[node_type][mask]       #(num_active_nodes)

        _, active_counts = active_batch.unique(return_counts=True)          # counts how many nodes are active per graph    -> (active_graphs)


        active_ptr_new_index = torch.cat([torch.tensor([0]).to(active_counts.device),torch.cumsum(active_counts, dim=0)])

        active_prep = layer(active_tensor)
        block_diag_matrix = self.create_block_diag(active_prep, active_ptr_new_index)

        return block_diag_matrix

    def add_correct_offset(self, edge_idx, ptr_new_index, batch_new_index, ptr_old_index, batch_old_index):
        # add correct offset
        offset_new_index = ptr_new_index[:-1][batch_new_index[edge_idx]]
        edge_idx = edge_idx - offset_new_index   #shift new_edge indices back to without offsets

        offset_old_index = ptr_old_index[:-1][batch_old_index[edge_idx]]
        edge_idx = edge_idx + offset_old_index    #shift new_edge_idx with ptr of clean graph
        return edge_idx

    def map_correct_indices(self, edge_idx, mask):
        a = torch.ones_like(mask)
        a = torch.cumsum(a, dim=0) - 1
        old_idx = a[mask]
        edge_idx = old_idx[edge_idx]
        return edge_idx
    
    def check_graph_active_src_and_dst(self, batch_dict, mask_active_nodes_dict):
        for edge_type, mask_active_nodes in mask_active_nodes_dict.items():
            src = edge_type[0]
            dst = edge_type[2]
            active_src = batch_dict[src][mask_active_nodes[0]]
            active_dst = batch_dict[dst][mask_active_nodes[1]]
            active_graphs = torch.unique(active_src[torch.isin(active_src,active_dst)])       #indices for active graphs, i.e. src and dst contains active nodes

            active_graph_src = (batch_dict[src]+1)*mask_active_nodes[0].long()-1
            active_graph_dst = (batch_dict[dst]+1)*mask_active_nodes[1].long()-1

            mask_active_nodes[0] = torch.isin(active_graph_src, active_graphs)
            mask_active_nodes[1] = torch.isin(active_graph_dst, active_graphs)
        
        return mask_active_nodes_dict

    def link_prediction(self, x_t_hat_dict, edge_index_dict, batch_dict, mask_active_nodes_dict):
        emerge_edge_index_dict = {}
        link_predictions_prob_dict = {}
        mask_active_nodes_dict = self.check_graph_active_src_and_dst(batch_dict, mask_active_nodes_dict)
        for edge_type, src_layer in self.mlp_link_prediction_src.items():
            edge_type = string_to_tuple(edge_type)
            src = edge_type[0]
            dst = edge_type[2]

            src_block_diag_matrix = self.get_block_diag(
                src, x_t_hat_dict, mask_active_nodes_dict[edge_type][0], batch_dict, src_layer
                )
            
            dst_block_diag_matrix = self.get_block_diag(
                dst, x_t_hat_dict, mask_active_nodes_dict[edge_type][1], batch_dict, self.mlp_link_prediction_dst[tuple_to_string(edge_type)]
                )

            #perform 'matmul'
            if src_block_diag_matrix.size(1)!=0 and dst_block_diag_matrix.size(1)!=0:
                active_matrix = src_block_diag_matrix @ dst_block_diag_matrix.T
                link_predictions_prob = (self.sigmoid(active_matrix)-0.5)#*2
                link_predictions = torch.where(link_predictions_prob>0.3, link_predictions_prob+0.5, torch.zeros_like(link_predictions_prob))    #0.8 threshold
                link_predictions_prob2 = link_predictions
                link_predictions = torch.bernoulli(link_predictions_prob2)
                src_new_edge, dst_new_edge = link_predictions.nonzero(as_tuple=True)    #(num_pred_edges)

                # map new indices to old
                if src_new_edge.size(0) > 0:
                    src_new_edge = self.map_correct_indices(src_new_edge, mask_active_nodes_dict[edge_type][0])
                    dst_new_edge = self.map_correct_indices(dst_new_edge, mask_active_nodes_dict[edge_type][1])

                emerge_edge_index = torch.stack([src_new_edge, dst_new_edge], dim=0)
            
            else:
                emerge_edge_index = torch.empty((2,0), device=self.device)

            #check if already contained in edge_index_t
            check_mask = match_masking(edge_index_dict[edge_type], emerge_edge_index) #(num_emerge_edges)

            emerge_edge_index_dict[edge_type] = emerge_edge_index[:, ~check_mask].to(torch.int64)

            # for loss
            if src_block_diag_matrix.size(1)!=0 and dst_block_diag_matrix.size(1)!=0:
                nonzero_mask = link_predictions_prob != 0
                link_probs_flat = link_predictions_prob[nonzero_mask]   #1D-tensor with (num_nodes_src_active*num_nodes_dst_active*bs)
                src_new_edge, dst_new_edge = torch.where(nonzero_mask)
                link_probs_flat += 0.5

                # map new indices to old
                src_new_edge = self.map_correct_indices(src_new_edge, mask_active_nodes_dict[edge_type][0])
                dst_new_edge = self.map_correct_indices(dst_new_edge, mask_active_nodes_dict[edge_type][1])

                indices = torch.stack((src_new_edge, dst_new_edge), dim=0)
                link_predictions_prob_dict[edge_type] = (indices, link_probs_flat)
            else:
                link_predictions_prob_dict[edge_type] = (torch.empty((2,0), device=self.device), torch.empty((0), device=self.device))
        return  emerge_edge_index_dict, link_predictions_prob_dict
    
    def add_emb(self, x_t_dict, edge_index_t_dict, edge_attr_t_dict, batch_dict, embedding):
        x_emb_dict = {}
        edge_attr_emb_dict={}
        for node_type, value in x_t_dict.items():
            emb_batch = embedding[batch_dict[node_type]]
            x_emb_dict[node_type] = torch.cat([value, emb_batch], dim=1)
        
        for edge_type, value in edge_attr_t_dict.items():
            if edge_index_t_dict[edge_type].shape[1] > 0:
                batch_src = batch_dict[edge_type[0]]
                batch_edge = batch_src[edge_index_t_dict[edge_type][0]]
                emb_batch = embedding[batch_edge]
                edge_attr_emb_dict[edge_type] = torch.cat([value, emb_batch], dim=1)
            else:
                edge_attr_emb_dict[edge_type] = torch.cat([value, torch.empty((0,embedding.size(1)), device=value.device)], dim=1)
        
        return x_emb_dict, edge_index_t_dict, edge_attr_emb_dict
    
    
    def add_degree_emb(self, x_dict, edge_index_dict, degree_0_dict_list, layer):      
        for node_type, value in x_dict.items():
            deg_emb = torch.tensor([]).to(value.device).view(value.shape[0],0)
            for edge_type, edge_index in edge_index_dict.items():
                deg1=torch.tensor([]).to(value.device)
                deg2=torch.tensor([]).to(value.device)
                src = edge_type[0]
                dst = edge_type[2]

                if src == node_type:
                    deg1=degree(edge_index[0], num_nodes=value.shape[0])
                    deg1=torch.stack([deg1, degree_0_dict_list[edge_type][0]], dim=1)
                else:
                    deg1=torch.zeros((value.shape[0], 2)).to(value.device)
                if dst == node_type:
                    deg2=degree(edge_index[1], num_nodes=value.shape[0])
                    deg2=torch.stack([deg2, degree_0_dict_list[edge_type][1]], dim=1)
                else:
                    deg2=torch.zeros((value.shape[0], 2)).to(value.device)
                
                deg_emb = torch.cat([deg_emb, deg1, deg2], dim=1)
            
            deg_emb = layer(torch.log1p(deg_emb))
            x_dict[node_type] = torch.cat([x_dict[node_type], deg_emb], dim=1)

        return x_dict

    def compute_degree_dist(self, num_nodes_diff):#, c_embedded):
        num_nodes_diff = torch.stack([v for v in num_nodes_diff.values()], dim=1)
        h_init = int((self.rnn_layers-2))
        degree_dict_list = {edge_type:[torch.empty(0), torch.empty(0)] for edge_type in self.metadata[1]}
        max_nodes=num_nodes_diff.max(dim=0)[0]          #(num_nt)
        bs = num_nodes_diff.shape[0]
        h_node_type = torch.zeros((h_init, bs), device=self.device)
        make_it_random = torch.randn((2, bs), device=self.device)
        h_node_type = torch.cat([h_node_type, make_it_random], dim=0)  #(num_layers, bs, occurence)
        for i, node_type in enumerate(self.metadata[0]):
            occurence_node = self.occurence_nodes[node_type]
            h_node_type = h_node_type.unsqueeze(2).repeat(1, 1, occurence_node)+1e-5 if i==0 else h_node_type.sum(dim=-1).unsqueeze(2).repeat(1, 1, occurence_node)
            init = torch.ones((max_nodes[i].int(), bs), device=self.device).cumsum(dim=0)
            mask = (init <= num_nodes_diff[:,i]).T    #(bs, max_nodes)
            init_deg_node = torch.zeros((bs,max_nodes[i].int(),occurence_node), device=self.device)   #(bs, max_nodes, occurence)
            init_deg_node = init_deg_node + num_nodes_diff[:,i].unsqueeze(1).unsqueeze(2).repeat(1, max_nodes[i].int(), occurence_node)/10000

            #c_embedded_for_rnn = self.degree_dist_condition_emb[node_type](c_embedded)
            init_deg_node = init_deg_node #+ c_embedded_for_rnn.unsqueeze(1).repeat(1, max_nodes[i].int(), 1)
            pred_degree_node, h_node_type = self.degree_dist_rnn[node_type](init_deg_node, h_node_type)
            
            full_degree_node = pred_degree_node.reshape(-1,occurence_node)      #(bs*max_nodes, occurence)
            masked_degree_node = full_degree_node[mask.flatten()]
            degree_dict_list = insert_tensor_in_list(node_type, masked_degree_node, degree_dict_list)
        return degree_dict_list

    def compute_d_0(self, edge_index_0_dict, ptr_dict):
        degree_0_dict_list = {edge_type:[torch.empty(0), torch.empty(0)] for edge_type in edge_index_0_dict.keys()}
        for node_type, ptr in ptr_dict.items():
            deg_list = []
            for edge_type, edge_index in edge_index_0_dict.items():
                if node_type==edge_type[0]:
                    deg_list.append(degree(edge_index[0], num_nodes=ptr.max()))
                if node_type==edge_type[2]:
                    deg_list.append(degree(edge_index[1], num_nodes=ptr.max()))
            deg_ten = torch.stack(deg_list, dim=1)
            degree_0_dict_list = insert_tensor_in_list(node_type, deg_ten, degree_0_dict_list)
        return degree_0_dict_list
    
    def graph_aggregation(self, x_dict, ptr_dict, batch_dict):
        first_node_type = next(iter(x_dict))
        batch_size = ptr_dict[first_node_type].size(0) - 1  
        embedding_size = x_dict[first_node_type].size(1)      
        aggregated_embeddings = []

        for node_type, x in x_dict.items():
            ptr = ptr_dict[node_type]  

            # batch_indices = torch.repeat_interleave(torch.arange(batch_size, device=x.device), ptr[1:] - ptr[:-1])
            # batch_embeddings_type = torch.zeros((batch_size, embedding_size), device=x.device)
            # batch_embeddings_type.scatter_add_(0, batch_indices.unsqueeze(-1).expand(-1, embedding_size), x)

            summed_emb = torch.zeros(batch_size, x.size(1), device=x.device).scatter_add(0, batch_dict[node_type].unsqueeze(1).expand_as(x), x)
            # sq_summed_emb = torch.zeros(batch_size, x.size(1), device=x.device).scatter_add(0, batch_dict[node_type].unsqueeze(1).expand_as(x) , x**2)
            node_counts = (ptr[1:] - ptr[:-1]).float().unsqueeze(1)  # (bs, 1)
            summed_emb /= node_counts
            # sq_summed_emb /= node_counts
            # std_summed_emb = torch.sqrt(sq_summed_emb - (summed_emb)**2)
            # emb = torch.stack((torch.sum(summed_emb, dim=1)/x.size(1), torch.sqrt(torch.var(summed_emb, dim=1)), torch.sum(std_summed_emb, dim=1)/x.size(1), torch.sqrt(torch.var(std_summed_emb, dim=1))), dim=1)
            emb = (summed_emb - summed_emb.mean(dim=0, keepdim=True))/summed_emb.std(dim=0, keepdim=True)
            aggregated_embeddings.append(emb)
            
        return torch.cat(aggregated_embeddings, dim=1)
            