import math
from typing import Any, Dict, List, Optional, Tuple, Union

import torch
import torch.nn as nn
from torch import Tensor
from torch.nn import Parameter
from torch.nn import functional as F

from torch_geometric.nn.conv import MessagePassing
from torch_geometric.nn.dense import HeteroDictLinear, HeteroLinear, Linear
from torch_geometric.nn import LayerNorm
from torch_geometric.nn.inits import ones
from torch_geometric.nn.parameter_dict import ParameterDict
from torch_geometric.typing import Adj, EdgeType, Metadata, NodeType
from torch_geometric.utils import softmax, coalesce
from torch_geometric.utils.hetero import construct_bipartite_edge_index

import torch_geometric.backend
import torch_geometric.typing
from torch_geometric import is_compiling
from torch_geometric.typing import pyg_lib


class HGTConv(MessagePassing):
    def __init__(
        self,
        in_channels: Union[int, Dict[str, int]],
        out_channels: int,
        metadata: Metadata,
        heads: int = 1,
        **kwargs,
    ):
        super().__init__(aggr='add', node_dim=0, **kwargs)

        if out_channels % heads != 0:
            raise ValueError(f"'out_channels' (got {out_channels}) must be "
                             f"divisible by the number of heads (got {heads})")

        if not isinstance(in_channels, dict):
            in_channels = {node_type: in_channels for node_type in metadata[0]}
        s = 1
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.heads = heads
        self.node_types = metadata[0]
        self.edge_types = metadata[1]
        self.edge_types_map = {
            edge_type: i
            for i, edge_type in enumerate(metadata[1])
        }

        self.dst_node_types = {key[-1] for key in self.edge_types}

        self.kqv_lin = HeteroDictLinear(self.in_channels,
                                        self.out_channels * 3)

        self.out_node_lin = HeteroDictLinear(self.out_channels, self.out_channels, types=self.node_types)
        self.out_edge_attr_lin = HeteroDictLinearEdges(self.out_channels, self.out_channels, types=self.edge_types)

        dim = out_channels // heads
        num_types = heads * len(self.edge_types)

        self.k_rel = HeteroLinear(dim, dim, num_types, bias=False,
                                  is_sorted=True)
        self.v_rel = HeteroLinear(dim, dim, num_types, bias=False,
                                  is_sorted=True)

        self.skip = ParameterDict({
            node_type: Parameter(torch.empty(1))
            for node_type in self.node_types
        })

        # self.p_rel = ParameterDict()
        # for edge_type in self.edge_types:
        #     edge_type = '__'.join(edge_type)
        #     self.p_rel[edge_type] = Parameter(torch.empty(1, heads))
        
        # FiLM y to E
        self.y_e_mul = nn.Linear(self.out_channels, self.out_channels)     
        self.y_e_add = nn.Linear(self.out_channels, self.out_channels)

        # FiLM y to X
        self.y_x_mul = nn.Linear(self.out_channels, self.out_channels)
        self.y_x_add = nn.Linear(self.out_channels, self.out_channels)

        # Process y
        self.y_y = nn.Linear(self.out_channels, self.out_channels)
        self.x_y = FeaturetoyHetero(self.out_channels, self.out_channels, types=self.node_types)
        self.e_y = FeaturetoyHetero(self.out_channels, self.out_channels, types=self.edge_types)
        self.y_out = nn.Linear(self.out_channels, self.out_channels)

        # Feed Forward
        self.nodes_ln_1 = LayerDictNorm({nodetype: out_channels for nodetype in self.node_types})
        self.nodes_ffn_up = HeteroDictLinear(self.out_channels, self.out_channels * 2, types=self.node_types)
        self.nodes_ffn_down = HeteroDictLinear(self.out_channels * 2, self.out_channels, types=self.node_types)
        self.nodes_ln_2 = LayerDictNorm({nodetype: out_channels for nodetype in self.node_types})

        self.edges_ln_1 = LayerDictNormEdges({edgetype: out_channels for edgetype in self.edge_types})
        self.edges_ffn_up = HeteroDictLinearEdges(self.out_channels, self.out_channels * 2, types=self.edge_types)
        self.edges_ffn_down = HeteroDictLinearEdges(self.out_channels * 2, self.out_channels, types=self.edge_types)
        self.edges_ln_2 = LayerDictNormEdges({edgetype: out_channels for edgetype in self.edge_types})

        self.norm_y1 = nn.LayerNorm(self.out_channels, eps=1e-05)
        self.lin_y1 = nn.Linear(self.out_channels, self.out_channels*2)
        self.lin_y2 = nn.Linear(self.out_channels*2, self.out_channels)
        self.norm_y2 = nn.LayerNorm(self.out_channels, eps=1e-05)

        self.reset_parameters()
    
    def reset_parameters(self):
        super().reset_parameters()
        self.kqv_lin.reset_parameters()
        self.out_node_lin.reset_parameters()
        self.k_rel.reset_parameters()
        self.v_rel.reset_parameters()
        ones(self.skip)
        #ones(self.p_rel)


    def _cat(self, x_dict: Dict[str, Tensor]) -> Tuple[Tensor, Dict[str, int]]:
        """Concatenates a dictionary of features."""
        cumsum = 0
        outs: List[Tensor] = []
        offset: Dict[str, int] = {}
        for key, x in x_dict.items():
            outs.append(x)
            offset[key] = cumsum
            cumsum += x.size(0)
        return torch.cat(outs, dim=0), offset

    def _construct_src_node_feat(
        self, k_dict: Dict[str, Tensor], v_dict: Dict[str, Tensor],
        edge_index_dict: Dict[EdgeType, Adj]
    ) -> Tuple[Tensor, Tensor, Dict[EdgeType, int]]:
        """Constructs the source node representations."""
        cumsum = 0
        num_edge_types = len(self.edge_types)
        H, D = self.heads, self.out_channels // self.heads

        # Flatten into a single tensor with shape [num_edge_types * heads, D]:
        ks: List[Tensor] = []
        vs: List[Tensor] = []
        type_list: List[Tensor] = []
        offset: Dict[EdgeType] = {}
        for edge_type in edge_index_dict.keys():
            src = edge_type[0]
            N = k_dict[src].size(0)
            offset[edge_type] = cumsum
            cumsum += N

            # construct type_vec for curr edge_type with shape [H, D]
            edge_type_offset = self.edge_types_map[edge_type]
            type_vec = torch.arange(H, dtype=torch.long).view(-1, 1).repeat(
                1, N) * num_edge_types + edge_type_offset

            type_list.append(type_vec)
            ks.append(k_dict[src])
            vs.append(v_dict[src])

        ks = torch.cat(ks, dim=0).transpose(0, 1).reshape(-1, D)
        vs = torch.cat(vs, dim=0).transpose(0, 1).reshape(-1, D)
        type_vec = torch.cat(type_list, dim=1).flatten()

        k = self.k_rel(ks, type_vec).view(H, -1, D).transpose(0, 1)
        v = self.v_rel(vs, type_vec).view(H, -1, D).transpose(0, 1)

        return k, v, offset

    def forward(
        self,
        x_dict: Dict[NodeType, Tensor],
        edge_index_dict: Dict[EdgeType, Adj],
        edge_attr_dict: Dict[EdgeType, Tensor],
        batch_dict: Dict[NodeType, Tensor],
        y: Tensor,
    ) -> Dict[NodeType, Optional[Tensor]]:
        F = self.out_channels
        H = self.heads
        D = F // H

        k_dict, q_dict, v_dict, out_node_dict, out_edge_attr_dict= {}, {}, {}, {}, {}

        # Compute K, Q, V over node types:
        kqv_dict = self.kqv_lin(x_dict)
        for key, val in kqv_dict.items():
            k, q, v = torch.tensor_split(val, 3, dim=1)
            k_dict[key] = k.view(-1, H, D)
            q_dict[key] = q.view(-1, H, D)
            v_dict[key] = v.view(-1, H, D)

        q, dst_offset = self._cat(q_dict)
        k, v, src_offset = self._construct_src_node_feat(
            k_dict, v_dict, edge_index_dict)

        edge_index, edge_attr = construct_bipartite_edge_index(
            edge_index_dict, src_offset, dst_offset, edge_attr_dict=edge_attr_dict,
            num_nodes=k.size(0))            # add offset, that each index is unique and node_type claims a specific index range
        edge_attr = edge_attr.view(-1, H, D)

        num_edges = 0
        edge_offset={}
        for edge_type, tensor_idx in edge_index_dict.items():
            edge_offset[edge_type] = num_edges
            num_edges += tensor_idx.shape[1]
        
        do_msg_pass = edge_index.size(1) > 0
        out = self.propagate(edge_index, k=k, q=q, v=v, edge_attr=edge_attr) if do_msg_pass else None
        
        ### actual update method here (...outsourced)
        # Reconstruct output node embeddings dict & edge embeddings:
        if do_msg_pass:
            for node_type, start_offset in dst_offset.items():
                end_offset = start_offset + q_dict[node_type].size(0)
                if node_type in self.dst_node_types:
                    out_node_dict[node_type] = out[start_offset:end_offset]
            
            for edge_type, start_offset in edge_offset.items():
                end_offset = start_offset + edge_attr_dict[edge_type].size(0)
                if edge_type in self.edge_types:
                    out_edge_attr_dict[edge_type] = self.w_hat[start_offset:end_offset]

            # Transform output node embeddings & output edge embeddings:
            a_dict = self.out_node_lin({
                k:
                nn.functional.gelu(v) if v is not None else v
                for k, v in out_node_dict.items()
            })

            b_dict = self.out_edge_attr_lin({
                k:
                nn.functional.gelu(v) if v is not None else v
                for k, v in out_edge_attr_dict.items()
            })
        else: 
            a_dict = {node_type: torch.zeros_like(x) for node_type, x in x_dict.items()}
            b_dict = {edge_type: torch.zeros_like(edge_attr) for edge_type, edge_attr in edge_attr_dict.items()}
        
        if y is not None:
            # #Incorporate globale features
            yx1 = self.y_x_add(y)
            yx2 = self.y_x_mul(y)
            a_dict = {key: yx1[batch_dict[key]] + (yx2[batch_dict[key]] + 1)*value for key, value in a_dict.items()}

            batch_dict_edges = {edge_type: batch_dict[edge_type[0]][edge_index_dict[edge_type][0]] for edge_type in self.edge_types}
            ye1 = self.y_e_add(y)
            ye2 = self.y_e_mul(y)
            b_dict = {key: ye1[batch_dict_edges[key]] + (ye2[batch_dict_edges[key]] + 1)*value for key, value in b_dict.items()}

            #update y
            bs = y.size(0)
            y_y = self.y_y(y)
            e_y = self.e_y(edge_attr_dict, batch_dict_edges, bs)
            x_y = self.x_y(x_dict, batch_dict, bs)
            new_y = y_y + x_y + e_y
            new_y = self.y_out(new_y)

        if False:   # Version HGTConv in pytorch geometric
            # Iterate over node types:
            for node_type, out in out_node_dict.items():
                out = a_dict[node_type]

                if out.size(-1) == x_dict[node_type].size(-1):
                    alpha = self.skip[node_type].sigmoid()
                    out = alpha * out + (1 - alpha) * x_dict[node_type]
                out_node_dict[node_type] = out
        
        else:       # adapted version according to graph transformer Dwivedi
            ### nodes
            # Norm
            h_hat_hat_dict = {}
            for node_type, out in a_dict.items():
                out = out + x_dict[node_type]
                h_hat_hat_dict[node_type] = out
            h_hat_hat_dict = self.nodes_ln_1(h_hat_hat_dict)

            # FFN
            h_hat_hat_hat_dict = self.nodes_ffn_down(relu_dict(self.nodes_ffn_up(h_hat_hat_dict)))

            # Norm
            out_node_dict = {}
            for node_type, value in h_hat_hat_hat_dict.items():
                value = value + h_hat_hat_dict[node_type]
                out_node_dict[node_type] = value
            out_node_dict = self.nodes_ln_2(out_node_dict)

            ### edges
            # Norm
            e_hat_hat_dict = {}
            for edge_type, out in b_dict.items():
                out = out + edge_attr_dict[edge_type]
                e_hat_hat_dict[edge_type] = out
            e_hat_hat_dict = self.edges_ln_1(e_hat_hat_dict)

            # FFN
            e_hat_hat_hat_dict = self.edges_ffn_down(relu_dict(self.edges_ffn_up(e_hat_hat_dict)))
                
            # Norm
            out_edge_attr_dict = {}
            for edge_type, value in e_hat_hat_hat_dict.items():
                value = value + e_hat_hat_dict[edge_type]
                out_edge_attr_dict[edge_type] = value
            out_edge_attr_dict = self.edges_ln_2(out_edge_attr_dict)

            if y is not None:
                ### gloabl feature
                y = self.norm_y1(y + new_y)
                ff_output_y = self.lin_y2(torch.relu(self.lin_y1(y)))
                y = self.norm_y2(y + ff_output_y)

        return out_node_dict, out_edge_attr_dict, y


    def message(self, k_j: Tensor, q_i: Tensor, v_j: Tensor, edge_attr: Tensor,
                index: Tensor, ptr: Optional[Tensor],
                size_i: Optional[int]) -> Tensor:
        if False: # HGTConv as in pytorch geometric
            alpha = (q_i * k_j).sum(dim=-1) * edge_attr
            alpha = alpha / math.sqrt(q_i.size(-1))
            alpha = softmax(alpha, index, ptr, size_i)
            out = v_j * alpha.view(-1, self.heads, 1)
        else:     # adapted version
            alpha = (q_i * k_j) * edge_attr
            alpha = alpha / math.sqrt(q_i.size(-1))
            alpha = alpha.view(-1, self.out_channels)
            self.w_hat = alpha
            alpha = softmax(alpha, index, ptr, size_i)
            alpha = alpha.view(-1, self.heads, self.out_channels//self.heads)
            out = v_j * alpha
        return out.view(-1, self.out_channels)
    

    def __repr__(self) -> str:
        return (f'{self.__class__.__name__}(-1, {self.out_channels}, '
                f'heads={self.heads})')

class LayerDictNorm(nn.Module):
    def __init__(self, in_channels):
        super(LayerDictNorm, self).__init__()

        self.lns = nn.ModuleDict({
            key:
            nn.LayerNorm(channels)
            for key, channels in in_channels.items()
        })
    
    def forward(self, x_dict):
        out_node_dict = {}
        for key, ln in self.lns.items():
            if key in x_dict:
                out_node_dict[key] = ln(x_dict[key])
        return out_node_dict

class LayerDictNormEdges(nn.Module):
    def __init__(self, in_channels):
        super(LayerDictNormEdges, self).__init__()

        self.lns = nn.ModuleDict({
            '__'.join(key):
            nn.LayerNorm(channels)
            for key, channels in in_channels.items()
        })
    
    def forward(self, x_dict):
        out_dict = {}
        for key, ln in self.lns.items():
            key = tuple(key.split('__'))
            if key in x_dict:
                out_dict[key] = ln(x_dict[key])
        return out_dict

def relu_dict(x_dict):
    for key, v in x_dict.items():
        x_dict[key] = F.leaky_relu(v)
    return x_dict

class HeteroDictLinearEdges(nn.Module):
    def __init__(
        self,
        in_channels: Union[int, Dict[Any, int]],
        out_channels: int,
        types: Optional[Any] = None,
        **kwargs,
    ):
        super(HeteroDictLinearEdges, self).__init__()

        if isinstance(in_channels, dict):
            self.types = list(in_channels.keys())

            if any([i == -1 for i in in_channels.values()]):
                self._hook = self.register_forward_pre_hook(
                    self.initialize_parameters)

            if types is not None and set(self.types) != set(types):
                raise ValueError("The provided 'types' do not match with the "
                                 "keys in the 'in_channels' dictionary")

        else:
            if types is None:
                raise ValueError("Please provide a list of 'types' if passing "
                                 "'in_channels' as an integer")

            if in_channels == -1:
                self._hook = self.register_forward_pre_hook(
                    self.initialize_parameters)

            self.types = types
            in_channels = {edge_type: in_channels for edge_type in types}

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kwargs = kwargs

        self.lins = nn.ModuleDict({
            '__'.join(key):
            Linear(channels, self.out_channels, **kwargs)
            for key, channels in self.in_channels.items()
        })

        self.reset_parameters()
    
    def forward(self, x_dict: Dict[EdgeType, Tensor]) -> Dict[EdgeType, Tensor]:
        out_dict = {}

        # Only apply fused kernel for more than 10 types, otherwise use
        # sequential computation (which is generally faster for these cases).
        use_segment_matmul = torch_geometric.backend.use_segment_matmul
        if use_segment_matmul is None:
            use_segment_matmul = len(x_dict) >= 10

        if (use_segment_matmul and torch_geometric.typing.WITH_GMM
                and not is_compiling() and not torch.jit.is_scripting()):
            xs, weights, biases = [], [], []
            for key, lin in self.lins.items():
                if key in x_dict:
                    xs.append(x_dict[key])
                    weights.append(lin.weight.t())
                    biases.append(lin.bias)
            biases = None if biases[0] is None else biases
            outs = pyg_lib.ops.grouped_matmul(xs, weights, biases)
            for key, out in zip(x_dict.keys(), outs):
                if key in x_dict:
                    out_dict[key] = out
        else:
            for key, lin in self.lins.items():
                key = tuple(key.split('__'))
                if key in x_dict:
                    out_dict[key] = lin(x_dict[key])

        return out_dict
    
    def reset_parameters(self):
        r"""Resets all learnable parameters of the module."""
        for lin in self.lins.values():
            lin.reset_parameters()
    
    @torch.no_grad()
    def initialize_parameters(self, module, input):
        for key, x in input[0].items():
            lin = self.lins[key]
            if self.is_uninitialized_parameter(lin.weight):
                self.lins[key].initialize_parameters(None, x)
        self.reset_parameters()
        self._hook.remove()
        self.in_channels = {key: x.size(-1) for key, x in input[0].items()}
        delattr(self, '_hook')

    def __repr__(self) -> str:
        return (f'{self.__class__.__name__}({self.in_channels}, '
                f'{self.out_channels}, bias={self.kwargs.get("bias", True)})')
    
    def is_uninitialized_parameter(x: Any) -> bool:
        if not hasattr(nn.parameter, 'UninitializedParameter'):
            return False
        return isinstance(x, nn.parameter.UninitializedParameter)

class HeteroDictLinearReconstruct(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: Union[int, Dict[Any, int]],
        types: Optional[Any] = None,
        **kwargs,
    ):
        super(HeteroDictLinearReconstruct, self).__init__()

        if isinstance(out_channels, dict):
            self.types = list(out_channels.keys())

            if any([i == -1 for i in out_channels.values()]):
                self._hook = self.register_forward_pre_hook(
                    self.initialize_parameters)

            if types is not None and set(self.types) != set(types):
                raise ValueError("The provided 'types' do not match with the "
                                 "keys in the 'out_channels' dictionary")

        else:
            if types is None:
                raise ValueError("Please provide a list of 'types' if passing "
                                 "'out_channels' as an integer")

            if out_channels == -1:
                self._hook = self.register_forward_pre_hook(
                    self.initialize_parameters)

            self.types = types
            out_channels = {node_type: out_channels for node_type in types}

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kwargs = kwargs

        self.lins = nn.ModuleDict({
            '__'.join(key) if isinstance(key, Tuple) else key:
            Linear(self.in_channels, channels, **kwargs)
            for key, channels in self.out_channels.items()
        })

        self.reset_parameters()
    
    def forward(self, x_dict: Dict[Any, Tensor]) -> Dict[Any, Tensor]:
        out_dict = {}

        # Only apply fused kernel for more than 10 types, otherwise use
        # sequential computation (which is generally faster for these cases).
        use_segment_matmul = torch_geometric.backend.use_segment_matmul
        if use_segment_matmul is None:
            use_segment_matmul = len(x_dict) >= 10

        if (use_segment_matmul and torch_geometric.typing.WITH_GMM
                and not is_compiling() and not torch.jit.is_scripting()):
            xs, weights, biases = [], [], []
            for key, lin in self.lins.items():
                if key in x_dict:
                    xs.append(x_dict[key])
                    weights.append(lin.weight.t())
                    biases.append(lin.bias)
            biases = None if biases[0] is None else biases
            outs = pyg_lib.ops.grouped_matmul(xs, weights, biases)
            for key, out in zip(x_dict.keys(), outs):
                if key in x_dict:
                    out_dict[key] = out
        else:
            for key, lin in self.lins.items():
                key = tuple(key.split('__')) if '__' in key else key
                if key in x_dict:
                    out_dict[key] = lin(x_dict[key])

        return out_dict
    
    
    def reset_parameters(self):
        r"""Resets all learnable parameters of the module."""
        for lin in self.lins.values():
            lin.reset_parameters()
    
    @torch.no_grad()
    def initialize_parameters(self, module, input):
        for key, x in input[0].items():
            lin = self.lins[key]
            if self.is_uninitialized_parameter(lin.weight):
                self.lins[key].initialize_parameters(None, x)
        self.reset_parameters()
        self._hook.remove()
        self.in_channels = {key: x.size(-1) for key, x in input[0].items()}
        delattr(self, '_hook')

    def __repr__(self) -> str:
        return (f'{self.__class__.__name__}({self.in_channels}, '
                f'{self.out_channels}, bias={self.kwargs.get("bias", True)})')
    
    def is_uninitialized_parameter(x: Any) -> bool:
        if not hasattr(nn.parameter, 'UninitializedParameter'):
            return False
        return isinstance(x, nn.parameter.UninitializedParameter)

class HeteroDictAct(object):
    def __init__(self, act=None):
        if act == 'relu':
            self.activation = nn.ReLU()
        elif act == 'leakyrelu':
            self.activation = nn.LeakyReLU()
        elif act == 'silu':
            self.activation = nn.SiLU()
        elif act == 'sigmoid':
            self.activation = nn.Sigmoid()
        else:
            raise f'{act} not implemented'
    
    def forward(self, x_dict):
        x_dict = {key: self.activation(data) for key, data in x_dict.items()}
        return x_dict

class FeaturetoyHetero(nn.Module):
    def __init__(self, dx, dy, types):
        """ Map features of different types to global features """
        super().__init__()
        self.bool_if_edge = isinstance(next(iter(types)), tuple)
        if self.bool_if_edge:
            self.lin_dict = nn.ModuleDict({
                '__'.join(key): nn.Linear(4 * dx, dy)
                for key in types
            })
        else:
            self.lin_dict = nn.ModuleDict({
                key: nn.Linear(4 * dx, dy)
                for key in types
            })

    def forward(self, feature_dict, batch_dict, bs):
        out_list = []
        
        for key, x in feature_dict.items():
            batch = batch_dict[key]
            num_graphs = bs

            sum_x = torch.zeros((num_graphs, x.size(1)), device=x.device).scatter_add_(0, batch.unsqueeze(-1).expand_as(x), x)
            min_x = torch.full((num_graphs, x.size(1)), float(0), device=x.device).scatter_reduce_(0, batch.unsqueeze(-1).expand_as(x), x, reduce='amin')
            max_x = torch.full((num_graphs, x.size(1)), float(0), device=x.device).scatter_reduce_(0, batch.unsqueeze(-1).expand_as(x), x, reduce='amax')
            sum_squares_x = torch.zeros((num_graphs, x.size(1)), device=x.device).scatter_add_(0, batch.unsqueeze(-1).expand_as(x), x**2)

            num_nodes_per_graph = torch.bincount(batch, minlength=num_graphs).unsqueeze(-1)
            num_nodes_per_graph = torch.clamp(num_nodes_per_graph, min=1e-02)
            mean_x = sum_x / num_nodes_per_graph
            variance_x = sum_squares_x / num_nodes_per_graph - mean_x**2
            variance_x = torch.clamp(variance_x, min=1e-05)
            std_x = torch.sqrt(variance_x)

            z = torch.hstack((mean_x, min_x, max_x, std_x))
            z = torch.full((bs, z.size(1)), float(0), device=x.device)
            out = self.lin_dict[key if not self.bool_if_edge else '__'.join(key)](z)
            out_list.append(out)

        final_out = torch.stack(out_list).sum(dim=0)

        return final_out

class Prior_Layer(nn.Module):
    def __init__(self, config, dim, metadata):
        super(Prior_Layer, self).__init__()
        self.dim = dim
        self.nodes_mu_lin = HeteroDictLinear(self.dim, self.dim, types=metadata[0])
        self.edges_mu_lin = HeteroDictLinearEdges(self.dim, self.dim, types=metadata[1])

        self.nodes_logstd_lin = HeteroDictLinear(self.dim, self.dim, types=metadata[0])
        self.edges_logstd_lin = HeteroDictLinearEdges(self.dim, self.dim, types=metadata[1])

    def forward(self, latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict):
        mu_nodes = self.nodes_mu_lin(latent_nodes_dict)
        logstd_nodes = self.nodes_logstd_lin(latent_nodes_dict)
        for node_type, value in mu_nodes.items():
            std_nodes = torch.exp(logstd_nodes[node_type])
            eps = torch.randn_like(std_nodes)
            latent_nodes_dict[node_type] = value + eps*std_nodes

        mu_edges = self.edges_mu_lin(latent_edge_attr_dict)
        logstd_edges = self.edges_logstd_lin(latent_edge_attr_dict)
        for edge_type, value in mu_edges.items():
            std_edges = torch.exp(logstd_edges[edge_type])
            eps = torch.randn_like(std_edges)
            latent_edge_attr_dict[edge_type] = value + eps*std_edges
        
        misc = mu_nodes, logstd_nodes, mu_edges, logstd_edges

        return latent_nodes_dict, latent_edge_index_dict, latent_edge_attr_dict, misc

class Node_Classification(nn.Module):
    def __init__(self, config):
        super(Node_Classification, self).__init__()
        self.n_hid = config.model.n_hid

        self.node_out_mlp = nn.Sequential(
            nn.Linear(self.n_hid, self.n_hid),
            nn.LeakyReLU(),
            nn.Linear(self.n_hid, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        node_prob = self.node_out_mlp(x)
        return node_prob

class Node_MLP(nn.Module):
    def __init__(self, config):
        super(Node_MLP, self).__init__()
        self.n_hid = config.model.n_hid
        self.link_pred_dim = config.model.link_pred_dim

        self.node_out_mlp = nn.Sequential(
            nn.Linear(self.n_hid, self.n_hid),
            nn.LeakyReLU(),
            nn.Linear(self.n_hid, self.link_pred_dim),
            #nn.ReLU()
        )

    def forward(self, x):
        node_prob = self.node_out_mlp(x)
        return node_prob

class RNN_Layer(nn.Module):
    def __init__(self,config, occurence_node, rnn_layers):
        self.n_hid = config.model.n_hid
        super(RNN_Layer, self).__init__()
        self.gru = nn.GRU(occurence_node, occurence_node, num_layers=rnn_layers, batch_first=True)
        self.node_out_mlp = nn.Sequential(
            nn.Linear(occurence_node, self.n_hid),
            nn.LeakyReLU(),
            nn.Linear(self.n_hid, occurence_node),
            nn.LeakyReLU()
        )
    
    def forward(self, init_deg_node, h_node_type):
        init_deg_node, h_node_type = self.gru(init_deg_node, h_node_type)
        init_deg_node = self.node_out_mlp(init_deg_node)
        return init_deg_node, h_node_type