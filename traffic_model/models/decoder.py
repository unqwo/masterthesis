import torch.nn as nn
from torch_geometric.nn.dense import HeteroDictLinear
import models.HGTE_layers as HGTE_layers


class HGT_Transformer_dec(nn.Module):
    def __init__(self, config, in_dim_nodes, in_dim_edges, metadata):
        super(HGT_Transformer_dec, self).__init__()
        self.gcs = nn.ModuleList()
        self.in_dim_nodes = in_dim_nodes
        self.in_dim_edges = in_dim_edges  
        self.n_hid     = config.model.n_hid
        self.drop      = nn.Dropout(config.model.dropout)
        n_heads = config.model.n_heads

        self.raw2hidden_nodes_lin = HeteroDictLinear(self.n_hid, self.n_hid, types=metadata[0])
        self.raw2hidden_edges_lin = HGTE_layers.HeteroDictLinearEdges(self.n_hid, self.n_hid, types=metadata[1])

        for l in range(config.model.n_layers):
            self.gcs.append(HGTE_layers.HGTConv(self.n_hid, self.n_hid, metadata, n_heads))
        
        self.reconstruct_nodes_lin = HGTE_layers.HeteroDictLinearReconstruct(self.n_hid, self.in_dim_nodes, types=metadata[0])
        self.reconstruct_edges_lin = HGTE_layers.HeteroDictLinearReconstruct(self.n_hid, self.in_dim_edges, types=metadata[1])


    def forward(self, latent_nodes_dict, edge_index_dict, latent_edge_attr_dict):
        h_dict = self.raw2hidden_nodes_lin(latent_nodes_dict)
        e_dict = self.raw2hidden_edges_lin(latent_edge_attr_dict)

        
        for gc in self.gcs:
            h_dict, e_dict = gc(h_dict, edge_index_dict, e_dict)
        
        gen_nodes_dict = self.reconstruct_nodes_lin(h_dict)
        gen_edge_attr_dict = self.reconstruct_edges_lin(e_dict)

        return gen_nodes_dict, edge_index_dict, gen_edge_attr_dict

class HGT_Transformer_enc_dec(nn.Module):
    def __init__(self, config, in_dim_nodes, in_dim_edges, out_dim_nodes, out_dim_edges, metadata):
        super(HGT_Transformer_enc_dec, self).__init__()
        self.gcs = nn.ModuleList()
        self.in_dim_nodes = in_dim_nodes
        self.in_dim_edges = in_dim_edges
        self.out_dim_nodes = out_dim_nodes
        self.out_dim_edges = out_dim_edges
        self.n_hid     = config.model.n_hid
        self.drop      = nn.Dropout(config.model.dropout)
        n_heads = config.model.n_heads

        self.raw2hidden_nodes_lin = HeteroDictLinear(self.in_dim_nodes, self.n_hid, types=metadata[0])
        self.raw2hidden_edges_lin = HGTE_layers.HeteroDictLinearEdges(self.in_dim_edges, self.n_hid, types=metadata[1])
        self.y_in = nn.Linear(1, self.n_hid)

        for l in range(config.model.n_layers):
            self.gcs.append(HGTE_layers.HGTConv(self.n_hid, self.n_hid, metadata, n_heads))
        
        self.reconstruct_nodes_lin = HGTE_layers.HeteroDictLinearReconstruct(self.n_hid, self.out_dim_nodes, types=metadata[0])  #more general to HeteroDictLinearEdges
        self.reconstruct_edges_lin = HGTE_layers.HeteroDictLinearReconstruct(self.n_hid, self.out_dim_edges, types=metadata[1])


    def forward(self, latent_nodes_dict, edge_index_dict, latent_edge_attr_dict, batch_dict, y):
        h_dict = self.raw2hidden_nodes_lin(latent_nodes_dict)
        e_dict = self.raw2hidden_edges_lin(latent_edge_attr_dict)
        y = self.y_in(y.unsqueeze(1))

        for gc in self.gcs:
            h_dict, e_dict, y = gc(h_dict, edge_index_dict, e_dict, batch_dict, y)
        
        gen_nodes_dict = self.reconstruct_nodes_lin(h_dict)
        gen_edge_attr_dict = self.reconstruct_edges_lin(e_dict)

        return gen_nodes_dict, edge_index_dict, gen_edge_attr_dict

