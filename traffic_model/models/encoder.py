import torch.nn as nn
from torch_geometric.nn.dense import HeteroDictLinear
import models.HGTE_layers as HGTE_layers


class HGT_Transformer_enc(nn.Module):
    def __init__(self, config, in_dim_nodes, in_dim_edges, metadata):
        super(HGT_Transformer_enc, self).__init__()
        self.gcs = nn.ModuleList()
        self.in_dim_nodes = in_dim_nodes
        self.in_dim_edges = in_dim_edges  
        self.n_hid     = config.model.n_hid
        self.drop      = nn.Dropout(config.model.dropout)
        n_heads = config.model.n_heads

        self.raw2hidden_nodes_lin = HeteroDictLinear(self.in_dim_nodes, self.n_hid)
        self.raw2hidden_edges_lin = HGTE_layers.HeteroDictLinearEdges(self.in_dim_edges, self.n_hid)

        for l in range(config.model.n_layers):
            self.gcs.append(HGTE_layers.HGTConv(self.n_hid, self.n_hid, metadata, n_heads))
        
        
    def forward(self, x_dict, edge_index_dict, edge_attr_dict):
        h_dict = self.raw2hidden_nodes_lin(x_dict)
        e_dict = self.raw2hidden_edges_lin(edge_attr_dict)

        
        for gc in self.gcs:
            h_dict, e_dict = gc(h_dict, edge_index_dict, e_dict)
        
        return h_dict, edge_index_dict, e_dict 

