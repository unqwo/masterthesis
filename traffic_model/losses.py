import torch.nn
from utils.misc import match_masking, insert_tensor_in_list
from torch_geometric.utils import degree

def get_loss_fn(model_type):
    if model_type == 'autoencoder':
        return loss_autoencoder
    elif model_type == 'variationalautoencoder' or model_type=='latent_variationalautoencoder':
        return loss_variationalautoencoder
    elif model_type == 'diffusionmodel' or model_type=='latent_diffusionmodel':
        return loss_diffusionmodel
    elif model_type == 'diffusionmodel2':
        return loss_diffusionmodel2
    else:
        raise ValueError(f"Model Name <{model_type}> is Unknown")


def loss_autoencoder(real_tupel, gen_tupel, model):
    x_loss = loss_heterodict_reconstruction(real_tupel[0], gen_tupel[0], torch.nn.MSELoss(reduction='mean'))
    edge_attr_loss = loss_heterodict_reconstruction(real_tupel[2], gen_tupel[2], torch.nn.MSELoss(reduction='mean'))
    loss = x_loss + edge_attr_loss
    return x_loss + edge_attr_loss, x_loss, edge_attr_loss

def loss_variationalautoencoder (real_tupel, gen_tupel, model):
    mu_nodes, logvar_nodes, mu_edges, logvar_edges = model.misc[0]    
    x_loss = loss_heterodict_reconstruction(real_tupel[0], gen_tupel[0], torch.nn.MSELoss(reduction='mean'))
    edge_attr_loss = loss_heterodict_reconstruction(real_tupel[2], gen_tupel[2], torch.nn.MSELoss(reduction='mean'))
    kl_loss_nodes = loss_heterodict_kl(mu_nodes, logvar_nodes)
    kl_loss_edge_attr = loss_heterodict_kl(mu_edges, logvar_edges)
    loss = x_loss + edge_attr_loss + 0.00001*kl_loss_nodes + kl_loss_edge_attr        #0.00001
    return loss, x_loss, edge_attr_loss, kl_loss_nodes.item(), kl_loss_edge_attr.item()

def loss_diffusionmodel(real_tupel, gen_tupel, model):
    _, edge_index_0_dict, _,ptr_dict,batch_dict,_ = real_tupel
    x_score_dict, _, edge_attr_score_dict,_,_ = gen_tupel
    edge_index_t_dict, A_t_dict, delta_Atmin1_to_At_dict, link_predictions_prob_dict = model.misc['for loss_link_predicition']

    # Loss total number of nodes based on conditioning
    pred_n_total = model.misc['for n_total'].squeeze()
    num_nodes_diff = torch.stack(list(ptr_dict.values()), dim=1).diff(dim=0).float()
    real_n_total = torch.sum(num_nodes_diff, dim=1)
    # real_n_total = (torch.sum(num_nodes_diff, dim=1)-911)/320
    #loss_n_total = torch.nn.MSELoss(reduction='mean')(pred_n_total, real_n_total)/100
    loss_n_total = loss_nll_gaussian(real_n_total, pred_n_total)

    # Loss sampling initial number of nodes
    pred_num_nodes_dict = model.misc['for num_nodes_learning']
    num_nodes_diff = torch.stack(list(ptr_dict.values()), dim=1).diff(dim=0).float()
    real_num_nodes_dict = {node_type: num_nodes_diff[:, i] for i, node_type in enumerate(ptr_dict.keys())}
    # real_num_nodes_dict = {'road_segment': (real_num_nodes_dict['road_segment']-161)/42,
    #                        'obstacle':  (real_num_nodes_dict['obstacle']-749)/326}
    #loss_init_num_nodes = loss_heterodict_reconstruction(real_num_nodes_dict, pred_num_nodes_dict, torch.nn.MSELoss(reduction='mean'))/100
    loss_init_num_nodes = loss_heterodict_nll(real_num_nodes_dict, pred_num_nodes_dict)

    # Loss predicting degree distribution of graph
    pred_degree_dict_list = model.misc['for degree_dist_learning']
    real_degree_dict_list = prepare_for_degree_loss(edge_index_0_dict, ptr_dict, batch_dict)
    loss_degree_dist = loss_heterodict_reconstruction_list(real_degree_dict_list, pred_degree_dict_list, torch.nn.MSELoss(reduction='mean'))

    # Loss link prediction
    loss_ones_label_dict = {}
    loss_ones_gen_dict = {}
    loss_zeros_label_dict = {}
    loss_zeros_gen_dict = {}
    for edge_type, edge_index_0 in edge_index_0_dict.items():
        #mark the delta edges
        mask_sim_to_delta = match_masking(edge_index_0[:,delta_Atmin1_to_At_dict[edge_type]], link_predictions_prob_dict[edge_type][0]) # same len as link_predictions_prob
        link_predictions_only_delta_prob = link_predictions_prob_dict[edge_type][1][mask_sim_to_delta]
        loss_ones_label_dict[edge_type] = torch.ones_like(link_predictions_only_delta_prob)
        loss_ones_gen_dict[edge_type] = link_predictions_only_delta_prob

        #mark the edges, which should have not been predicted with positive probability
        mask_sim_to_edge_index_t = match_masking(edge_index_t_dict[edge_type], link_predictions_prob_dict[edge_type][0])  # same len as link_predictions_prob
        mask_relevant = ~mask_sim_to_delta*~mask_sim_to_edge_index_t
        link_predictions_relevant = link_predictions_prob_dict[edge_type][1][mask_relevant]
        loss_zeros_label_dict[edge_type] = torch.zeros_like(link_predictions_relevant)
        loss_zeros_gen_dict[edge_type] = link_predictions_relevant

    loss_ones = loss_heterodict_reconstruction(loss_ones_label_dict, loss_ones_gen_dict, torch.nn.BCELoss(reduction='mean')) 
    loss_zeros = loss_heterodict_reconstruction(loss_zeros_label_dict, loss_zeros_gen_dict, torch.nn.BCELoss(reduction='mean')) 
    loss_link_predicition = loss_ones + loss_zeros

    # Loss score prediction
    noiser = model.forward_diffuser
    loss_x = loss_score(x_score_dict, noiser.std_x_dict, noiser.z_x_dict)

    loss_edge_attr = loss_score(edge_attr_score_dict, noiser.std_edge_attr_dict, noiser.z_edge_attr_dict, mask_A_t=A_t_dict, mask_delta = delta_Atmin1_to_At_dict)

    loss = loss_x + loss_edge_attr + loss_link_predicition + loss_degree_dist# + loss_init_num_nodes + loss_n_total

    return loss, loss_x, loss_edge_attr, loss_link_predicition.item(), loss_init_num_nodes.item(), loss_degree_dist.item(), loss_n_total.item(), loss_ones.item(), loss_zeros.item()


def loss_heterodict_reconstruction(real_dict, gen_dict, loss_fn):
    loss = torch.tensor([0.0])
    for key, value in real_dict.items():
        loss = loss.to(device=value.device)
        if (value.numel() != 0 and gen_dict[key].numel()!=0):
            loss += loss_fn(gen_dict[key], value)
    loss /= len(real_dict.keys())
    return loss

def loss_heterodict_reconstruction_list(real_dict, gen_dict, loss_fn):
    loss = 0
    for key, value in real_dict.items():
        loss += loss_fn(gen_dict[key][0].squeeze(), value[0].float())*0.5
        loss += loss_fn(gen_dict[key][1].squeeze(), value[1].float())*0.5
    loss /= len(real_dict.keys())
    return loss

def loss_heterodict_kl(mu_dict, logvar_dict):
    loss = 0
    for key, value in mu_dict.items():
        if value.size(0)>0:
            logvar = logvar_dict[key]
            kl_divergence = -0.5 * (1 + logvar - value**2 - torch.exp(logvar)).mean(1).mean()
            loss += kl_divergence
    loss /= len(mu_dict.keys())
    return loss


def loss_score(score_dict, std_dict, z_dict, mask_A_t=None, mask_delta=None, edge_index_0_dict=None):
    loss = 0
    for key, value in score_dict.items():
        if mask_A_t==None and edge_index_0_dict==None:
            loss += torch.square(value - z_dict[key]).mean(1).mean()
            c = value
            d = z_dict[key]
        elif mask_A_t==None:
            count_edge_types = len(edge_index_0_dict.keys())
            score_value = value[:, :-(count_edge_types*2)]
            loss += torch.square(score_value - z_dict[key]).mean(1).mean()
            c = score_value
            d = z_dict[key]
        else:
            if value.size(0) > 0:
                loss += torch.square(value[:mask_A_t[key].sum()] - z_dict[key][mask_A_t[key]]).mean(1).mean()
                a = value[:mask_A_t[key].sum()]
                b = z_dict[key][mask_A_t[key]]
                #loss += torch.square(value[-mask_delta[key].sum():] - z_dict[key][mask_delta[key]]).mean(1).mean()
                e = value[-mask_delta[key].sum():]
                f = z_dict[key][mask_delta[key]]

    loss /= len(score_dict.keys())
    return loss

def loss_nll_gaussian(targets, predictions):
    mu = predictions[:, 0] 
    logvar = predictions[:, 1] 
    
    var = torch.exp(logvar)  # variance = exp(logvar)
    
    # NLL for gaussian
    nll = 0.5 * (logvar + ((targets - mu)**2) / var + torch.log(torch.tensor(2 * torch.pi)).to(var.device))
    return nll.mean()

def loss_heterodict_nll(real_dict, gen_dict):
    loss = torch.tensor([0.0])
    for key, value in real_dict.items():
        loss = loss.to(device=value.device)
        if (value[0].numel() != 0 and gen_dict[key][0].numel()!=0):
            loss += loss_nll_gaussian(value, gen_dict[key])
    loss /= len(real_dict.keys())
    return loss

def prepare_for_degree_loss(edge_index_0_dict, ptr_dict, batch_dict):  
    reduce_scale = 1

    degree_dict_list = {edge_type:[torch.empty(0), torch.empty(0)] for edge_type in edge_index_0_dict.keys()}
    for node_type, ptr in ptr_dict.items():
        deg_list = []
        for edge_type, edge_index in edge_index_0_dict.items():
            if node_type==edge_type[0]:
                deg_list.append(torch.log1p(degree(edge_index[0], num_nodes=ptr.max())-0)/reduce_scale)
                med = deg_list[0].median()
                mu = deg_list[0].mean()
                maxi = deg_list[0].max()
            if node_type==edge_type[2]:
                deg_list.append(torch.log1p(degree(edge_index[1], num_nodes=ptr.max())-0)/reduce_scale)
        ptr_diff = ptr_dict[node_type].diff().long()
        ptr_diff_sqr = torch.cat([torch.tensor([0], device=ptr_diff.device), ptr_diff[:-1]]).cumsum(dim=0)
        ptr_diff_sqr = ptr_diff_sqr[batch_dict[node_type]]
        deg_ten = torch.stack(deg_list, dim=1) + ptr_diff_sqr[:, None]
        for col in reversed(range(deg_ten.shape[1])):
            sorted_indices = deg_ten[:, col].argsort(stable=True)
            deg_ten = deg_ten[sorted_indices]
        deg_ten-= ptr_diff_sqr[:, None]
        degree_dict_list = insert_tensor_in_list(node_type, deg_ten, degree_dict_list)
    return degree_dict_list