import torch
import abc
import wandb

from utils.loader import load_sde
from torch_geometric.utils import degree, remove_self_loops

import seaborn as sns
import matplotlib.pyplot as plt
import wandb

from utils.misc import match_masking
from models.noising import Noiser4

class Predictor(abc.ABC):
    """The abstract class for a predictor algorithm."""
    def __init__(self, sde_x, sde_edge_attr, probability_flow=False):
        super().__init__()
        self.sde_x = sde_x
        self.sde_edge_attr = sde_edge_attr
        # Compute the reverse SDE/ODE
        self.rsde_x = sde_x.reverse(probability_flow)
        self.rsde_edge_attr = sde_edge_attr.reverse(probability_flow)

    @abc.abstractmethod
    def update_fn(self, x, t, flags):
        pass

class Corrector(abc.ABC):
    """The abstract class for a corrector algorithm."""
    def __init__(self, config):
        super().__init__()

    @abc.abstractmethod
    def update_fn(self, x_dict, edge_attr_dict, t):
        pass

class ReverseDiffusionPredictor(Predictor):
    def __init__(self, sde_x, sde_adj, probability_flow=False, config=None):
        super().__init__(sde_x, sde_adj, probability_flow)
        self.scale = 1
        self.noiser = Noiser4(config)

    def update_fn(self, x_dict, x_score_dict, edge_attr_dict, edge_attr_score_dict, edge_index_dict, batch_dict, t):
        
        for node_type, x in x_dict.items():
            f_x, G_x = self.rsde_x.discretize(x, t, x_score_dict[node_type], batch_dict[node_type])     #(bs*n, dx), (bs*n)
            z = torch.randn_like(x)
            x_mean = x - f_x
            x_dict[node_type] = x_mean + G_x[:, None] * z
        
        for edge_type, edge_attr in edge_attr_dict.items():
            if edge_index_dict[edge_type].size(1) > 0:
                batch_src = batch_dict[edge_type[0]]
                batch_edge = batch_src[edge_index_dict[edge_type][0]]

                f_edge_attr, G_edge_attr = self.rsde_edge_attr.discretize(edge_attr, t, edge_attr_score_dict[edge_type], batch_edge)    #(bs*e, de), (bs*e)
                z = torch.randn_like(edge_attr)
                edge_attr_mean = edge_attr - f_edge_attr
                edge_attr_dict[edge_type] = edge_attr_mean + G_edge_attr[:, None] * z

            
        return x_dict, edge_attr_dict
    
    def update_fn_with_classifier_free_guidance(self, x_dict, x_uncon_score_dict, x_con_score_dict, edge_attr_dict, edge_attr_uncon_score_dict, edge_attr_con_score_dict, edge_index_dict, batch_dict, t):
        for node_type, x in x_dict.items():
            t_b = t[batch_dict[node_type]]
            beta_t = self.noiser.sde.discrete_betas.to(x.device)[t_b]
            alpha_t = self.noiser.sde.alphas.to(x.device)[t_b]
            alpha_bar_t = self.noiser.sde.alphas_cumprod.to(x.device)[t_b]
            sqrt_1m_alphas_cumprod = self.noiser.sde.sqrt_1m_alphas_cumprod.to(x.device)[t_b]
            if t[0] > 0:
                alpha_bar_t_prev = self.noiser.sde.alphas_cumprod.to(x.device)[t_b-1]
            else:
                alpha_bar_t_prev = torch.ones_like(alpha_bar_t)
            sigma_t = torch.sqrt(beta_t*(1. - alpha_bar_t_prev)/(1. - alpha_bar_t))
            
            if t[0] > 0:
                z = torch.randn_like(x)                                     #(bs*n, dx)
            else:
                z = torch.zeros_like(x)
            x_score = x_uncon_score_dict[node_type] + self.scale*(x_con_score_dict[node_type]-x_uncon_score_dict[node_type])

            x_dict[node_type] = 1/torch.sqrt(alpha_t[:, None])*(x - (beta_t/sqrt_1m_alphas_cumprod)[:,None]*x_score) + sigma_t[:, None] * z
        
        for edge_type, edge_attr in edge_attr_dict.items():
            if edge_index_dict[edge_type].size(1) > 0:
                batch_src = batch_dict[edge_type[0]]
                batch_edge = batch_src[edge_index_dict[edge_type][0]]

                t_b = t[batch_edge]
                beta_t = self.noiser.sde.discrete_betas.to(x.device)[t_b]
                alpha_t = self.noiser.sde.alphas.to(x.device)[t_b]
                alpha_bar_t = self.noiser.sde.alphas_cumprod.to(x.device)[t_b]
                sqrt_1m_alphas_cumprod = self.noiser.sde.sqrt_1m_alphas_cumprod.to(x.device)[t_b]
                if t[0] > 0:
                    alpha_bar_t_prev = self.noiser.sde.alphas_cumprod.to(x.device)[t_b-1]
                else:
                    alpha_bar_t_prev = torch.ones_like(alpha_bar_t)
                sigma_t = torch.sqrt(beta_t*(1. - alpha_bar_t_prev)/(1. - alpha_bar_t))

                if t[0] > 0:
                    z = torch.randn_like(edge_attr)                                        #(bs*e, de)
                else:
                    z = torch.zeros_like(edge_attr)

                edge_attr_score = edge_attr_uncon_score_dict[edge_type] + self.scale*(edge_attr_con_score_dict[edge_type] - edge_attr_uncon_score_dict[edge_type])

                edge_attr_dict[edge_type] = 1/torch.sqrt(alpha_t[:, None])*(edge_attr - (beta_t/sqrt_1m_alphas_cumprod)[:,None]*edge_attr_score) + sigma_t[:, None] * z

        return x_dict, edge_attr_dict
    
class NoneCorrector(Corrector):
    """An empty corrector that does nothing."""

    def __init__(self, config):
        pass

    def update_fn(self, x_dict, edge_attr_dict, t):
        return x_dict, edge_attr_dict

class Diff_Sampler():
    def __init__(self, config):
        self.config=config
        self.n_hid = config.model.n_hid
        self.dc = config.model.dc
        self.sde_x = load_sde(config.sde.x)
        self.sde_edge_attr = load_sde(config.sde.edge_attr) 
        self.predictor = ReverseDiffusionPredictor(self.sde_x, self.sde_edge_attr, config.sample.probability_flow, config)
        self.corrector = NoneCorrector(config)   #LangevinCorrector if corrector=='Langevin' else

    def diff_sample(self, num, model):
        combined_x_dict = {node_type: torch.empty((0,model.out_dim_nodes[node_type]), device=model.device) for node_type in model.metadata[0]}
        combined_edge_index_dict = {edge_type: torch.empty((2,0), device=model.device, dtype=torch.int64) for edge_type in model.metadata[1]}
        combined_edge_attr_dict = {edge_type: torch.empty((0,model.out_dim_edges[edge_type]), device=model.device) for edge_type in model.metadata[1]}
        combined_ptr_dict = {node_type: torch.tensor([0], device=model.device) for node_type in model.metadata[0]}
        combined_batch_dict = {node_type: torch.empty((0), device=model.device) for node_type in model.metadata[0]}
        
        bs = self.config.data.batch_size
        rounds = int(num/bs) if num%bs==0 else int(num/bs)+1
        for round in range(rounds):
            bs = bs if num/bs >= 1 else num%bs
            num-=bs

            # StepA: sample conditionig vectors for each graph
            y = torch.randn((bs, model.condition_dim), device=model.device)       #(bs, dc)
            if self.config.data.data == 'Traffic':
                y = torch.nn.functional.one_hot(torch.randint(0, model.condition_dim, (bs,)), num_classes=model.condition_dim).float().to(model.device)
            c_embedded = model.condition_emb_1(y)
            #c_embedded = torch.zeros_like(c_embedded)
            #c_embedded = torch.randn((bs, model.config_m.dc), device=model.device)

            # StepB: sample amount of total nodes per graph
            n_total = model.mlp_total_number_nodes(c_embedded)         #mu and sigma     #(bs, 2)
            n_total = n_total[:, 0] + torch.exp(n_total[:, 1])*torch.randn_like(n_total[:, 1])    #sampled n_total
            #n_total = (n_total[:, 0] + torch.exp(0.5 *n_total[:, 1])*torch.randn_like(n_total[:, 1]))*320 + 911    # for traffic
            #n_total = torch.ones_like(n_total[:, 0])*200 + torch.randn_like(n_total[:, 1])*torch.ones_like(n_total[:, 1])*50
            n_total = torch.ones_like(n_total)     #1222 #2485 #Cora
            n_total = torch.max(n_total, torch.ones_like(n_total)*5)#*500)
            n_total = n_total.unsqueeze(1)
            print(n_total)
            n_total = (torch.rand(bs, device=model.device)*21 + 7).long().float().unsqueeze(1) ##useless

            # StepC: sample number of node for each node type
            #learned_num_nodes = model.mlp_number_nodes(torch.cat([n_total, c_embedded], dim=1))
            learned_num_nodes = model.mlp_number_nodes(torch.cat([(n_total), c_embedded], dim=1))
            #learned_num_nodes = model.mlp_number_nodes(n_total)
            # n_dict = {node_type: (learned_num_nodes[:,2*i]+ torch.exp(0.5 *learned_num_nodes[:, 2*i+1])*torch.abs(torch.randn_like(learned_num_nodes[:, 2*i+1]))).long()
            #           for i, node_type in enumerate(model.metadata[0])}      #nt: (bs)
            n_dict = {node_type: (learned_num_nodes[:,2*i]+ torch.exp(0.5 *learned_num_nodes[:, 2*i+1])*torch.randn_like(learned_num_nodes[:, 2*i+1]))
                      for i, node_type in enumerate(model.metadata[0])}
            # n_dict = {'road_segment': torch.abs((n_dict['road_segment']*42+161)).long(),
            #         'obstacle':  torch.abs((n_dict['obstacle']*326+749)).long()}     #traffic scenario
            # n_dict = {'road_segment': torch.abs((torch.ones_like(n_total.squeeze())*161 + torch.randn_like(n_total.squeeze())*42)).long(),
            #         'obstacle':  torch.abs((torch.ones_like(n_total.squeeze())*749 + torch.randn_like(n_total.squeeze())*326)).long()}   
            print(n_dict)
            # n_dict = {node_type: torch.max(value, torch.ones_like(value)*5) for node_type, value in n_dict.items()} #only for traffic
            n_dict = {node_type: n_total.squeeze().long()  
                     for i, node_type in enumerate(model.metadata[0])}      #only for molecules, that number of nodes is equal
            #n_dict = {node_type: (n_total.squeeze()).long() for i, node_type in enumerate(model.metadata[0])}
            #n_dict = {'doc': n_total.squeeze().long()} #Cora
            ptr_dict = {node_type: torch.cat([torch.tensor([0], device=value.device), value.cumsum(dim=0)]) for node_type, value in n_dict.items()}   #nt: (bs+1)
            batch_dict = {node_type: torch.repeat_interleave(torch.arange(len(value), device=value.device), value) for node_type, value in n_dict.items()}

            # StepD: sample node degree distribution
            #c_embedded = model.condition_emb_4(y)
            degree_0_dict_list = model.compute_degree_dist(n_dict)#, c_embedded)     #et: [(bs*n_src), (bs*n_dst)]
            degree_t_dict_list = {edge_type: [torch.zeros_like(value[0]),torch.zeros_like(value[1])]  for edge_type, value in degree_0_dict_list.items()}
            degree_0_dict_list = {edge_type: [torch.expm1(value[0]),torch.expm1(value[1])]  for edge_type, value in degree_0_dict_list.items()}
            #degree_0_dict_list = {edge_type: [torch.rand_like(value[0])*4 + 1]*2  for edge_type, value in degree_0_dict_list.items()}
            degree_0_dict_list = {edge_type: [value[0]]*2  for edge_type, value in degree_0_dict_list.items()}      #only for molecules
            #degree_0_dict_list = {edge_type: [(value[0].exp()-1)]*2  for edge_type, value in degree_0_dict_list.items()} #only for cora
            if True:
                for et, v in degree_0_dict_list.items():
                    plt.figure()
                    sns.kdeplot(v[0].cpu().numpy(), fill=True, label='degree distribution', color='orange', alpha=0.5)
                    plt.legend()
                    wandb.log({
                            f"Out-Degree distribution of {et[1]}": wandb.Image(plt),
                        })
                    plt.figure()
                    sns.kdeplot(v[1].cpu().numpy(), fill=True, label='degree distribution', color='orange', alpha=0.5)
                    plt.legend()
                    wandb.log({
                            f"In-Degree distribution of {et[1]}": wandb.Image(plt),
                        })

            # StepE: reverse diffusion process
            # Initialize
            x_dict = {node_type: torch.randn((value[-1], model.out_dim_nodes[node_type]), device=model.device) for node_type, value in ptr_dict.items()}   #nt: (bs*n, dx)
            edge_index_dict = {edge_type: torch.empty((2,0), device=model.device, dtype=torch.int64) for edge_type in model.metadata[1]}                   #et: (2, 0)
            edge_attr_dict = {edge_type: torch.empty((0,model.out_dim_edges[edge_type]), device=model.device) for edge_type in model.metadata[1]}          #et: (0, de)

            # Start diffusion
            T = self.config.sde.x.num_scales
            with torch.no_grad():
                for t in reversed(range(T)):
                    t = torch.full((bs,1), fill_value=t, device=model.device).squeeze()
                    tmin1 = torch.max(torch.zeros_like(t), (t - 1))
                    t_embedded = model.time_emb((t/T).unsqueeze(1))

                    # Substep i: determine active nodes
                    active_dict_list= {edge_type:[torch.empty(0), torch.empty(0)] for edge_type in model.metadata[1]}
                    for edge_type, l in degree_0_dict_list.items():
                        src = edge_type[0]
                        dst = edge_type[2]
                        for i, node_type in enumerate([src,dst]):
                            batch=batch_dict[node_type]
                            alpha_bar_t = model.forward_diffuser.sde.alphas_cumprod.to(model.device)[t[batch]]         #(bs*n_nt)
                            alpha_bar_tmin1 = model.forward_diffuser.sde.alphas_cumprod.to(model.device)[tmin1[batch]]
                            gamma_t = (alpha_bar_tmin1-alpha_bar_t)/(1-alpha_bar_t)
                            d_0 = l[i]                                             #(bs*n_nt)
                            d_t = degree_t_dict_list[edge_type][i]
                            delta_0 = torch.max(torch.zeros_like(d_0), d_0 - d_t)
                            prob_active = torch.where(gamma_t < 1.0, 1-(1-gamma_t)**(delta_0*4), 0)
                            active_dict_list[edge_type][i]=torch.bernoulli(prob_active).bool().to(device=model.device)        #(bs*n_nt)

                    active_sum = {edge_type: [value[0].sum(), value[1].sum()] for edge_type, value in active_dict_list.items()}                
                    # Substep ii: link prediction --> new edges
                    # corresponds to Step3 in Diffusionmodel_class: First message passing
                    t_embedded_pseudo = torch.zeros_like(t_embedded)
                    x_emb_dict, edge_index_dict, edge_attr_emb_dict = model.add_emb(x_dict, edge_index_dict, edge_attr_dict, batch_dict, t_embedded_pseudo)
                    c_embedded = model.condition_emb_2(y)
                    x_emb_dict, edge_index_dict, edge_attr_emb_dict = model.add_emb(x_emb_dict, edge_index_dict, edge_attr_emb_dict, batch_dict, c_embedded)
                    x_emb_dict = model.add_degree_emb(x_emb_dict, edge_index_dict, degree_0_dict_list, model.degree_emb_1)
                    x_t_hat_dict,_,_ = model.first_mpnn(x_emb_dict, edge_index_dict, edge_attr_emb_dict,  batch_dict, t/T)    
                    # corresponds to Step4 & Step5 in Diffusionmodel_class: link prediction & updated list
                    emerge_edge_index_dict, link_predictions_prob_dict = model.link_prediction(x_t_hat_dict, edge_index_dict, batch_dict, active_dict_list)

                    if (t[0]*T)%1!=0:
                        emerge_edge_index_dict = {key: torch.empty((2,0), device=value.device, dtype=torch.int64) for key, value in emerge_edge_index_dict.items()} #delete predictions
                    else:
                        o = 0
                        wandb.log({"num_edges": emerge_edge_index_dict[('atom', 'connected', 'atom')].size(1), "active_sum": active_sum[('atom', 'connected', 'atom')][0]})
                        #wandb.log({"num_edges": emerge_edge_index_dict[('doc', 'cites', 'doc')].size(1), "active_sum": active_sum[('doc', 'cites', 'doc')][0]})
                        #wandb.log({"active_sum": active_sum[('obstacle', 'obstacle_to_obstacle_temporal', 'obstacle')][0],"num_edges_o2o": emerge_edge_index_dict[('obstacle', 'obstacle_to_obstacle', 'obstacle')].size(1), "num_edges_o2r": emerge_edge_index_dict[('obstacle', 'obstacle_to_road', 'road_segment')].size(1), "num_edges_r2r": emerge_edge_index_dict[('road_segment', 'road_to_road', 'road_segment')].size(1), "num_edges_o2ot": emerge_edge_index_dict[('obstacle','obstacle_to_obstacle_temporal', 'obstacle')].size(1)})
                    updated_edge_index_dict={}
                    updated_edge_attr_dict={}
                    for edge_type, edge_index in edge_index_dict.items():
                        emerge_edge_index = emerge_edge_index_dict[edge_type]
                        emerge_edge_attr = torch.zeros((emerge_edge_index.shape[1], edge_attr_dict[edge_type].shape[1]), device=emerge_edge_index.device) #initialize with zero vector
                        emerge_edge_index, emerge_edge_attr = self.symmetrize_and_remove_self_loops(emerge_edge_index, emerge_edge_attr, edge_type)
                        if edge_type[1] == 'obstacle_to_obstacle_temporal':
                            emerge_edge_index, emerge_edge_attr = self.delete_reverse_edges(emerge_edge_index, emerge_edge_attr, edge_index)

                        updated_edge_index_dict[edge_type] = torch.cat([edge_index, emerge_edge_index], dim=1)
                        updated_edge_attr_dict[edge_type] = torch.cat([edge_attr_dict[edge_type], emerge_edge_attr], dim=0)
                    
                    # Substep iii: compute scores
                    # corresponds to Step6 in Diffusionmodel_class: score prediction
                    c_embedded = model.condition_emb_3(y)
                    x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict = model.add_emb(x_dict, updated_edge_index_dict, updated_edge_attr_dict, batch_dict, t_embedded)
                    x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict = model.add_emb(x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict, batch_dict, c_embedded)
                    x_emb_dict = model.add_degree_emb(x_emb_dict, edge_index_dict,degree_0_dict_list, model.degree_emb_2)
                    #updated_edge_index_dict = {node_type: value.int() for node_type, value in updated_edge_index_dict.items()}
                    x_con_score_dict,_, edge_attr_con_score_dict = model.score_net(x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict, batch_dict, t/T)

                    # Substep iii.b: compute scores unconditioned
                    c_embedded = torch.zeros_like(c_embedded)
                    x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict = model.add_emb(x_dict, updated_edge_index_dict, updated_edge_attr_dict, batch_dict, t_embedded)
                    x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict = model.add_emb(x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict, batch_dict, c_embedded)
                    x_emb_dict = model.add_degree_emb(x_emb_dict, edge_index_dict, degree_0_dict_list, model.degree_emb_2)
                    #updated_edge_index_dict = {node_type: value.int() for node_type, value in updated_edge_index_dict.items()}
                    x_uncon_score_dict,_, edge_attr_uncon_score_dict = model.score_net(x_emb_dict, updated_edge_index_dict, updated_edge_attr_emb_dict,  batch_dict, t/T)


                    # Substep iv: update all variables with scores
                    #x_dict, updated_edge_attr_dict = self.corrector.update_fn(x_dict, updated_edge_attr_dict, t)
                    edge_index_dict=updated_edge_index_dict
                    x_dict, edge_attr_dict = self.predictor.update_fn_with_classifier_free_guidance(x_dict, x_uncon_score_dict, x_con_score_dict, updated_edge_attr_dict, edge_attr_uncon_score_dict, edge_attr_con_score_dict, updated_edge_index_dict, batch_dict, t)
                    degree_t_dict_list = self.update_degree_dict_list(degree_t_dict_list, edge_index_dict)

            
            # StepF: shift indices to fit in batch
            ptr_dict, batch_dict, edge_index = self.shift_indices_for_batch(ptr_dict, batch_dict, edge_index_dict, combined_ptr_dict)

            # append batch to set
            combined_x_dict = {node_type: torch.cat([value, x_dict[node_type]], dim=0) for node_type, value in combined_x_dict.items()}
            combined_edge_index_dict = {edge_type: torch.cat([value, edge_index_dict[edge_type]], dim=1) for edge_type, value in combined_edge_index_dict.items()}
            combined_edge_attr_dict = {edge_type: torch.cat([value, edge_attr_dict[edge_type]], dim=0) for edge_type, value in combined_edge_attr_dict.items()}
            combined_ptr_dict = {node_type: torch.cat([value, ptr_dict[node_type][1:]], dim=0) for node_type, value in combined_ptr_dict.items()}
            combined_batch_dict = {node_type: torch.cat([value, batch_dict[node_type]], dim=0) for node_type, value in combined_batch_dict.items()}

        return combined_x_dict, combined_edge_index_dict, combined_edge_attr_dict, combined_ptr_dict, combined_batch_dict
    
    def shift_indices_for_batch(self, ptr_dict, batch_dict, edge_index_dict, combined_ptr_dict):
        ptr_dict = {node_type: value + combined_ptr_dict[node_type][-1] for node_type, value in ptr_dict.items()}
        batch_dict = {node_type: value + combined_ptr_dict[node_type][-1] for node_type, value in batch_dict.items()}
        for edge_type, edge_index in edge_index_dict.items():
            src = edge_type[0]
            dst = edge_type[2]
            edge_index[0] = edge_index[0] + combined_ptr_dict[src][-1] 
            edge_index[1] = edge_index[1] + combined_ptr_dict[dst][-1] 
        return ptr_dict, batch_dict, edge_index
    
    def update_degree_dict_list(self, degree_t_dict_list, edge_index_dict):
        for edge_type, edge_index in edge_index_dict.items():
            d = degree_t_dict_list[edge_type] 
            degree_t_dict_list[edge_type] = [degree(edge_index[0], num_nodes=d[0].shape[0]), degree(edge_index[1], num_nodes=d[1].shape[0])]
        return degree_t_dict_list

    def symmetrize_and_remove_self_loops(self, edge_index, edge_attr, edge_type):  
        # notice, only when src and dst the same
        if edge_type[0]==edge_type[2] and edge_index.size(1)>0:
            edge_index, edge_attr = remove_self_loops(edge_index, edge_attr)
            full_edge_index = torch.cat([edge_index, edge_index[[1, 0], :]], dim=1) if edge_type[1]!='obstacle_to_obstacle_temporal' else edge_index
            full_edge_attr = torch.cat([edge_attr, edge_attr], dim=0)
            unique_edge_index, unique_indices = torch.unique(full_edge_index, return_inverse=True, dim=1)
            unique_edge_attr = torch.zeros_like(full_edge_attr[:unique_edge_index.shape[1]])
            edge_index = unique_edge_index
            edge_attr = unique_edge_attr
        return edge_index, edge_attr
    

    def delete_reverse_edges(self, new_edge_index, emerge_edge_attr, existing_edge_index):
        check_mask = match_masking(existing_edge_index, new_edge_index[[1,0],:]) #(num_emerge_edges)

        emerge_edge_attr = emerge_edge_attr[:new_edge_index.shape[1],:]
        return new_edge_index, emerge_edge_attr

    
