import torch
import json
from torch_geometric.utils import to_dense_adj

import numpy as np
import scipy.sparse as sp
import networkx as nx
import powerlaw

def eval_cora(test_set, gen_set, device):
    result = 0
    result_list_end = {}

    result_list = [compute_graph_statistics(to_dense_adj(g['doc', 'cites', 'doc'].edge_index).squeeze().cpu().numpy()) for g in gen_set]
    result_listb = compute_graph_statistics(to_dense_adj(test_set[0]['doc', 'cites', 'doc'].edge_index).squeeze().cpu().numpy())
    for k,v in result_list[0].items():
        value = 0
        for j in range(len(gen_set)):
            value += result_list[j][k]
        result_list_end[k] = value/len(gen_set)

    eo = [edge_overlap(to_dense_adj(test_set[0]['doc', 'cites', 'doc'].edge_index).squeeze().cpu().numpy(), to_dense_adj(g['doc', 'cites', 'doc'].edge_index).squeeze().cpu().numpy()) for g in gen_set]
    eo_mean = sum(eo)/len(eo)
    eo_check = edge_overlap(to_dense_adj(test_set[0]['doc', 'cites', 'doc'].edge_index).squeeze().cpu().numpy(), to_dense_adj(test_set[0]['doc', 'cites', 'doc'].edge_index).squeeze().cpu().numpy())
    
    result_list_end['eo'] = (eo_mean/eo_check)*100
    result_list_end['NTC'] = result_list_end['triangle_count']/result_listb['triangle_count']
    result_list_end['real_num_edges'] = test_set[0]['doc', 'cites', 'doc'].edge_index.shape[1]
    result_list_end['gen_num_edges'] = gen_set[0]['doc', 'cites', 'doc'].edge_index.shape[1]

    with open(f'/home/ws/unqwo/masterthesis/traffic_model/evaluation/results_coraP.txt', "w") as file:
        json.dump(result_list_end,file)
    return result_list_end

def max_degree(A):
    """
    Compute the maximum degree.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Maximum degree.
    """
    if True:
        degrees = A.sum(axis=-1)
        return np.max(degrees)
    else:
        return np.nan


def min_degree(A):
    """
    Compute the minimum degree.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Minimum degree.
    """
    if True:
        degrees = A.sum(axis=-1)
        return np.min(degrees)
    else:
        return np.nan


def average_degree(A):
    """
    Compute the average degree.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Average degree.
    """
    if True:
        degrees = A.sum(axis=-1)
        return np.mean(degrees)
    else:
        return np.nan


def LCC(A):
    """
    Compute the size of the largest connected component (LCC).
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Size of the largest connected component.
    """
    if True:
        G = nx.Graph(A)
        return max([len(c) for c in nx.connected_components(G)])
    else:
        return np.nan


def wedge_count(A):
    """
    Compute the wedge count.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Wedge count.
    """
    if True:
        degrees = np.array(A.sum(axis=-1))
        return 0.5 * np.dot(degrees.T, degrees - 1).reshape([]) 
    else:
        return np.nan


def claw_count(A):
    """
    Compute the claw count.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Claw count.
    """
    if True:
        degrees = np.array(A.sum(axis=-1))
        return 1 / 6 * np.sum(degrees * (degrees - 1) * (degrees - 2))
    else:
        return np.nan


def triangle_count(A):
    """
    Compute the triangle count.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Triangle count.
    """
    if True:
        A_graph = nx.Graph(A)
        triangles = nx.triangles(A_graph)
        t = np.sum(list(triangles.values())) / 3
        return int(t)
    else:
        return np.nan


def square_count(A):
    """
    Compute the square count.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Square count.
    """
    if True:
        A_squared = A @ A
        common_neighbors = sp.triu(A_squared, k=1).tocsr()
        num_common_neighbors = np.array(
            common_neighbors[common_neighbors.nonzero()]
        ).reshape(-1)
        return np.dot(num_common_neighbors, num_common_neighbors - 1) / 4
    else:
        return np.nan


def power_law_alpha(A):
    """
    Compute the power law coefficient of the degree distribution of the input graph.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Power law coefficient.
    """
    if True:
        degrees = np.array(A.sum(axis=-1)).flatten()
        return powerlaw.Fit(
            degrees, xmin=max(np.min(degrees), 1), verbose=False
        ).power_law.alpha
    else:
        return np.nan


def gini(A):
    """
    Compute the Gini coefficient of the degree distribution of the input graph.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Gini coefficient.
    """
    if True:
        N = A.shape[0]
        degrees_sorted = np.sort(np.array(A.sum(axis=-1)).flatten())
        return (
            2 * np.dot(degrees_sorted, np.arange(1, N + 1)) / (N * np.sum(degrees_sorted))
            - (N + 1) / N
        )
    else:
        return np.nan


def edge_distribution_entropy(A):
    """
    Compute the relative edge distribution entropy of the input graph.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Relative edge distribution entropy.
    """
    if True:
        N = A.shape[0]
        degrees = np.array(A.sum(axis=-1)).flatten()
        degrees /= degrees.sum()
        return -np.dot(np.log(degrees), degrees) / np.log(N)
    else:
        return np.nan


def assortativity(A):
    """
    Compute the assortativity of the input graph.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Assortativity.
    """
    if True:
        G = nx.Graph(A)
        return nx.degree_assortativity_coefficient(G)
    else:
        return np.nan


def clustering_coefficient(A):
    """
    Compute the clustering coefficient of the input graph.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Clustering coefficient.
    """
    if True:
        return 3 * triangle_count(A) / wedge_count(A)   
    else:
        return np.nan


def cpl(A):
    """
    Compute the characteristic path length of the input graph.
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
    
    Returns:
        Characteristic path length.
    """
    if True:
        P = sp.csgraph.shortest_path(A)
        return P[((1 - np.isinf(P)) * (1 - np.eye(P.shape[0]))).astype(bool)].mean()
    else:
        return np.nan


def compute_graph_statistics(A):
    """
    Compute a selection of graph statistics for the input graph.
    
    Args:
        A (sp.csr.csr_matrix): The input adjacency matrix.
          
    Returns:
        Dictionary containing the following statistics:
                 * Maximum, minimum, mean degree of nodes
                 * Size of the largest connected component (LCC)
                 * Wedge count
                 * Claw count
                 * Triangle count
                 * Square count
                 * Power law exponent
                 * Gini coefficient
                 * Relative edge distribution entropy
                 * Assortativity
                 * Clustering coefficient
                 * Characteristic path length
    """
    statistics = {"d_max": max_degree(A),
                  "d_min": min_degree(A),
                  "d": average_degree(A),
                  "LCC": LCC(A),
                  "wedge_count": wedge_count(A),
                  "claw_count": claw_count(A),
                  "triangle_count": triangle_count(A),
                  "square_count": square_count(A),
                  "power_law_exp": power_law_alpha(A),
                  "gini": gini(A),
                  "rel_edge_distr_entropy": edge_distribution_entropy(A),
                  "assortativity": assortativity(A),
                  "clustering_coefficient": clustering_coefficient(A),
                  "cpl": cpl(A)} 
    return statistics
    
def edge_overlap(A, B):
    """
    Compute edge overlap between two graphs (amount of shared edges).
    Args:
        A (sp.csr.csr_matrix): First input adjacency matrix.
        B (sp.csr.csr_matrix): Second input adjacency matrix.
    Returns:
        Edge overlap.
    """
    if True:
        return (A*B).sum() / 2
    else:
        return np.nan