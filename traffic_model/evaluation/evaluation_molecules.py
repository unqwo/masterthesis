from rdkit import Chem
from fcd_torch import FCD as FCDMetric
import numpy as np
import torch
import torch_geometric
import torch_geometric.data as pyg_data
from torch_geometric.utils import to_undirected, to_networkx

from sklearn.metrics.pairwise import pairwise_kernels
from evaluation.eden import vectorize

import rdkit.Chem as Chem
from rdkit.Geometry.rdGeometry import Point3D
from rdkit.Chem import QED, Crippen, rdMolDescriptors, rdmolops

from rdkit.Chem import AllChem

print("PyTorch version {}".format(torch.__version__))
print("PyG version {}".format(torch_geometric.__version__))

#@title [RUN] Helper functions for visualization

allowable_atoms = [
    "H",
    "C",
    "N",
    "O",
    "F",
    "C",
    "Cl",
    "Br",
    "I",
    "H", 
    "Unknown",
]

def to_atom(t):
    try:
        return allowable_atoms[int(t.argmax())]
    except:
        return "C"


def to_bond_index(t):
    t_s = t.squeeze()
    # return [1, 2, 3, 4][
    #     int(
    #         torch.dot(
    #             t_s,
    #             torch.tensor(
    #                 range(t_s.size()[0]), dtype=torch.float, device=t.device
    #             ),
    #         ).item()
    #     )
    # ]
    return t_s.argmax() + 1

def to_rdkit(data, device=None):
    has_pos = False
    node_list = []
    for i in range(data.x.size()[0]):
        node_list.append(to_atom(data.x[i][:5]))

    # create empty editable mol object
    mol = Chem.RWMol()
    # add atoms to mol and keep track of index
    node_to_idx = {}
    invalid_idx = set([])
    for i in range(len(node_list)):
        if node_list[i] == "Stop" or node_list[i] == "H":
            invalid_idx.add(i)
            continue
        a = Chem.Atom(node_list[i])
        molIdx = mol.AddAtom(a)
        node_to_idx[i] = molIdx

    added_bonds = set([])
    for i in range(0, data.edge_index.size()[1]):
        ix = data.edge_index[0][i].item()
        iy = data.edge_index[1][i].item()
        bond = to_bond_index(data.edge_attr[i])  # <font color='red'>TODO</font> fix this
        # bond = 1
        # add bonds between adjacent atoms

        if data.edge_attr[i].sum() == 0:
          continue

        if (
            (str((ix, iy)) in added_bonds)
            or (str((iy, ix)) in added_bonds)
            or (iy in invalid_idx or ix in invalid_idx)
        ):
            continue
        # add relevant bond type (there are many more of these)

        if bond == 0:
            continue
        elif bond == 1:
            bond_type = Chem.rdchem.BondType.SINGLE
            mol.AddBond(node_to_idx[ix], node_to_idx[iy], bond_type)
        elif bond == 2:
            bond_type = Chem.rdchem.BondType.DOUBLE
            mol.AddBond(node_to_idx[ix], node_to_idx[iy], bond_type)
        elif bond == 3:
            bond_type = Chem.rdchem.BondType.TRIPLE
            mol.AddBond(node_to_idx[ix], node_to_idx[iy], bond_type)
        elif bond == 4:
            bond_type = Chem.rdchem.BondType.SINGLE
            mol.AddBond(node_to_idx[ix], node_to_idx[iy], bond_type)

        added_bonds.add(str((ix, iy)))

    if has_pos:
        conf = Chem.Conformer(mol.GetNumAtoms())
        for i in range(data.pos.size(0)):
            if i in invalid_idx:
                continue
            p = Point3D(
                data.pos[i][0].item(),
                data.pos[i][1].item(),
                data.pos[i][2].item(),
            )
            conf.SetAtomPosition(node_to_idx[i], p)
        conf.SetId(0)
        mol.AddConformer(conf)

    # Convert RWMol to Mol object
    mol = mol.GetMol()
    mol_frags = rdmolops.GetMolFrags(mol, asMols=True, sanitizeFrags=False)
    largest_mol = max(mol_frags, default=mol, key=lambda m: m.GetNumAtoms())
    return largest_mol



def smi2conf(smiles):
    '''Convert SMILES to rdkit.Mol with 3D coordinates'''
    mol = Chem.MolFromSmiles(smiles)
    if mol is not None:
        mol = Chem.AddHs(mol)
        AllChem.EmbedMolecule(mol)
        AllChem.MMFFOptimizeMolecule(mol, maxIters=200)
        return mol
    else:
        return None

def graph_to_smiles(data):
    # mol = RWMol()
    # for i in range(data.num_nodes):
    #     atom = Chem.Atom(int(data.x[i][0]))
    #     mol.AddAtom(atom)
    # for start, end in data.edge_index.t().tolist():
    #     mol.AddBond(int(start), int(end), Chem.BondType.SINGLE)
    data = hetero_to_homo(data)
    mol=to_rdkit(data)
    return Chem.MolToSmiles(mol, canonical=True)

def hetero_to_homo(data):
    graph_homo = pyg_data.Data()
    for node_type in data.node_types:
        graph_homo.x = data[node_type].x
    for edge_type in data.edge_types:
        undirected_edges, undirected_attr = to_undirected(data[edge_type].edge_index, data[edge_type].edge_attr, reduce="mean")
        graph_homo.edge_index = undirected_edges
        graph_homo.edge_attr = undirected_attr
    return graph_homo

def eval_fcd(real_list, gen_list, device):
    real_molecules, generated_molecules = [], []
    sum_real, sum_gen = 0,0
    for graph in real_list:
        sm = graph_to_smiles(graph)
        molecule = Chem.MolFromSmiles(sm)
        if molecule is None or sm=='':
            sum_real+=1
        else:
            real_molecules.append(sm)
    for graph in gen_list:
        bond=torch.matmul(graph[graph.edge_types[0]].edge_attr,
                torch.tensor(range(graph[graph.edge_types[0]].edge_attr.shape[1]), dtype=torch.float, device=graph[graph.edge_types[0]].edge_attr.device).unsqueeze(1)).int()
        if ((bond < 0) | (bond > 4)).sum() > 0:
            sum_gen+=1
            #continue
        sm = graph_to_smiles(graph)
        molecule = Chem.MolFromSmiles(sm)
        if molecule is None or sm=='':
            sum_gen+=1
        else:
            generated_molecules.append(sm)


    fcd = FCDMetric(device=device)
    fcd_value = fcd(generated_molecules, real_molecules[:len(generated_molecules)])
    val_score = len(generated_molecules)/len(gen_list)
    print(f'FCD value: {fcd_value} and validity score: {val_score}')
    real_list = [hetero_to_homo(graph) for graph in real_list]
    gen_list = [hetero_to_homo(graph) for graph in gen_list]
    #nspdk_score = nspdk_stats(real_list, gen_list)
    nspdk_score = 0
    print(f'NSPDK value: {nspdk_score}')
    return fcd_value, val_score, nspdk_score

def nspdk_stats(graph_ref_list, graph_pred_list, KERNEL=None):
    graph_pred_list_remove_empty = [to_networkx(G, to_undirected=True) for G in graph_pred_list if not G.num_nodes == 0]
    graph_ref_list = [to_networkx(G, to_undirected=True) for G in graph_ref_list]

    mmd_dist = compute_nspdk_mmd(graph_ref_list, graph_pred_list_remove_empty, metric='nspdk', is_hist=False, n_jobs=20)
    return mmd_dist

def compute_nspdk_mmd(samples1, samples2, metric, is_hist=True, n_jobs=None):
    def kernel_compute(X, Y=None, is_hist=True, metric='linear', n_jobs=None):
        X = vectorize(X, complexity=4, discrete=True)
        if Y is not None:
            Y = vectorize(Y, complexity=4, discrete=True)
        return pairwise_kernels(X, Y, metric='linear', n_jobs=n_jobs)

    X = kernel_compute(samples1, is_hist=is_hist, metric=metric, n_jobs=n_jobs)
    Y = kernel_compute(samples2, is_hist=is_hist, metric=metric, n_jobs=n_jobs)
    Z = kernel_compute(samples1, Y=samples2, is_hist=is_hist, metric=metric, n_jobs=n_jobs)

    return np.average(X) + np.average(Y) - 2 * np.average(Z)

def novelty(gen, train):
    gen_smiles_set = set(gen)
    train_set = set(train)
    novel = len(gen_smiles_set - train_set) / len(gen_smiles_set)
    unique = len(gen_smiles_set)/len(gen)
    return novel, unique

def eval_novelty(real_list, gen_list, device):
    real_molecules, generated_molecules = [], []
    sum_real, sum_gen = 0,0
    for graph in real_list:
        sm = graph_to_smiles(graph)
        molecule = Chem.MolFromSmiles(sm)
        if molecule is None:
            sum_real+=1
        else:
            real_molecules.append(sm)
    for graph in gen_list:
        bond=torch.matmul(graph[graph.edge_types[0]].edge_attr,
                torch.tensor(range(graph[graph.edge_types[0]].edge_attr.shape[1]), dtype=torch.float, device=graph[graph.edge_types[0]].edge_attr.device).unsqueeze(1)).int()
        if ((bond < 0) | (bond > 4)).sum() > 0:
            sum_gen+=1
            #continue
        sm = graph_to_smiles(graph)
        molecule = Chem.MolFromSmiles(sm)
        if molecule is None:
            sum_gen+=1
        else:
            generated_molecules.append(sm)

    novelty_score, uniqueness = novelty(generated_molecules, real_molecules)
    print(f'novelty_score: {novelty_score} uniqueness: {uniqueness}')
    return novelty_score, uniqueness