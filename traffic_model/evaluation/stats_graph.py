from datetime import datetime
import random

from scipy.linalg import eigvalsh
import networkx as nx
import numpy as np
import torch
import torch_geometric.utils as pygu
from torch_geometric.loader import DataLoader
from scipy import linalg

from evaluation.mmd import process_tensor, compute_mmd, gaussian, gaussian_emd, compute_nspdk_mmd
from utils.loader import load_batch
from utils.data_loader_others import CustomDataset

import seaborn as sns
import matplotlib.pyplot as plt
import wandb

def fid_as_mmd2(graph_ref_list, graph_pred_list, encoder, KERNEL=gaussian_emd, is_parallel=True, use_mmd=False, device='cpu', isTraffic = False):
    sample_ref = []
    sample_pred = []

    prev = datetime.now()
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device
    #encoder = encoder.to(device_id)
    encoder.eval()
    #### zu einem batch machen
    with torch.no_grad():
        num_graphs_ref = 256#len(graph_ref_list)
        num_graphs_pred = 256#len(graph_pred_list)
        graph_ref_loader = DataLoader(CustomDataset(graph_ref_list), batch_size=256)#len(graph_ref_list))
        graph_pred_loader = DataLoader(CustomDataset(graph_pred_list), batch_size=256)#len(graph_pred_list))
        for batch in graph_ref_loader:  #only one big batch
            batch = load_batch(batch, device)
            if isTraffic:
                batch[2][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')] = torch.zeros((0, 1), dtype=torch.float32, device=device_id)
            latent_batch = forward_encoder_cut(encoder, batch)
            ptr = batch[3]
            for i in range(num_graphs_ref):
                degree_temp = torch.cat([torch.sum(value[ptr[node_type][i]:ptr[node_type][i+1]], dim=0) for node_type, value in latent_batch[0].items()], dim=0 )
                sample_ref.append(degree_temp)
        for batch in graph_pred_loader:
            batch = load_batch(batch, device)
            if isTraffic:
                batch[2][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')] = torch.zeros((0, 1), dtype=torch.float32, device=device_id)
            latent_batch = forward_encoder_cut(encoder, batch)
            ptr = batch[3]
            for i in range(num_graphs_pred):
                degree_temp = torch.cat([torch.sum(value[ptr[node_type][i]:ptr[node_type][i+1]], dim=0) for node_type, value in latent_batch[0].items()], dim=0 )
                sample_pred.append(degree_temp)
        if use_mmd:
            sample_ref = [el.to('cpu').detach().numpy() for el in sample_ref]
            sample_pred = [el.to('cpu').detach().numpy() for el in sample_pred]
            mmd_dist = compute_mmd(sample_ref, sample_pred, is_hist=False, kernel=KERNEL)
            res = mmd_dist
        else:
            sample_ref, sample_pred = prepare(sample_ref, sample_pred)
            mu1, sigma1 = sample_ref.mean(dim=0), torch.cov(sample_ref)
            mu2, sigma2 = sample_pred.mean(dim=0), torch.cov(sample_pred)

            res = calculate_fid(mu1, sigma1, mu2, sigma2)
        elapsed = datetime.now() - prev
        print('Time computing degree mmd: ', elapsed)
    return res

def fid_as_mmd(graph_ref_list, graph_pred_list, encoder, KERNEL=gaussian_emd, is_parallel=True, use_mmd=False, device='cpu', isTraffic = False):
    sample_ref = []
    sample_pred = []

    prev = datetime.now()
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device
    #encoder = encoder.to(device_id)
    encoder.eval()
    #### zu einem batch machen
    with torch.no_grad():
        graph_ref_loader = DataLoader(CustomDataset(graph_ref_list), batch_size=1024)#len(graph_ref_list))
        graph_pred_loader = DataLoader(CustomDataset(graph_pred_list), batch_size=1024)#len(graph_pred_list))
        for batch in graph_ref_loader: 
            batch = load_batch(batch, device)
            fig = {k[1]: [plt.figure(), plt.figure()] for k in batch[1].keys()}
            if isTraffic:
                batch[2][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')] = torch.zeros((batch[1][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')].shape[1], 1), dtype=torch.float64, device=device_id)
            latent_batch = forward_encoder_cut(encoder, batch)
            sample_ref.append(latent_batch)
        for batch in graph_pred_loader:
            batch = load_batch(batch, device)
            latent_batch = forward_encoder_cut(encoder, batch)
            sample_pred.append(latent_batch)
        sample_ref, sample_pred = prepare(sample_ref, sample_pred)
        mu1, sigma1 = sample_ref.mean(dim=0), torch.cov(sample_ref.T)
        mu2, sigma2 = sample_pred.mean(dim=0), torch.cov(sample_pred.T)

        res = calculate_fid(mu1, sigma1, mu2, sigma2)
    return res

def encoder2graph_rep(args):
    latent_nodes, _, __ = args
    return torch.cat([torch.sum(value, dim=0) for node_type, value in latent_nodes.items()], dim=0 )

def graph2tupel(graph, device):
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device
    x_dict = {node_type: graph[node_type].x.to(device_id) for node_type in graph.node_types}
    edge_index_dict = {edge_type: graph[edge_type].edge_index.to(device_id) for edge_type in graph.edge_types}
    edge_attr_dict = {edge_type: graph[edge_type].edge_attr.to(device_id) for edge_type in graph.edge_types}

    return x_dict, edge_index_dict, edge_attr_dict


def calculate_fid(mu1, sigma1, mu2, sigma2, eps=1e-6):
    mu1_np = mu1.cpu().numpy()
    sigma1_np = sigma1.cpu().numpy()
    mu2_np = mu2.cpu().numpy()
    sigma2_np = sigma2.cpu().numpy()

    diff = mu1_np - mu2_np
    diff_squared = diff.dot(diff)

    covmean, _ = linalg.sqrtm(sigma1_np @ sigma2_np, disp=False)

    if not np.isfinite(covmean).all():
        offset = np.eye(sigma1_np.shape[0]) * eps
        covmean = linalg.sqrtm((sigma1_np + offset) @ (sigma2_np + offset))

    if np.iscomplexobj(covmean):
        covmean = covmean.real

    fid = diff_squared + np.trace(sigma1_np + sigma2_np - 2 * covmean)
    return fid

def prepare(sample_ref, sample_pred):
    sample_ref = torch.cat(sample_ref, dim=0)
    sample_pred = torch.cat(sample_pred, dim=0)
    all_embeddings = torch.cat([sample_ref, sample_pred], dim=0)

    mean = all_embeddings.mean(dim=0)
    std = all_embeddings.std(dim=0)
    std = torch.max(std, torch.ones_like(std)*1e-7)
    return sample_ref, sample_pred

def forward_encoder_cut(encoder, batch):
    x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict = batch
    graph_embeddings = encoder(x_dict, edge_index_dict, batch_dict, edge_attr_dict)
    return graph_embeddings
