import abc
import torch
import numpy as np
from scipy.interpolate import interp1d

def linear_beta_schedule(timesteps):
    scale = 1000 / timesteps
    beta_start = scale * 0.0001
    beta_end = scale * 0.02
    return beta_start, beta_end

def clip_noise_schedule(alphas2, clip_value=0.001):
    """
    For a noise schedule given by alpha^2, this clips alpha_t / alpha_t-1. This may help improve stability during
    sampling.
    """
    alphas2 = np.concatenate([np.ones(1), alphas2], axis=0)

    alphas_step = (alphas2[1:] / alphas2[:-1])

    alphas_step = np.clip(alphas_step, a_min=clip_value, a_max=1.)
    alphas2 = np.cumprod(alphas_step, axis=0)

    return alphas2


def polynomial_schedule(timesteps: int, s=1e-4, power=1.):
    """
    A noise schedule based on a simple polynomial equation: 1 - x^power.
    """
    steps = timesteps + 1
    x = np.linspace(0, steps, steps)
    alphas2 = (1 - np.power(x / steps, power))**2

    alphas2 = clip_noise_schedule(alphas2, clip_value=0.001)

    precision = 1 - 2 * s

    alphas2 = precision * alphas2 + s

    return alphas2

class SDE(abc.ABC):
  """SDE abstract class. Functions are designed for a mini-batch of inputs."""

  def __init__(self, N):
    """Construct an SDE.
    Args:
      N: number of discretization time steps.
    """
    super().__init__()
    self.N = N
    self.beta_start, self.beta_end=linear_beta_schedule(N)

    alphas2 = polynomial_schedule(N)
    sigmas2 = 1 - alphas2
    log_alphas2 = np.log(alphas2)
    log_sigmas2 = np.log(sigmas2)
    log_alphas2_to_sigmas2 = log_alphas2 - log_sigmas2
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    self.gamma_schedule = torch.nn.Parameter(torch.from_numpy(-log_alphas2_to_sigmas2).float(), requires_grad=False).to(device)

    t_values = torch.linspace(0, 1, N + 1).to(device)
    self.continuous_gamma_schedule = lambda x: (
        lambda idxs: (
          (self.gamma_schedule[idxs + 1] - self.gamma_schedule[idxs]) / (t_values[idxs + 1] - t_values[idxs]) * (x - t_values[idxs]) + self.gamma_schedule[idxs]
        )
    )(torch.searchsorted(t_values, x, right=False) - 1)
    #         (self.gamma_schedule[1:] - self.gamma_schedule[:-1]) / (t_values[1:] - t_values[:-1]) * x + 
    #         (self.gamma_schedule[:-1] - (self.gamma_schedule[1:] - self.gamma_schedule[:-1]) / (t_values[1:] - t_values[:-1]) * t_values[:-1])
    #     )[idxs]
    # )(torch.searchsorted(t_values, x, right=False) - 1)

  @property
  @abc.abstractmethod
  def T(self):
    """End time of the SDE."""
    pass

  @abc.abstractmethod
  def sde(self, x, t):
    pass

  @abc.abstractmethod
  def marginal_prob(self, x, t, batch=None):
    """Parameters to determine the marginal distribution of the SDE, $p_t(x)$."""
    pass

  @abc.abstractmethod
  def prior_sampling(self, shape):
    """Generate one sample from the prior distribution, $p_T(x)$."""
    pass

  @abc.abstractmethod
  def prior_logp(self, z):
    """Compute log-density of the prior distribution.
    Useful for computing the log-likelihood via probability flow ODE.
    Args:
      z: latent code
    Returns:
      log probability density
    """
    pass
  def beta(self,t):
    return self.beta_start + t * (self.beta_end - self.beta_start) / self.N

  def trapezoidal_rule(self, y, h):
      return h * (0.5 * (y[:, 0] + y[:, -1]) + torch.sum(y[:, 1:-1], dim=1))

  def integrand(self,t):
      return torch.log(1 - self.beta(t))
  
  def get_alpha_t_bar(self, t, batch, n=1000):  
    t = t*self.N
    t_max = t.max()
    base_t = torch.linspace(0, t_max.item(), steps=n, device=t.device)
    t_grid = base_t.unsqueeze(0) <= t.unsqueeze(1)  
    t = base_t.unsqueeze(0).expand(t.size(0), n)
    integ_vals = self.integrand(t) * t_grid.float()  
    h = t_max / n
    integral_results = self.trapezoidal_rule(integ_vals, h)
    alpha_t_bar=torch.exp(integral_results)
    alpha_t_bar = alpha_t_bar[batch]
    return alpha_t_bar
  
  def alpha(self, gamma):
    """Computes alpha given gamma."""
    return torch.sqrt(torch.sigmoid(-gamma))
  
  def sigma(self, gamma):
    """Computes sigma given gamma."""
    return torch.sqrt(torch.sigmoid(gamma))

  def discretize(self, x, t):
    """Discretize the SDE in the form: x_{i+1} = x_i + f_i(x_i) + G_i z_i.
    Useful for reverse diffusion sampling and probabiliy flow sampling.
    Defaults to Euler-Maruyama discretization.
    Args:
      x: a torch tensor
      t: a torch float representing the time step (from 0 to `self.T`)
    Returns:
      f, G
    """
    dt = 1 / self.N
    drift, diffusion = self.sde(x, t)
    f = drift * dt
    G = diffusion * torch.sqrt(torch.tensor(dt, device=t.device))
    return f, G

  def reverse(self, probability_flow=False):
    """Create the reverse-time SDE/ODE.
    Args:
      score_fn: A time-dependent score-based model that takes x and t and returns the score.
      probability_flow: If `True`, create the reverse-time ODE used for probability flow sampling.
    """
    N = self.N
    T = self.T
    sde_fn = self.sde
    discretize_fn = self.discretize

    # -------- Build the class for reverse-time SDE --------
    class RSDE(self.__class__):
      def __init__(self):
        self.N = N
        self.probability_flow = probability_flow

      @property
      def T(self):
        return T

      def sde(self, z, t, score):
        """Create the drift and diffusion functions for the reverse SDE/ODE."""
        drift, diffusion = sde_fn(z, t)
        drift = drift - diffusion[:, None, None] ** 2 * score * (0.5 if self.probability_flow else 1.)
        # -------- Set the diffusion function to zero for ODEs. --------
        diffusion = torch.zeros_like(diffusion) if self.probability_flow else diffusion
        return drift, diffusion

      def discretize(self, z, t, score, batch):
        """Create discretized iteration rules for the reverse diffusion sampler."""
        f, G = discretize_fn(z, t, batch)
        G = G[batch]
        rev_f = f - G[:, None] ** 2 * score * (0.5 if self.probability_flow else 1.)
        rev_G = torch.zeros_like(G) if self.probability_flow else G
        return rev_f, rev_G

    return RSDE()

class VPSDE(SDE):
  def __init__(self, beta_min=0.1, beta_max=20, N=1000):
    """Construct a Variance Preserving SDE.
    Args:
      beta_min: value of beta(0)
      beta_max: value of beta(1)
      N: number of discretization steps
    """
    super().__init__(N)
    self.beta_0 = beta_min
    self.beta_1 = beta_max
    self.N = N
    self.discrete_betas = torch.linspace(beta_min / N, beta_max / N, N)
    self.alphas = 1. - self.discrete_betas
    self.alphas_cumprod = torch.cumprod(self.alphas, dim=0)
    #self.alphas_cumprod = self.cosine_beta_schedule(N)        #(N)--> changed
    self.sqrt_alphas_cumprod = torch.sqrt(self.alphas_cumprod)
    self.sqrt_1m_alphas_cumprod = torch.sqrt(1. - self.alphas_cumprod)

  @property
  def T(self):
    return 1

  def sde(self, x, t):
    beta_t = self.beta_0 + t * (self.beta_1 - self.beta_0)
    drift = -0.5 * beta_t[:, None, None] * x
    diffusion = torch.sqrt(beta_t)
    return drift, diffusion

  # -------- mean, std of the perturbation kernel --------
  def marginal_prob(self, x, t, batch=None, gdss=False):
    if gdss:
      log_mean_coeff = -0.25 * t ** 2 * (self.beta_1 - self.beta_0) - 0.5 * t * self.beta_0
      mean = torch.exp(log_mean_coeff[batch][:, None]) * x
      std = torch.sqrt(1. - torch.exp(2. * log_mean_coeff))
    else:
      mean = self.alpha(self.continuous_gamma_schedule(t))
      mean = mean[batch][:, None]*x
      std =  self.sigma(self.continuous_gamma_schedule(t))
    return mean, std
  
  def get_alpha_t_bar_old(self, t, batch):
    log_mean_coeff = -0.25 * t ** 2 * (self.beta_1 - self.beta_0) - 0.5 * t * self.beta_0
    mean = torch.exp(log_mean_coeff)
    t_max = torch.ones_like(t)
    min_value = -0.25 * t_max ** 2 * (self.beta_1 - self.beta_0) - 0.5 * t_max * self.beta_0
    min_value = torch.exp(min_value)
    alpha_t_bar = (mean-min_value)/(1-min_value)
    alpha_t_bar = alpha_t_bar[batch]
    return alpha_t_bar

  def get_reverse_param(self, t, batch):
    sigma = self.sigma(self.continuous_gamma_schedule(t))[batch]
    beta = (1-self.alpha(self.continuous_gamma_schedule(t))/self.alpha(self.continuous_gamma_schedule(t-1/self.N)))[batch]
    return beta, sigma

  def prior_sampling(self, shape):
    return torch.randn(*shape)

  def prior_sampling_sym(self, shape):
    x = torch.randn(*shape).triu(1) 
    return x + x.transpose(-1,-2)

  def prior_logp(self, z):
    shape = z.shape
    N = np.prod(shape[1:])
    logps = -N / 2. * np.log(2 * np.pi) - torch.sum(z ** 2, dim=(1, 2)) / 2.
    return logps

  def discretize(self, x, t, batch):
    """DDPM discretization."""
    timestep = (t * (self.N - 1) / self.T).long()
    beta = self.discrete_betas.to(x.device)[timestep]
    alpha = self.alphas.to(x.device)[timestep]
    sqrt_beta = torch.sqrt(beta)
    f = torch.sqrt(alpha[batch])[:, None] * x - x
    G = sqrt_beta
    return f, G

  def transition(self, x, t, dt):
    # -------- negative timestep dt --------
    log_mean_coeff = 0.25 * dt * (2*self.beta_0 + (2*t + dt)*(self.beta_1 - self.beta_0) )
    mean = torch.exp(-log_mean_coeff[:, None, None]) * x
    std = torch.sqrt(1. - torch.exp(2. * log_mean_coeff))
    return mean, std
  
  def cosine_beta_schedule(timesteps, s=0.008, raise_to_power: float = 1):  #from GeoLDM
    """
    cosine schedule
    as proposed in https://openreview.net/forum?id=-NEXDKk8gZ
    """
    steps = timesteps + 2
    x = np.linspace(0, steps, steps)
    alphas_cumprod = np.cos(((x / steps) + s) / (1 + s) * np.pi * 0.5) ** 2
    alphas_cumprod = alphas_cumprod / alphas_cumprod[0]
    betas = 1 - (alphas_cumprod[1:] / alphas_cumprod[:-1])
    betas = np.clip(betas, a_min=0, a_max=0.999)
    alphas = 1. - betas
    alphas_cumprod = np.cumprod(alphas, axis=0)

    if raise_to_power != 1:
        alphas_cumprod = np.power(alphas_cumprod, raise_to_power)

    return alphas_cumprod



