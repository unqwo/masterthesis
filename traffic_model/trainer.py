import os
import time
from tqdm import tqdm, trange
import numpy as np
import torch
from torch_geometric.loader import DataLoader

from utils.loader import load_seed, load_device, load_data, load_model_params, load_model_optimizer, \
                         load_ema, load_loss_fn, load_batch, load_sampler, load_fid_model_from_ckpt, \
                         load_latent_model_from_ckpt, load_train_model_from_ckpt
from utils.logger import Logger, set_log, start_log, train_log
from utils.misc import heterobatch2list, get_max_min_dict, scale_latent, rescale_latent, batch_norm_dict
from utils.data_loader_others import CustomDataset
from evaluation.stats_graph import fid_as_mmd
from evaluation.evaluation_molecules import eval_fcd, eval_novelty
from evaluation.evaluate_cora import eval_cora
from torch_geometric.data import HeteroData
import wandb

class Trainer(object):
    def __init__(self, config):
        super(Trainer, self).__init__()

        self.config = config
        self.log_folder_name, self.log_dir, self.ckpt_dir = set_log(self.config)

        self.seed = load_seed(self.config.seed)
        self.device = load_device()
        
        self.train_loader, self.test_loader = load_data(self.config)

        self.params = load_model_params(self.config)
        ds = self.train_loader.dataset
        self.in_dim_nodes = {nodetype: ds[0][nodetype].x.shape[1] for nodetype in ds[0].node_types}
        self.in_dim_edges = {edgetype: ds[0][edgetype].edge_attr.shape[1] if len(ds[0][edgetype].edge_attr.shape)>1 else 1 for edgetype in ds[0].edge_types}
        self.clean_in_dim_edges = self.in_dim_edges
        if self.config.model.type == 'latent_diffusionmodel':
            self.in_dim_nodes = {nodetype: config.model.latent_dim for nodetype in ds[0].node_types}
            self.in_dim_edges = {edgetype: config.model.latent_dim for edgetype in ds[0].edge_types}
        self.metadata = ds[0].metadata()
        self.config.data.condition_dim = ds[0].y.shape[1]
        self.params = (self.in_dim_nodes, self.in_dim_edges, self.metadata)
    
    def train(self, ts):
        self.config.exp_name = ts
        self.ckpt = f'{ts}'
        print('\033[91m' + f'{self.ckpt}' + '\033[0m')

        # -------- Load models, optimizers, ema --------
        self.model, self.optimizer, self.scheduler = load_model_optimizer(self.params, self.config, self.device)
        self.model = load_train_model_from_ckpt(self.ckpt_dir, self.config.model.type, self.device) if self.config.model.from_checkpoint else self.model
        self.sampler = load_sampler(self.config)
        self.fid_model = load_fid_model_from_ckpt(self.ckpt_dir, self.device).eval() if self.config.model.type == 'autoencoder' else self.model
        self.latent_vae = load_latent_model_from_ckpt(self.ckpt_dir, self.device).eval() if self.config.model.type == 'latent_diffusionmodel' else self.model
        if self.config.data.data == 'Traffic' and self.config.train.training == False:
            self.fid_model = load_fid_model_from_ckpt(self.device).eval()
        self.ema = load_ema(self.model, decay=self.config.train.ema)

        logger = Logger(str(os.path.join(self.log_dir, f'{self.ckpt}.log')), mode='a')
        logger.log(f'{self.ckpt}', verbose=False)
        start_log(logger, self.config)
        train_log(logger, self.config, self.params)
        run = wandb.init(project= f'Generation - {self.config.data.data}', notes="", tags=["baseline1", "baseline2"])
        wandb.config = self.config

        self.loss_fn = load_loss_fn(self.config)    #returns loss function
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        if self.config.model.type == 'latent_diffusionmodel' and self.config.train.training:
            print('downsample to latent space')
            ds = self.train_loader.dataset
            ds_new = []
            x_max, x_min = None, None
            edge_attr_max, edge_attr_min = None, None
            for batch in self.train_loader:
                latent_tuple = load_batch(batch, device, second_load=True) 
                if self.config.data.data == 'Traffic':
                    latent_tuple[2][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')] = torch.zeros((latent_tuple[1][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')].shape[1], self.clean_in_dim_edges[('obstacle','obstacle_to_obstacle_temporal', 'obstacle')]), dtype=torch.float32, device=device)

                with torch.no_grad():
                    latent_x, _, latent_edge_attr, *_ = self.latent_vae.sample_mu_and_sig(*latent_tuple)#, graph.y.to(device).to(torch.float32))
                    latent_x_cpu = {node_type: latent_values.to('cpu') for node_type, latent_values in latent_x.items()}
                    latent_edge_attr_cpu = {edge_type: latent_values.to('cpu') for edge_type, latent_values in latent_edge_attr.items()}
                    latent_tuple_cpu = tuple({key: value.to('cpu') for key, value in dict.items()} for dict in latent_tuple[:-1])
                    latent_tuple_cpu += (latent_tuple[-1].to('cpu'),)

                    for i in range(batch.num_graphs):
                        graph = HeteroData()
                        for node_type, latent_values in latent_x_cpu.items():
                            graph[node_type].x= latent_values[latent_tuple_cpu[-3][node_type][i] : latent_tuple_cpu[-3][node_type][i + 1]]
                    
                        for edge_type, latent_values in latent_edge_attr_cpu.items():
                            batch_src = latent_tuple_cpu[-2][edge_type[0]]
                            batch_edge = batch_src[latent_tuple_cpu[1][edge_type][0]]
                            graph[edge_type].edge_index = latent_tuple_cpu[1][edge_type][:, batch_edge==i] - torch.tensor([latent_tuple_cpu[-3][edge_type[0]][i], latent_tuple_cpu[-3][edge_type[2]][i]]).unsqueeze(1)
                            graph[edge_type].edge_attr = latent_values[batch_edge==i]
                        graph.y = latent_tuple_cpu[-1][i,:].unsqueeze(0)
                        ds_new.append(graph)
            ds_new = CustomDataset(ds_new)
            self.train_loader = DataLoader(ds_new, batch_size=self.config.data.batch_size)

        # -------- Training --------
        earlystopping = [100, 0, 0]
        for epoch in trange(0, (self.config.train.num_epochs), desc = '[Epoch]', position = 1, leave=False):
            train_loss_x = []
            train_loss_edge_attr = []
            train_loss_misc = []
            t_start = time.time()
            self.latent_vae.eval()
            self.model.train()
            self.model.device = device

            if self.config.train.training:
                for _, train_b in enumerate(self.train_loader):
                    self.optimizer.zero_grad()
                    b_tuple = load_batch(train_b, self.device) 
                    latent_tuple = b_tuple
                    #b_tuple[2][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')] = torch.zeros((b_tuple[1][('obstacle','obstacle_to_obstacle_temporal', 'obstacle')].shape[1], 1), dtype=torch.float32, device=device)

                    if self.config.model.type == 'latent_diffusionmodelx':
                        with torch.no_grad():
                            latent_x, _, latent_edge_attr, *_ = self.latent_vae.sample_mu(*latent_tuple)
                            latent_tuple = (latent_x, latent_tuple[1], latent_edge_attr, *b_tuple[-3:])

                    gen_tuple = self.model(*latent_tuple)    # for VAE actual graphs, for diffusion only scores

                    loss, loss_x, loss_edge_attr, *losses_misc = self.loss_fn(latent_tuple, gen_tuple, self.model)
                    loss.backward()

                    if self.config.train.grad_norm > 0:
                        grad_norm = torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.config.train.grad_norm)
                    else:
                        grad_norm = 0
                    self.optimizer.step()

                    # -------- EMA update --------
                    self.ema.update(self.model.parameters())

                    train_loss_x.append(loss_x.item())
                    train_loss_edge_attr.append(loss_edge_attr.item())
                    train_loss_misc.append(losses_misc)

                

            if epoch % self.config.train.eval_interval == self.config.train.eval_interval-1 and epoch>=0:
                with torch.no_grad():
                    test_set_list, gen_set_list = [],[]
                    for k, test_b in enumerate(self.test_loader):
                        test_tuple = load_batch(test_b, self.device, second_load=True) 
                        self.test_graph_list = heterobatch2list(*test_tuple[:5])
                        if self.config.model.type in ['diffusionmodel', 'latent_diffusionmodel']: #sample graphs
                            self.model.eval()
                            gen_tuple = self.sampler.diff_sample(num=len(self.test_graph_list), model=self.model)
                            self.model.train()
                            if self.config.model.type == 'latent_diffusionmodel':       #transform back to observable space
                                batch_dict = gen_tuple[4]
                                batch_dict = {node_type: value.to(torch.int64) for node_type, value in batch_dict.items()}                             
                                latent_gen_tuple = self.latent_vae.decoder_hgt(*gen_tuple[:3], batch_dict, torch.zeros(test_tuple[-1].size(0), device=test_tuple[-1].device))
                                gen_tuple = (*latent_gen_tuple, *gen_tuple[-2:])
                            
                            self.gen_graph_list = heterobatch2list(*gen_tuple)      #convert to hetero data batrch object to list
                            val_score, nspdk_score, result = 1, 0, 0                          
                            test_set_list+=self.test_graph_list
                            gen_set_list+=self.gen_graph_list
                            logger.log(f'FID_full {result} , validity {val_score},  nspdk {nspdk_score}', verbose=True)
                            logger.log('='*100)
                            torch.cuda.empty_cache()
                            #break
                                                                        
                        if self.config.model.type in ['variationalautoencoder', 'latent_variationalautoencoder']:
                            self.model.eval()                   
                            gen_tuple = self.model.sample(*test_tuple)
                            self.model.train()
                            self.gen_graph_list = heterobatch2list(*gen_tuple)
                            val_score, nspdk_score, result = 1, 0, 0
                            test_set_list+=self.test_graph_list
                            gen_set_list+=self.gen_graph_list                            
                        
                    
                    if self.config.data.data == 'QM9' and not self.config.train.training:
                        torch.cuda.empty_cache()
                        result, val_score, nspdk_score = eval_fcd(test_set_list, gen_set_list, device)
                        train_set_list = []
                        for train_b in self.train_loader:
                            train_set_list += heterobatch2list(*load_batch(train_b, self.device)[:5])
                        novelty, uniqueness = eval_novelty(train_set_list, gen_set_list, device)
                        logger.log(f'FCD {result} , validity {val_score}, uniqueness {uniqueness}, novelty {novelty}, effective validity: {val_score*uniqueness*novelty}', verbose=True)
                        logger.log('='*150)
                        wandb.log({"FCD": result , "validity": val_score, "uniqueness": uniqueness, "novelty": novelty, "effective validity": val_score*uniqueness*novelty})

                    elif self.config.data.data == 'Traffic' and not self.config.train.training:
                        self.fid_model.eval()
                        classifier = self.fid_model
                        torch.cuda.empty_cache()
                        result = fid_as_mmd(test_set_list, gen_set_list, classifier, device=self.device, isTraffic=True)
                        train_set_list = []
                        for train_b in self.train_loader:
                            train_set_list += heterobatch2list(*load_batch(train_b, self.device, second_load=True)[:5])
                            if len(train_set_list) >= len(test_set_list):
                                train_set_list = train_set_list[:len(test_set_list)]
                                #break
                        result_baseline = fid_as_mmd(test_set_list, train_set_list, classifier, device=self.device, isTraffic=True)
                        logger.log(f'FID_Traffic {result}, FID_Traffic_baseline {result_baseline}', verbose=True)
                        logger.log('='*150)
                        wandb.log({"FID_Traffic": result, "FID_Traffic_baseline": result_baseline})
                    
                    elif self.config.data.data in ['Cora', 'Polblogs'] and not self.config.train.training:
                        result = eval_cora(test_set_list, gen_set_list, device)

                    elif not self.config.train.training:
                        for i, graph in enumerate(gen_set_list):
                            torch.save(graph.cpu(), f'/home/ws/unqwo/local_home_dir/Generated_scenario/heterodata{i}.pt')
                    
                    if earlystopping[2]>=4:
                        break
                    breakpoint=0
                
            mean_train_x = np.mean(train_loss_x)
            mean_train_edge_attr = np.mean(train_loss_edge_attr)
            mean_train_misc = tuple(np.mean(values) for values in zip(*train_loss_misc))
            mean_train_misc =  tuple(f'{num:.3e}' for num in mean_train_misc)
            
            log_dict = {f"train misc_{i}": float(value) for i, value in enumerate(mean_train_misc)}
            wandb.log({"train x": mean_train_x, "train edge_attr": mean_train_edge_attr, "grad_norm": grad_norm, **log_dict})

            if ((epoch % self.config.train.print_interval == self.config.train.print_interval-1) or epoch==0):
                # -------- Log losses --------
                logger.log(f'{epoch+1:03d} | {time.time()-t_start:.2f}s | '
                            f'train x: {mean_train_x:.3e} | train edge_attr: {mean_train_edge_attr:.3e} | '
                            f'grad_norm: {grad_norm:.2e} | train misc: {mean_train_misc}', verbose=True)
               
                           
        print(' ')
        torch.save({
        'model_state_dict': self.model.state_dict(),
        'config': self.config,
        'params': self.params,
         }, str(os.path.join(f'{self.ckpt_dir}',f'{self.ckpt}' + f'_{self.config.model.type}.pth')))
        wandb.finish()
        return self.ckpt
        