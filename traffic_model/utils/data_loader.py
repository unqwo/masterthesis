import pydot

import torch
from torch_geometric.data import Data, HeteroData

# load file
(dot_graph,) = pydot.graph_from_dot_file('/home/ws/unqwo/masterthesis/traffic_model/datasets_traffic/ESP_Barcelona-14_54_T-1_t100.dot')

# extract nodes
nodes = dot_graph.get_nodes()
node_attributes = {}
for node in nodes:
    name = node.get_name().strip('"')
    attrs = node.get_attributes()
    node_attributes[name] = attrs

for node, attrs in node_attributes.items():
    print(f"{node}: {attrs}")

# extract edges
edges = dot_graph.get_edges()
edge_attributes = []
for edge in edges:
    source = edge.get_source().strip('"')
    destination = edge.get_destination().strip('"')
    attrs = edge.get_attributes()
    edge_attributes.append((source, destination, attrs))

# seperate node types
obstacles = {}
ob_non_numeric_features_keys = ['type', 'role', 'subtype', 'traffic_light_state']
ob_non_numeric_features = {}
for j in ob_non_numeric_features_keys:
    ob_non_numeric_features[j] = set()
    
road_segments = {}
rs_non_numeric_features_keys = ['type', 'subtype', 'traffic_signs']
rs_non_numeric_features = {}
for j in rs_non_numeric_features_keys:
    rs_non_numeric_features[j] = set()

for key, value in node_attributes.items():
    if value.get('type') == 'obstacle':
        obstacles[key] = value
        for j in ob_non_numeric_features_keys:
            ob_non_numeric_features[j].add(value[j])
    elif value.get('type') == 'road_segment':
        road_segments[key] = value
        for j in rs_non_numeric_features_keys:
            rs_non_numeric_features[j].add(value[j])

# advise node "position" for each X tensor
obstacles_index = {name: i for i, name in enumerate(obstacles.keys())}
road_segments_index = {name: i for i, name in enumerate(road_segments.keys())}

# initialize hetero-data object
data = HeteroData()
data['obstacle'].num_nodes = len(obstacles)
data['road_segment'].num_nodes = len(road_segments)


############################################## ab hier weiter implementieren ############
# Füge Knoten zum HeteroData-Objekt hinzu
for node, attrs in obstacles.items():  
    index = node_index[node]
    for attr_name, attr_value in attrs.items():
        if attr_name != 'type':
            if isinstance(attr_value, list) or isinstance(attr_value, tuple):
                data['obstacles'][attr_name].append(attr_value)
            else:
                if attr_name not in data[node_type]:
                    data[node_type][attr_name] = []
                data[node_type][attr_name].append([attr_value])

# Konvertiere Knotenattribute in Tensoren
for node_type in data.node_types:
    for attr_name in data[node_type]:
        if isinstance(data[node_type][attr_name][0], list):
            data[node_type][attr_name] = torch.tensor(data[node_type][attr_name], dtype=torch.float)
        else:
            data[node_type][attr_name] = torch.tensor(data[node_type][attr_name], dtype=torch.float).view(-1, 1)

# Füge Kanten zum HeteroData-Objekt hinzu
edge_index_dict = {
    ('road', 'road_to_road', 'road'): ([], []),
    ('obstacle', 'obstacle_to_road', 'road'): ([], []),
    ('obstacle', 'obstacle_to_obstacle', 'obstacle'): ([], [])
}

edge_attrs = {key: {} for key in edge_index_dict.keys()}

for source, destination, attrs in edge_attributes:
    src_type = node_attributes[source]['type']
    dst_type = node_attributes[destination]['type']
    edge_type = attrs['type']  # Extrahiere den Kanten-Typ
    
    if (src_type, edge_type, dst_type) in edge_index_dict:
        edge_index_dict[(src_type, edge_type, dst_type)][0].append(node_index[source])
        edge_index_dict[(src_type, edge_type, dst_type)][1].append(node_index[destination])
        
        for attr_name, attr_value in attrs.items():
            if attr_name != 'type':
                if attr_name not in edge_attrs[(src_type, edge_type, dst_type)]:
                    edge_attrs[(src_type, edge_type, dst_type)][attr_name] = []
                edge_attrs[(src_type, edge_type, dst_type)][attr_name].append(attr_value)

# Konvertiere Kantenindexe und Kantenattribute in Tensoren und füge sie dem HeteroData-Objekt hinzu
for key, (src_list, dst_list) in edge_index_dict.items():
    if src_list:
        edge_index = torch.tensor([src_list, dst_list], dtype=torch.long)
        data[key].edge_index = edge_index
        for attr_name, attr_values in edge_attrs[key].items():
            if isinstance(attr_values[0], list):
                data[key][attr_name] = torch.tensor(attr_values, dtype=torch.float)
            else:
                data[key][attr_name] = torch.tensor(attr_values, dtype=torch.float).view(-1, 1)

print(data)


























# Extrahiere die Knoten und Kanten aus dem pydot-Graphen
nodes = dot_graph.get_nodes()
edges = dot_graph.get_edges()

# Erstelle eine Mapping von Node-Name zu Index
node_indices = {node.get_name(): i for i, node in enumerate(nodes)}

# Erstelle Kantenlisten
edge_index = []
for edge in edges:
    src = node_indices[edge.get_source()]
    dst = node_indices[edge.get_destination()]
    edge_index.append([src, dst])

# Konvertiere zu einem Tensor
edge_index = torch.tensor(edge_index, dtype=torch.long).t().contiguous()

# Erstelle das Data-Objekt
data = Data(edge_index=edge_index)

# Optional: Füge Knotenattribute hinzu (hier Dummy-Attribute)
num_nodes = len(nodes)
data.num_nodes = num_nodes
node_attr = []
data.x = nodes.get_attributes()

print(data)

# Beispielhafte Knoten- und Kantentypen
node_types = ['typeA', 'typeB']
edge_types = [('typeA', 'to', 'typeB'), ('typeB', 'to', 'typeA')]

# Erstelle das HeteroData-Objekt
hetero_data = HeteroData()

# Füge Knoten hinzu
for node_type in node_types:
    hetero_data[node_type].num_nodes = num_nodes // 2  # Beispielhaft

# Füge Kanten hinzu
edge_index_dict = {
    ('typeA', 'to', 'typeB'): edge_index[:, :num_nodes // 2],
    ('typeB', 'to', 'typeA'): edge_index[:, num_nodes // 2:]
}

for edge_type, index in edge_index_dict.items():
    hetero_data[edge_type].edge_index = index

# Optional: Füge Knoten- und Kantenattribute hinzu
hetero_data['typeA'].x = torch.ones(num_nodes // 2, 1)  # Beispielhafte Knotenattribute
hetero_data['typeB'].x = torch.ones(num_nodes // 2, 1)  # Beispielhafte Knotenattribute

print(hetero_data)