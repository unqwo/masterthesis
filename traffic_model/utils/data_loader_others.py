import os
import networkx as nx
import torch
import torch_geometric.datasets as datasets # type: ignore
import torch_geometric.data as geo_data # type: ignore
import numpy as np
from torch_geometric.utils import dense_to_sparse, to_undirected, subgraph, to_networkx
import networkx as nx
from torch_geometric.data import HeteroData
from torch_geometric.loader import DataLoader
from torch_geometric.utils import coalesce
from utils.dsloader import Zinc_ds_deliverer, Qm9_ds_deliverer

import sys
import os
import logging

import torch
from torch import Tensor
from torch.utils.data import random_split
from torch_geometric.loader import DataLoader
import torch.nn.functional as F
import torch_geometric.transforms as T
import numpy as np
from tqdm import tqdm, trange
sys.path.append('/home/ws/unqwo/supplementary/phd')  # https://gitlab.kit.edu/ferdinand.muetsch/phd
from learning.data.data import NuplanDataset
from lib import transform


class CustomDataset(geo_data.Dataset):
    def __init__(self, data_list):
        super().__init__()
        self.data_list = data_list

    def len(self):
        return len(self.data_list)

    def get(self, idx):
        return self.data_list[idx]

def get_dataloader(config, get_graph_list=False):
    ds_name = config.data.data
    dataset = get_data_set(ds_name)
    train_dataset, test_dataset = random_split(dataset, [0.8, 0.2])
    test_dataset = test_dataset#[:256*12]
    train_loader = DataLoader(train_dataset, batch_size=config.data.batch_size)
    test_loader = DataLoader(test_dataset, batch_size=config.data.batch_size)
    return train_loader, test_loader

    
def get_data_set(name) -> geo_data.Dataset:
    
    if name=='MovieLens100K':
        if name == 'OGB_MAG':
            ds_name = datasets.OGB_MAG
        elif name == 'DBLP':
            ds_name = datasets.HGBDataset

        graph_dataset = datasets.MovieLens100K(root='/home/ws/unqwo/masterthesis/traffic_model/datasets')
        graph_data = graph_dataset[0]

        x, e, b = graph_data['user'].x, graph_data[('user', 'rates', 'movie')].edge_label_index, graph_data[('user', 'rates', 'movie')].edge_label

        start_nodes = torch.randint(0, x.size(0), (500,))
        random_walks = hetero_random_walk(graph_data, start_nodes)
        subgraphs = extract_subgraphs(graph_data, random_walks)

        graph_dataset = CustomDataset(subgraphs)
    
    elif name == 'Zinc':
        graph_dataset = get_Zinc_hetero()
    elif name == 'QM9':
        graph_dataset = get_QM9_hetero()
    elif name == 'Cora':
        graph_dataset = get_Cora_hetero()
    elif name == 'Polblogs':
        graph_dataset = get_Polblogs_hetero()
    
    elif name=='Traffic':
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        BASE_PATH = '/pool/pu0288/data/nuplan_graphs/scenarios_downsampled'
        # BASE_PATH = '/home/ws/unqwo/local_home_dir/data/nuplan_graphs2'
        #graph_dataset = get_Traffic_data(BASE_PATH)
        graph_dataset: NuplanDataset = NuplanDataset(
            BASE_PATH,
            max_n=-1,
            no_multi=False,
            in_memory=False,
            # pre_transform=T.Compose([T.NormalizeFeatures()]),
            pre_transform=None,
            pre_filter=None,
            transform=T.Compose([
                transform.UnifyDtypes(dtype=torch.float32, replace_nan=True),
                # T.RemoveIsolatedNodes(),  # not working if one edge store is empty
            ]),
            objective='scenario_classification',
            min_obstacles=0,
            positional_encoding='sinusoidal',
            minmax_scaling=True,
            force_reload=False,
            device=device,
        )

    return graph_dataset

def hetero_random_walk(data, start_nodes):
    walks = []
    for start_node in start_nodes:
        current_node = start_node
        current_type = 'movie'
        walk = [(current_node.item(), current_type)]
        walk_length = torch.randint(50, 80, (1,)).item()
        for _ in range(walk_length - 1):
            edge_type = data.edge_types[0]
            edge_index = data[edge_type].edge_label_index
            neighbors = edge_index[1][edge_index[0] == current_node] if current_type=='user' else edge_index[0][edge_index[1] == current_node]
            if neighbors.size(0) == 0:
                break  
            next_node = neighbors[torch.randint(0, neighbors.size(0), (1,)).item()]
            if current_type == 'user':
                current_type = 'movie'
            else:
                current_type = 'user'
            current_node = next_node
            walk.append((current_node.item(), current_type))
        walks.append(walk)
    return walks

def extract_subgraphs(data, walks):
    subgraphs = []
    for walk in walks:
        subgraph = HeteroData()
        walk = np.array(walk)
        movie_mask = np.array([walk[i][1] == 'movie' for i in range(len(walk))])
        movies = torch.tensor(walk[movie_mask][:,0].astype(int).tolist())
        users = torch.tensor(walk[~movie_mask][:,0].astype(int).tolist())

        movies = torch.unique(movies)
        users = torch.unique(users)
        edge_type = data.edge_types[0]
        edge_mask = torch.isin(data[edge_type].edge_label_index[0], users) & torch.isin(data[edge_type].edge_label_index[1], movies)

        if movies.shape[0] == 0 or users.shape[0] == 0:
            continue
        subgraph['movie'].x = data['movie'].x[movies]
        subgraph['user'].x = data['user'].x[users]

        subgraph[edge_type].edge_index = data[edge_type].edge_label_index[:, edge_mask]
        mapping_0 = {old_idx: new_idx for new_idx, old_idx in enumerate(users.tolist())}
        mapping_1 = {old_idx: new_idx for new_idx, old_idx in enumerate(movies.tolist())}
        row_0 = torch.tensor([mapping_0[idx.item()] for idx in subgraph[edge_type].edge_index[0]])
        row_1 = torch.tensor([mapping_1[idx.item()] for idx in subgraph[edge_type].edge_index[1]])
        subgraph[edge_type].edge_index = torch.stack((row_0, row_1), dim=0)
        subgraph[data.edge_types[1]].edge_index = torch.stack((row_1, row_0), dim=0)

        subgraph[edge_type].edge_attr = data[edge_type].edge_label[edge_mask].unsqueeze(1)
        subgraph[data.edge_types[1]].edge_attr = data[edge_type].edge_label[edge_mask].unsqueeze(1)

        subgraph[edge_type].edge_index, subgraph[edge_type].edge_attr = coalesce(subgraph[edge_type].edge_index, subgraph[edge_type].edge_attr, reduce='mean', is_sorted=False)
        subgraph[data.edge_types[1]].edge_index, subgraph[data.edge_types[1]].edge_attr = coalesce(subgraph[data.edge_types[1]].edge_index, subgraph[data.edge_types[1]].edge_attr, reduce='mean', is_sorted=False)

        subgraphs.append(subgraph)
    return subgraphs

def get_Zinc_hetero():
    dataset_homo = Zinc_ds_deliverer().get_special_ds()

    graph_list = []
    for g in dataset_homo:
        hetero_graph = HeteroData()

        hetero_graph['atom_0'].x = g.x
        hetero_graph['atom_1'].x = g.x

        hetero_graph['atom_0', 'connected', 'atom_1'].edge_index = g.edge_index
        hetero_graph['atom_1', 'reconnected', 'atom_0'].edge_index = g.edge_index

        hetero_graph['atom_0', 'connected', 'atom_1'].edge_attr = g.edge_attr
        hetero_graph['atom_1', 'reconnected', 'atom_0'].edge_attr = g.edge_attr

        graph_list.append(hetero_graph)

    return CustomDataset(graph_list)

def get_QM9_hetero():
    dataset_homo = Qm9_ds_deliverer().get_special_ds()
    graph_list = []
    for g in dataset_homo:
        hetero_graph = HeteroData()

        hetero_graph['atom'].x = g.x

        hetero_graph['atom', 'connected', 'atom'].edge_index = torch.cat([g.edge_index], dim=1)
        hetero_graph['atom', 'connected', 'atom'].edge_attr = torch.cat([g.edge_attr], dim=0)

        hetero_graph.y = g.y

        graph_list.append(hetero_graph)
    
    return CustomDataset(graph_list)

def get_Cora_hetero():
    pyg_graph_homo = torch.load('/home/ws/unqwo/masterthesis/traffic_model/datasets/cora/cora_graph.pt')
    graph_list = []

    hetero_graph = HeteroData()
    hetero_graph['doc'].x = torch.zeros((pyg_graph_homo.num_nodes, 1))

    hetero_graph['doc', 'cites', 'doc'].edge_index = pyg_graph_homo.edge_index
    hetero_graph['doc', 'cites', 'doc'].edge_attr = torch.zeros((pyg_graph_homo.edge_index.shape[1], 1))

    hetero_graph.y = torch.ones((1,1))

    graph_list = [hetero_graph]*1024

    return CustomDataset(graph_list)

def get_Polblogs_hetero():
    pyg_graph_homo = torch.load('/home/ws/unqwo/masterthesis/traffic_model/datasets/polblogs/polblogs_graph.pt')
    graph_list = []

    hetero_graph = HeteroData()
    hetero_graph['doc'].x = torch.ones((pyg_graph_homo.num_nodes, 1))

    hetero_graph['doc', 'cites', 'doc'].edge_index = pyg_graph_homo.edge_index
    hetero_graph['doc', 'cites', 'doc'].edge_attr = torch.ones((pyg_graph_homo.edge_index.shape[1], 1))

    hetero_graph.y = torch.ones((1,1))

    graph_list = [hetero_graph]*1024

    return CustomDataset(graph_list)

def get_Traffic_data(root):
    hetero_data_list = []

    for filename in os.listdir(root):
        if filename.endswith(".gpickle.gz"):
            file_path = os.path.join(root, filename)
            hetero_data = gpickle_to_heterodata(file_path)
            hetero_data_list.append(hetero_data)

    return CustomDataset(hetero_data_list)

def gpickle_to_heterodata(file_path):
    graph = nx.read_gpickle(file_path)
    data = HeteroData()

    node_types = set(nx.get_node_attributes(graph, 'type').values())
    
    for node_type in node_types:
        node_indices = [n for n, d in graph.nodes(data=True) if d['type'] == node_type]
        if node_indices:  # Falls der Knotentyp existiert
            features = [graph.nodes[n].get('feature', []) for n in node_indices]
            data[node_type].x = torch.tensor(features, dtype=torch.float)
            data[node_type].num_nodes = len(node_indices)
    
    for edge in graph.edges(data=True):
        source_node, target_node, edge_data = edge
        source_type = graph.nodes[source_node]['type']
        target_type = graph.nodes[target_node]['type']
        edge_type = edge_data.get('type', 'default')
        
        edge_indices = (source_node, target_node)
        edge_index = torch.tensor(edge_indices, dtype=torch.long).view(2, -1)
        
        for attr_name, attr_value in edge_data.items():
            if attr_name != 'type':
                data[(source_type, edge_type, target_type)][attr_name] = torch.tensor([attr_value], dtype=torch.float)

        if (source_type, edge_type, target_type) in data.edge_types:
            data[(source_type, edge_type, target_type)].edge_index = torch.cat([data[(source_type, edge_type, target_type)].edge_index, edge_index], dim=1)
        else:
            data[(source_type, edge_type, target_type)].edge_index = edge_index
    
    return data


if __name__ == '__main__':
    get_data_set(name = 'QM9')