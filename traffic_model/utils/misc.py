        # Step3: node predicition/node classification to determine active nodes, for every edge type
        # active_prob_dict={}
        # mask_pred_active_nodes_dict = {}
        # for edge_type, src_layer in self.mlp_active_nodes_src.items():
        #     edge_type = string_to_tuple(edge_type)
        #     src = edge_type[0]
        #     dst = edge_type[2]
        #     src_active_prob = src_layer(x_t_hat_dict[src])
        #     mask_src_active = torch.bernoulli(src_active_prob).long().bool()
        #     dst_active_prob = self.mlp_active_nodes_dst[tuple_to_string(edge_type)](x_t_hat_dict[dst])
        #     mask_dst_active = torch.bernoulli(dst_active_prob).long().bool()
        #     active_prob_dict[edge_type] = (src_active_prob, dst_active_prob)
        #     mask_pred_active_nodes_dict[edge_type] = (mask_src_active, mask_dst_active)     #(2, num_nodes)

#losses
        # # Loss node prediction (activeness)
        # real, pred = model.misc['for loss_active']
        # loss_node_active = loss_heterodict_reconstruction_tup(real, pred, torch.nn.BCELoss(reduction='mean')) 

# def loss_heterodict_reconstruction_tup(real_dict, gen_dict, loss_fn):
#     loss = 0
#     for key, value in real_dict.items():
#         loss += loss_fn(gen_dict[key][0].squeeze(), value[0].float())*0.5
#         loss += loss_fn(gen_dict[key][1].squeeze(), value[1].float())*0.5
#     loss /= len(real_dict.keys())
#     return loss
from torch_geometric.data import HeteroData
import torch


def heterobatch2list(x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict):
    graph_list = []
    num_graphs = len(next(iter(ptr_dict.values()))) - 1

    for g in range(num_graphs):
        graph = HeteroData()
        for node_type, value in x_dict.items():
            graph[node_type].x = value[batch_dict[node_type]==g] 

        for edge_type, value in edge_index_dict.items():
            src_type = edge_type[0]
            dst_type = edge_type[2]
            edge_mask = (value[0] >= ptr_dict[src_type][g]) & (value[0] < ptr_dict[src_type][g+1]) & \
                        (value[1] >= ptr_dict[dst_type][g]) & (value[1] < ptr_dict[dst_type][g+1])
            src_offset = ptr_dict[src_type][g]
            dst_offset = ptr_dict[dst_type][g]
            edge_index = value[:, edge_mask]
            edge_index[0] = edge_index[0] - src_offset
            edge_index[1] = edge_index[1] - dst_offset
            graph[edge_type].edge_index = edge_index
            graph[edge_type].edge_attr = edge_attr_dict[edge_type][edge_mask]
        graph_list.append(graph)
    
    return graph_list        
        
# def match_masking(base_edge_index, check_edge_index):
#     base_edge_index = base_edge_index.unsqueeze(1)
#     check_edge_index = check_edge_index.unsqueeze(2)
#     matches = (base_edge_index == check_edge_index).all(dim=0)
#     return matches.any(dim=1)

def match_masking(base_edge_index, check_edge_index):

    if base_edge_index.numel() == 0 or check_edge_index.numel() == 0:
        return torch.zeros(check_edge_index.size(1), dtype=torch.bool, device=check_edge_index.device)
    
    K = base_edge_index.max().item() + 1
    D = base_edge_index.shape[0]
    
    K_powers = K ** torch.arange(D - 1, -1, -1, device=base_edge_index.device)
    
    base_edge_hashes = (base_edge_index * K_powers[:, None]).sum(dim=0)
    check_edge_hashes = (check_edge_index * K_powers[:, None]).sum(dim=0)
    
    base_edge_hashes_sorted, _ = torch.sort(base_edge_hashes)
    
    positions = torch.searchsorted(base_edge_hashes_sorted, check_edge_hashes, right=False)
    positions = positions.clamp(max=base_edge_hashes_sorted.size(0) - 1)

    matches = base_edge_hashes_sorted[positions] == check_edge_hashes
    return matches

def count_occurence_node(node_type, edge_types):
    count = 0
    for edge_type in edge_types:
        if node_type==edge_type[0]:
            count+=1
        if node_type==edge_type[2]:
            count+=1
    return count

def insert_tensor_in_list(node_type, ten, dict_list):
    count = 0
    for edge_type, deg_list in dict_list.items():
        if node_type==edge_type[0]:
            deg_list[0] = ten[:, count]
            count+=1
        if node_type==edge_type[2]:
            deg_list[1] = ten[:, count]
            count+=1
    return dict_list

def tuple_to_string(key):
    key = '__'.join(key)
    return key

def string_to_tuple(key):
    key = tuple(key.split('__'))
    return key

def get_max_min_dict(current_dict, max_dict, min_dict):
    if max_dict==None:
        max_dict={key: value.max(dim=0)[0] for key, value in current_dict.items()}
    if min_dict==None:
        min_dict={key: value.min(dim=0)[0] for key, value in current_dict.items()}
    for key, value in current_dict.items():
        current_max = value.max(dim=0)[0]
        current_min = value.min(dim=0)[0]
        max_dict[key] = torch.max(current_max, max_dict[key])
        min_dict[key] = torch.min(current_min, min_dict[key])
    return max_dict, min_dict

def scale_latent(graph, x_max, x_min, edge_attr_max, edge_attr_min):
    for node_type in graph.node_types:
        graph[node_type].x = (graph[node_type].x - x_min[node_type].to('cpu'))/(x_max[node_type].to('cpu')-x_min[node_type].to('cpu'))
    for edge_type in graph.edge_types:
        graph[edge_type].edge_attr = (graph[edge_type].edge_attr - edge_attr_min[edge_type].to('cpu'))/(edge_attr_max[edge_type].to('cpu')-edge_attr_min[edge_type].to('cpu'))
    
def rescale_latent(gen_tuple, x_max, x_min, edge_attr_max, edge_attr_min):
    for node_type in gen_tuple[0].keys():
        gen_tuple[0][node_type] = gen_tuple[0][node_type]*(x_max[node_type]-x_min[node_type]) + x_min[node_type]
    for edge_type in gen_tuple[1].keys():
        gen_tuple[2][edge_type] = gen_tuple[2][edge_type]*(edge_attr_max[edge_type]-edge_attr_min[edge_type]) + edge_attr_min[edge_type]

def batch_norm_dict(z_dict):
    for key, value in z_dict.items():
        corr = torch.dot(value[:,0], value[:,1]) / (torch.norm(value[:,1]) * torch.norm(value[:,0]))
        corr_2 = torch.dot(torch.randn_like(value[:,0]), torch.randn_like(value[:,1])) / (torch.norm(torch.randn_like(value[:,1])) * torch.norm(torch.randn_like(value[:,0])))
        mean = value.mean(dim=0)
        std = value.std(dim=0)
        z_dict[key] = (value - mean) / std
    return z_dict