import os, glob, sys
import torch
import random
import numpy as np
import math
from datetime import datetime

# from models.transformer import GraphTransformer, GraphTransformer_Mol
from sde import VPSDE, VESDE, subVPSDE

from losses import get_loss_fn
# from solver import get_pc_sampler, S4_solver
##from evaluation.mmd import gaussian, gaussian_emd
from utils.ema import ExponentialMovingAverage

sys.path.append('/home/ws/unqwo/supplementary/phd')
from parser import nuplan_types
from learning.model import DeeptestSimpleGNN


def load_seed(seed):
    # Random Seed
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    return seed


def load_device():
    if torch.cuda.is_available():
        device = list(range(torch.cuda.device_count()))
    else:
        device = 'cpu'
    return device

def load_model(params, config):
    import models.models as models
    model_type = config.model.type
    if model_type == 'autoencoder':
        model = models.Autoencoder(config, *params)
    elif model_type == 'variationalautoencoder':
        model = models.Variationalautoencoder(config, *params)
    elif model_type == 'diffusionmodel' or model_type=='latent_diffusionmodel':
        model = models.Diffusionmodel(config, *params)
    elif model_type == 'diffusionmodel2':
        model = models.Diffusionmodel2(config, *params)
    elif model_type == 'latent_variationalautoencoder':
        model = models.Latent_Variationalautoencoder(config, *params)
    else:
        raise ValueError(f"Model Name <{model_type}> is Unknown")
    return model


def load_model_optimizer(params, config, device):
    config_train = config.train
    model = load_model(params, config)
    if isinstance(device, list):
        if len(device) > 1:
            model = torch.nn.DataParallel(model, device_ids=device)
        model = model.to(f'cuda:{device[0]}')
    if config_train.optimizer=='adamw':
        optimizer = torch.optim.AdamW(model.parameters(), lr=config_train.lr, amsgrad=True,
                                        weight_decay=config_train.weight_decay)
    else:
        raise NotImplementedError(f'Optimizer:{config_train.optimizer} not implemented.')
    scheduler = None
    if config_train.lr_schedule:
        num_epochs = config_train.num_epochs
        dataset_size = 6645.6
        batch_size = config.data.batch_size
        steps_per_epoch = math.ceil(dataset_size / batch_size)
        num_training_steps = steps_per_epoch * num_epochs  # Gesamtanzahl der Schritte
        num_warmup_steps = int(num_training_steps * 0.05)
    
        def lr_lambda(current_step):
            if current_step < num_warmup_steps:
                # Warmup-Phase: lineare Erhöhung der Lernrate
                return current_step / num_warmup_steps
            else:
                # Cosine Annealing
                progress = (current_step - num_warmup_steps) / (num_training_steps - num_warmup_steps)
                return 0.5 * (1 + math.cos(math.pi * progress))

        #scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=config_train.lr_decay)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=num_epochs*0.51, eta_min=config_train.lr/50)
    return model, optimizer, scheduler

def load_ema(model, decay=0.999):
    ema = ExponentialMovingAverage(model.parameters(), decay=decay)
    return ema


def load_ema_from_ckpt(model, ema_state_dict, decay=0.999):
    ema = ExponentialMovingAverage(model.parameters(), decay=decay)
    ema.load_state_dict(ema_state_dict)
    return ema


def load_data(config, get_graph_list=False):
    from utils.data_loader_others import get_dataloader
    return get_dataloader(config, get_graph_list)


def load_batch(batch, device, second_load=False):
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device

    x_dict = {node_type: batch[node_type].x.to(device_id).to(torch.float32) for node_type in batch.node_types}
    edge_index_dict = {edge_type: batch[edge_type].edge_index.to(device_id) for edge_type in batch.edge_types}
    edge_attr_dict = {edge_type: batch[edge_type].edge_attr.to(device_id).to(torch.float32) for edge_type in batch.edge_types}
    if ('obstacle','obstacle_to_obstacle_temporal', 'obstacle') in edge_index_dict.keys():
        edge_attr_dict[('obstacle','obstacle_to_obstacle_temporal', 'obstacle')] = torch.zeros((edge_index_dict[('obstacle','obstacle_to_obstacle_temporal', 'obstacle')].shape[1] , 1), dtype=torch.float32, device=device_id) if second_load else edge_attr_dict[('obstacle','obstacle_to_obstacle_temporal', 'obstacle')]
        #edge_attr_dict[('obstacle','obstacle_to_obstacle_temporal', 'obstacle')] = torch.zeros((0, 1), dtype=torch.float32, device=device_id) if second_load else torch.zeros((0, 16), dtype=torch.float32, device=device_id)
    ptr_dict = {node_type: batch[node_type].ptr.to(device_id) for node_type in batch.node_types}
    batch_dict = {node_type: batch[node_type].batch.to(device_id) for node_type in batch.node_types}
    if hasattr(batch, 'y'):
        y = batch.y.to(device_id)
        if not second_load and not ('obstacle','obstacle_to_obstacle_temporal', 'obstacle') in edge_index_dict.keys() and not ('doc', 'cites', 'doc') in edge_index_dict.keys():
            mean = y.mean(dim=0)
            std = y.std(dim=0)
            y = (y - mean) / std

        return x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict, y
    else:
        return x_dict, edge_index_dict, edge_attr_dict, ptr_dict, batch_dict


def load_sde(config_sde):
    sde_type = config_sde.type
    beta_min = config_sde.beta_min
    beta_max = config_sde.beta_max
    num_scales = config_sde.num_scales

    if sde_type == 'VP':
        sde = VPSDE(beta_min=beta_min, beta_max=beta_max, N=num_scales)
    elif sde_type == 'VE':
        sde = VESDE(sigma_min=beta_min, sigma_max=beta_max, N=num_scales)
    elif sde_type == 'subVP':
        sde = subVPSDE(beta_min=beta_min, beta_max=beta_max, N=num_scales)
    else:
        raise NotImplementedError(f"SDE class {sde_type} not yet supported.")
    return sde


def load_loss_fn(config):
    model_type = config.model.type
    loss_fn = get_loss_fn(model_type)
    return loss_fn


# def load_sampling_fn(config_train, config_module, config_sample, device):
#     sde_x = load_sde(config_train.sde.x)
#     sde_adj = load_sde(config_train.sde.adj)
#     max_node_num  = config_train.data.max_node_num

#     device_id = f'cuda:{device[0]}' if isinstance(device, list) else device

#     if config_module.predictor == 'S4':
#         get_sampler = S4_solver
#     else:
#         get_sampler = get_pc_sampler

#     if config_train.data.data in ['QM9', 'ZINC250k']:
#         shape_x = (config_sample.batch_size, max_node_num, config_train.data.max_feat_num)
#         shape_adj = (config_sample.batch_size, max_node_num, max_node_num)
#     else:
#         shape_x = (config_sample.batch_size, max_node_num, config_train.data.max_feat_num)
#         shape_adj = (config_sample.batch_size, max_node_num, max_node_num)
        
#     sampling_fn = get_sampler(sde_x=sde_x, sde_adj=sde_adj, shape_x=shape_x, shape_adj=shape_adj, 
#                                 predictor=config_module.predictor, corrector=config_module.corrector,
#                                 snr=config_module.snr, scale_eps=config_module.scale_eps, 
#                                 n_steps=config_module.n_steps, 
#                                 probability_flow=config_sample.probability_flow, 
#                                 continuous=True, denoise=config_sample.noise_removal, 
#                                 eps=config_sample.eps, device=device_id)
#     return sampling_fn

def load_sampler(config):
    from sampler import Diff_Sampler
    return Diff_Sampler(config)

def load_model_params(config):
    return config

def get_latest_model(ckpt_dir, model_type):
    search_pattern = os.path.join(ckpt_dir, f'*_{model_type}.pth')
    model_files = glob.glob(search_pattern)
    if not model_files:
        raise FileNotFoundError("model not found")
    def extract_datetime_from_filename(filename):
        base_name = os.path.basename(filename)
        date_time_str = base_name.split('_')[0]
        return datetime.strptime(date_time_str, '%b%d-%H:%M:%S')

    latest_model = max(model_files, key=extract_datetime_from_filename)
    
    return latest_model

def load_train_model_from_ckpt(ckpt_dir, model_type, device):
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device
    path = get_latest_model(ckpt_dir, model_type)
    ckpt_dict = torch.load(path, map_location=device_id)
    print(f'{path} loaded')
    model = load_model_from_ckpt(ckpt_dict['params'], ckpt_dict['config'], ckpt_dict['model_state_dict'], device)
    return model

def load_fid_model_from_ckpt(device):
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device
    model = DeeptestSimpleGNN(
        n_classes=len(nuplan_types.scenario_types),
        include_edge_attrs=True,
        hidden_dim_obstacles_1=32,
        hidden_dim_road_1=32,
    ).to(device_id)
    path = '/home/ws/unqwo/classifier/20241108_050834_scenario_edgeattr_scaled_sinusoidal_0.569.pt'
    model.load_state_dict(torch.load(path, map_location=device_id))
    print(f'{path} loaded')
    return model

def load_latent_model_from_ckpt(ckpt_dir, device):
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device
    path = get_latest_model(ckpt_dir, 'latent_variationalautoencoder')
    ckpt_dict = torch.load(path, map_location=device_id)
    print(f'{path} loaded')
    model = load_model_from_ckpt(ckpt_dict['params'], ckpt_dict['config'], ckpt_dict['model_state_dict'], device)
    return model


def load_ckpt(config, device):
    device_id = f'cuda:{device[0]}' if isinstance(device, list) else device
    path = f'checkpoints/{config.data.data}/{config.ckpt}.pth'
    ckpt_dict = torch.load(path, map_location=device_id)
    print(f'{path} loaded')
    return ckpt_dict


def load_model_from_ckpt(params, config, state_dict, device):
    model = load_model(params, config)
    if 'module.' in list(state_dict.keys())[0]:
        # strip 'module.' at front; for DataParallel models
        state_dict = {k[7:]: v for k, v in state_dict.items()}
    model.load_state_dict(state_dict)
    if isinstance(device, list):
        if len(device) > 1:
            model = torch.nn.DataParallel(model, device_ids=device)
        model = model.to(f'cuda:{device[0]}')
    return model


def load_opt_from_ckpt(config_train, state_dict, model):
    if config_train.optimizer=='adamw':
        optimizer = torch.optim.AdamW(model.parameters(), lr=config_train.lr, amsgrad=True,
                                        weight_decay=config_train.weight_decay)
    else:
        raise NotImplementedError(f'Optimizer:{config_train.optimizer} not implemented.')
    optimizer.load_state_dict(state_dict)
    return optimizer

# def load_eval_settings(data, kernel='emd'):
#     # Settings for general graph generation
#     methods = ['degree', 'cluster', 'orbit', 'spectral', 'connected'] 
#     kernels = {'degree': kernel, 
#                 'cluster': kernel, 
#                 'orbit': kernel,
#                 'spectral': kernel}
#     if data == 'sbm':
#         try:
#             import graph_tool.all as gt
#             methods.append('eval_sbm')
#         except:
#             pass
#     elif data == 'planar':
#         methods.append('eval_planar')
#     return methods, kernels