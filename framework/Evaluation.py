import concurrent.futures
from multiprocessing import Pool
import torch
import numpy as np
import networkx as nx
import random
import util
import torch.nn as nn
import torch.optim as optim
import tqdm as tqdm
from torch_geometric.data import HeteroData, Data, DataLoader
import torch_geometric.utils as pyg_utils
from torch_geometric.nn import GCNConv, global_mean_pool
from sklearn.metrics import f1_score, accuracy_score
from sklearn.model_selection import train_test_split
from evaluation.stats import eval_graph_list


def evaluator(data_real, data_gen, num_graphs):
    global device
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('--Evaluation--')
    graph_ref_list = random.choices(dataset2list(data_real), k=num_graphs)
    graph_pred_list = dataset2list(data_gen)
    print('--converted--')

    # statistics_based = {'degree_stats':         GraphMetrics.degree_stats(graph_ref_list, graph_pred_list),
    #                     'clustering_stats':     GraphMetrics.clustering_stats(graph_ref_list, graph_pred_list),
    #                     #'orbit_stats_all_mp':   GraphMetrics.orbit_stats_all_mp(graph_ref_list, graph_pred_list)
    #                     }

    # classifier_based = {'cb':                      GraphMetrics.graph_classifier(data_real=data_real, data_fake=data_gen),
    #                     }

    res = eval_graph_list(graph_ref_list, graph_pred_list)

    return res

def dataset2list(data_set):
    if isinstance(data_set[0], HeteroData):
        tens_list = []
        for data in data_set:
            data.cpu()
            ten=np.array([pyg_utils.to_dense_adj(data[edge_type].edge_index) for edge_type in data.edge_types])
            tmp = ten[0].numpy().squeeze(0)     #here only first edge type taken
            graph = nx.from_numpy_array(tmp)
            if graph.number_of_nodes() > 0:
                tens_list.append(graph)
        return tens_list
    elif isinstance(data_set[0], Data):
        tens_list = []
        for data in data_set:
            data.cpu()
            adj = pyg_utils.to_dense_adj(data.edge_index).numpy().squeeze(0)
            graph = nx.from_numpy_array(adj)
            if graph.number_of_nodes() > 0:
                tens_list.append(graph)
        return tens_list
    else:
        raise Exception


class GraphMetrics():
    
    @staticmethod
    def degree_stats(graph_ref_list, graph_pred_list, is_parallel=True, max_workers=None):
        ''' Compute the distance between the degree distributions of two unordered sets of graphs.
        Args:
        graph_ref_list, graph_target_list: two lists of networkx graphs to be evaluated
        '''
        sample_ref = []
        sample_pred = []        

        if is_parallel:
            with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
                for deg_hist in executor.map(GraphMetrics.degree_worker, graph_ref_list):
                    sample_ref.append(deg_hist)
            with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
                for deg_hist in executor.map(GraphMetrics.degree_worker, graph_pred_list):
                    sample_pred.append(deg_hist)

        else:
            for i in range(len(graph_ref_list)):
                degree_temp = np.array(nx.degree_histogram(graph_ref_list[i]))
                sample_ref.append(degree_temp)
            for i in range(len(graph_pred_list)):
                degree_temp = np.array(nx.degree_histogram(graph_pred_list[i]))
                sample_pred.append(degree_temp)
        print(len(sample_ref),len(sample_pred))
        mmd_dist = util.compute_mmd(sample_ref, sample_pred, kernel=util.gaussian_emd)
        return mmd_dist

    @staticmethod
    def clustering_stats(graph_ref_list, graph_pred_list, bins=100, is_parallel=True, max_workers=None):
        sample_ref = []
        sample_pred = []
        graph_pred_list_remove_empty = [G for G in graph_pred_list if not G.number_of_nodes() == 0]

        if is_parallel:
            with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
                for clustering_hist in executor.map(GraphMetrics.clustering_worker, 
                        [(G, bins) for G in graph_ref_list]):
                    sample_ref.append(clustering_hist)
            with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
                for clustering_hist in executor.map(GraphMetrics.clustering_worker, 
                        [(G, bins) for G in graph_pred_list_remove_empty]):
                    sample_pred.append(clustering_hist)
            # check non-zero elements in hist
        else:
            for i in range(len(graph_ref_list)):
                clustering_coeffs_list = list(nx.clustering(graph_ref_list[i]).values())
                hist, _ = np.histogram(
                        clustering_coeffs_list, bins=bins, range=(0.0, 1.0), density=False)
                sample_ref.append(hist)

            for i in range(len(graph_pred_list_remove_empty)):
                clustering_coeffs_list = list(nx.clustering(graph_pred_list_remove_empty[i]).values())
                hist, _ = np.histogram(
                        clustering_coeffs_list, bins=bins, range=(0.0, 1.0), density=False)
                sample_pred.append(hist)

        print(len(sample_ref),len(sample_pred))
        mmd_dist = util.compute_mmd(sample_ref, sample_pred, kernel=util.gaussian_emd, sigma=1.0/10, distance_scaling=bins)
        return mmd_dist
    
    @staticmethod
    def orbit_stats_all_mp(graph_ref_list, graph_pred_list, processes=20, chunksize=60):
        total_counts_ref = []
        total_counts_pred = []

        def get_orbits(G):
            try:
                orbit_counts = orca(G)
            except:
                return None
            return np.sum(orbit_counts, axis=0) / G.number_of_nodes()

        graph_pred_list_remove_empty = [G for G in graph_pred_list if not G.number_of_nodes() == 0]
        with Pool(processes) as p:
            total_counts_ref = [x for x in p.imap_unordered(get_orbits, graph_ref_list, chunksize=chunksize) if x is not None]
            total_counts_pred = [x for x in p.imap_unordered(get_orbits, graph_pred_list_remove_empty, chunksize=chunksize) if x is not None]

        total_counts_ref = np.array(total_counts_ref)
        total_counts_pred = np.array(total_counts_pred)
        mmd_dist = util.compute_mmd(total_counts_ref, total_counts_pred, kernel=util.gaussian, is_hist=False, sigma=30.0)
        return mmd_dist
    
    @staticmethod
    def f1_accuracy_classifier():
        print('Running classifier-based eval...')
        zipped_gen_Gs = list(zip(*(gen_Gs[x] for x in range(self.opt['tensor_slices']))))
        differ = MultiviewDiffer(self.raw_graphs, zipped_gen_Gs, use_ensemble_model=True, embedding_model=Graph2Vec(), split_first=False, even_out=True)
        res = differ.eval()
        f1_score = res['f1']
        acc_score = res['accuracy']
        base_sample_count = len(self.raw_graphs)
        gen_sample_count = len(zipped_gen_Gs)
        return f1_score
    
    @staticmethod
    def graph_classifier(data_real, data_fake):

        class GCNClassifier(nn.Module):
            def __init__(self, input_dim, hidden_dim, output_dim):
                super(GCNClassifier, self).__init__()
                self.conv1 = GCNConv(input_dim, hidden_dim)
                self.conv2 = GCNConv(hidden_dim, hidden_dim)
                self.fc = nn.Linear(hidden_dim, output_dim)
                self.sm = nn.Softmax()

            def forward(self, data):
                x, edge_index, batch = data.x, data.edge_index, data.batch
                x = self.conv1(x, edge_index)
                x = nn.ReLU()(x)
                x = self.conv2(x, edge_index)
                x = nn.ReLU()(x)
                x = global_mean_pool(x, batch)
                x = self.fc(x)
                x = self.sm(x)
                return x
        
        # Training
        def train(model, dataloader, criterion, optimizer, device, epochs=2):
            model.train()
            for epoch in range(epochs):
                for data, targets in dataloader:
                    optimizer.zero_grad()
                    data = data.to(device)  
                    targets = targets.to(device)
                    outputs = model(data)
                    loss = criterion(outputs, targets)
                    loss.backward(retain_graph = True)
                    optimizer.step()


        # Evaluation
        def evaluate(model, dataloader):
            model.eval()
            all_preds = []
            all_targets = []
            with torch.no_grad():
                for data, targets in dataloader:
                    data = data.to(device)  
                    targets = targets.to(device)
                    outputs = model(data)
                    _, preds = torch.max(outputs, 1)
                    all_preds.append(preds)
                    all_targets.append(targets)
            all_preds = torch.cat(all_preds)
            all_targets = torch.cat(all_targets)
            accuracy = accuracy_score(all_targets.cpu(), all_preds.cpu())
            f1 = f1_score(all_targets.cpu(), all_preds.cpu())
            return accuracy, f1
        
        real_labels = torch.ones(len(data_real), dtype=torch.long)
        generated_labels = torch.zeros(len(data_fake), dtype=torch.long)

        all_graphs = data_real + data_fake
        all_labels = torch.cat([real_labels, generated_labels])

        dataset = [(graph, label) for graph, label in zip(all_graphs, all_labels)]
        train_test_data_split = 0.5
        train_graphs, test_graphs = train_test_split(dataset, test_size=1-train_test_data_split, random_state=42)
        train_loader = DataLoader(train_graphs, batch_size=50, shuffle=True)
        test_loader = DataLoader(test_graphs, batch_size=50, shuffle=True)

        input_dim = data_real[0].x.size(1)
        hidden_dim = 64
        output_dim = 2

        model = GCNClassifier(input_dim, hidden_dim, output_dim).to(device)
        criterion = nn.CrossEntropyLoss().to(device)
        optimizer = optim.Adam(model.parameters(), lr=0.001)

        # Training
        train(model, train_loader, criterion, optimizer, device,  epochs=20)

        # Evaluation
        accuracy, f1 = evaluate(model, test_loader)

        print(f"Accuracy: {accuracy}")
        print(f"F1-Score: {f1}")

        return f1, accuracy

    #########################helper###########################################################################
    @staticmethod
    def degree_worker(G):
            return np.array(nx.degree_histogram(G))
    
    @staticmethod
    def clustering_worker(param):
        G, bins = param
        clustering_coeffs_list = list(nx.clustering(G).values())
        hist, _ = np.histogram(
                clustering_coeffs_list, bins=bins, range=(0.0, 1.0), density=False)
        return hist


if __name__ == '__main__':
    print("hello")
    ten = np.random.rand(3,3)
    ten2 = np.zeros((3,3))
    print(ten)
    graph = nx.from_numpy_array(ten)
    graph2 = nx.from_numpy_array(ten)
    graph_ref_list =  [graph]
    graph_pred_list = [graph]
    print(graph_ref_list[0])
    print(graph_pred_list[0])
    deg = GraphMetrics.degree_stats(graph_ref_list=graph_ref_list, graph_pred_list=graph_pred_list)
    print(deg)