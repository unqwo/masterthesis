from abc import ABC, abstractmethod
import torch
import numpy as np
import pickle as pkl
from collections import Counter
from multiprocessing import Pool
import networkx as nx
from os import path
import glob
from tqdm import tqdm, trange
import random
import networkit as nk
import pandas as pd
import sys
import torch_geometric.datasets as datasets # type: ignore
import torch_geometric.data as data # type: ignore
# import torch_geometric.transforms as transforms # type: ignore
# import torch_geometric.loader as loaders # type: ignore
from torch_geometric.utils import dense_to_sparse, to_undirected, subgraph, to_networkx
#from torch_geometric.utils.convert import to_networkx, to_scipy_sparse_matrix # type: ignore
from torch.nn.functional import one_hot
from itertools import islice

class CustomDataset(data.Dataset):
    def __init__(self, data_list):
        super().__init__()
        self.data_list = data_list

    def len(self):
        return len(self.data_list)

    def get(self, idx):
        return self.data_list[idx]
    

class Ds_deliverer(ABC):
    @staticmethod
    def get_ds(name='football') -> data.Dataset:
        ds_dict = {'football':      Football_ds_deliverer,
                    'qm9':          Qm9_ds_deliverer,
                    'zinc':         Zinc_ds_deliverer,
                    'protein':      Protein_ds_deliverer,
                    'waxman':       Waxman_ds_deliverer,
                    'cora':         Cora_ds_deliverer,
                    }
        return ds_dict[name]().get_special_ds()

    @abstractmethod
    def get_special_ds(self) -> data.Dataset:
        pass



class Football_ds_deliverer(Ds_deliverer):
    def get_special_ds(self) -> data.Dataset:
        graph_dataset = self.create_tensors()
        
        return graph_dataset
    
    #####################specific methods###############################
    POOL_SIZE = 28
    def graphs2tens(self,graphs):
        tens = []
        for gs in tqdm(graphs):
            tens.append(np.stack([nx.to_numpy_array(g) for g in gs], axis=0))
        return tens

    def load_mtx(self, loc, weighted=False):
        mtx_files = glob.glob(path.join(loc, '*.mtx'))
        id_file = glob.glob(path.join(loc, '*.ids'))[0]
        # filter out 500 files
        mtx_files = [f for f in mtx_files if '500.mtx' not in f]

        # create mapping
        tweet2id = {}
        cnt = 0
        with open(id_file, 'r') as f:
            for line in f:
                tweet_id = line.strip()
                tweet2id[tweet_id] = cnt
                cnt += 1

        ten = None
        for k, slice_file in enumerate(mtx_files):
            # print('Opening ', slice_file)
            with open(slice_file, 'r') as f:
                first_line = True
                for line in f:
                    if first_line:
                        m, n, e = [int(x) for x in line.split(' ')]
                        if m != n:
                            print(f'Warning: m != n for {slice_file}')
                        if ten is None:
                            ten = np.zeros((m, n, len(mtx_files)))
                        first_line = False
                    else:
                        i_id, j_id, w = line.split(' ')
                        w = int(w)
                        try:
                            i, j = tweet2id[i_id], tweet2id[j_id]
                        except KeyError as e:
                            print(f'Couldn\'t find ID for: ', e)
                            continue
                        if weighted:
                            ten[i, j, k] = w
                        else:
                            ten[i, j, k] = 1

            # print(nx.read_edgelist(slice_file))
            # break
        T = [nx.from_numpy_array(ten[:, :, i], create_using=nx.DiGraph) for i in range(ten.shape[2])]
        return (ten, T)
        # print('Creating tensor with ')
        # print(mtx_files)

    def load_from_csv(self, loc, weighted=False, graph_class=nx.DiGraph):
        info = []
        with open(path.join(loc, 'edges.csv'), 'r') as f:
            max_node = 0
            max_layer = 0

            for line in f:
                if line.startswith('#'):
                    continue
                data = [int(x) for x in line.strip().split(',')]
                info.append(data)
                source, target, weight, layer = data
                max_node = max(source, target, max_node)
                max_layer = max(layer, max_layer)

        ten = np.zeros((max_node+1, max_node+1, max_layer))
        for source, target, weight, layer in info:
            ten[source, target, layer-1] = weight

        T = [nx.from_numpy_array(ten[:, :, i], create_using=graph_class) for i in range(ten.shape[2])]
        return (ten, T)

    shared_ten_data = None

    def torch_to_nx_graph(self, args):
        graph_axis, i, graph_class = args
        return nx.from_numpy_array(shared_ten_data.select(graph_axis, i).to_dense().numpy(), create_using=graph_class)

    def load_from_tns(self, loc, max_size=None, weighted=False, graph_class=nx.DiGraph, graph_axis=1):
        global shared_ten_data
        # with open(loc, 'r') as f:
        #     for line in f:
        #         pieces = line.strip().split(' ')
        #         idxs = [int(x) for x in pieces[:3]]
        #         if weighted:
        #             weight = float(pieces[-1])
        #         else:
        #             weight = 1
        print(f'Reading {loc} file...')
        idxs = []
        values = []
        with open(loc, 'r') as f:
            for line in tqdm(f):
                pieces = line.strip().split(' ')
                int_idxs = [int(x) for x in pieces[:3]]

                if max_size is not None:
                    size_check = [x >= max_size for x in int_idxs]
                    if all(size_check):
                        break
                    elif any(size_check):
                        continue
                idxs.append(torch.tensor(int_idxs))

                if weighted:
                    values.append([float(pieces[-1])])
                else:
                    values.append(1)

        idx_ten = torch.vstack(idxs).T
        print('idx: ', idx_ten.shape)
        value_ten = torch.tensor(values)
        print('values: ', value_ten.shape)
        ten = torch.sparse_coo_tensor(idx_ten, value_ten, size=(max_size, max_size, max_size))
        print('ten: ', ten.shape)
        print('Converting to nx.Graph:')
        shared_ten_data = ten

        with Pool(self.POOL_SIZE) as p:
            T = []
            parts = ((graph_axis, x, graph_class) for x in range(ten.shape[graph_axis]))
            for g in tqdm(p.imap_unordered(self.torch_to_nx_graph, parts, chunksize=25), total=ten.shape[graph_axis]):
                T.append(g)
        return (ten, T)


    def load_from_np(self, loc, graph_class=nx.DiGraph, graph_axis=1, has_extra_dim=False):
        print(f'Reading {loc} file...')
        ten = np.load(loc)
        ten = np.reshape(ten, ten.shape[:3])

        print('Converting to nx.Graph:')
        print(ten.shape)
        T = [nx.from_numpy_array(ten.take(i, axis=graph_axis), create_using=graph_class) for i in trange(ten.shape[graph_axis])]
        return (ten, T)
    def csr2nk(self, A):
        N = A.shape[0]
        Gnew = nk.graph.Graph(n=N, directed=True)
        (r, c) = A.nonzero()
        for i in range(len(r)):
            Gnew.addEdge(r[i], c[i])
        return Gnew


    def create_tensor(self, dset_name, path_prefix='data/', use_gtools=False):
        if dset_name == 'football':
            data_dir = path.join(path_prefix, 'football')
            ten, T = self.load_mtx(data_dir)
        elif dset_name == 'nell2':
            file_path = path.join(path_prefix, 'nell2', 'nell2.tns')
            ten, T = self.load_from_tns(file_path, max_size=5000, graph_axis=1)
        elif dset_name == 'nell2_sampled':
            file_path = path.join(path_prefix, 'nell2', 'nell_compressed_1000x4x1000.np.npy')
            ten, T = self.load_from_np(file_path, graph_axis=1)
            ten = np.swapaxes(ten, 1, 2) # swap so last dim is # of views
        elif dset_name == 'enron_mid':
            file_path = path.join(path_prefix, 'enron', 'enron_compressed_1000x1000x10.np.npy')
            ten, T = self.load_from_np(file_path, graph_axis=2)
        elif dset_name == 'comm':
            data = []
            filenameFormat = 'fill this in with the right local path'
            for d in range(2, 9):
                for p in ['22', '23', '443', '445', '80']:
                    print('loading day %i, port %s'%(d-1, p))
                    with open(filenameFormat%(d, p), 'rb') as f:
                        data.append(pkl.load(f))
            # get vertex labels
            vertices = [d.get_vertex_dataframe() for d in data]

            # give each vertex a consistent index across views
            nodeMap = []
            for i in range(len(vertices)):
                print('creating name map '+str(i))
                tempMap = {}
                for j in range(len(vertices[i])):
                    tempMap[j] = vertices[i]['name'][j]
                nodeMap.append(tempMap)

            # create a list of all names across views
            F = pd.concat(vertices)
            allNames = list(F['name'].unique())

            # make graphs and label the nodes
            T = [d.to_networkx() for d in data]
            for i in range(len(T)):
                print('relabeling nodes in graph '+str(i))
                nx.relabel_nodes(T[i], nodeMap[i], copy=False)

            # make the tensor a list of adjacency matrices
            ten = [nx.adjacency_matrix(t, nodelist=allNames) for t in T]
        else:
            raise Exception(f'Unknown dataset: {dset_name}')


        if use_gtools:
            print('converting to networkit')
            #T = [nx2gt(x) for x in T]
            T = [self.csr2nk(x) for x in ten]
        return ten, T

    def sample_subten(self, tensor, graphs, min_nodes=50, max_nodes=200):
        chosen_nodes = set()
        o = tensor.shape[2]
        print('Sampling from subtensor...')
        for i in tqdm(range(o)):
            for j in graphs[i].nodes():
                sub = nx.ego_graph(graphs[i], j, radius=2)
                sz = sub.order()
                if sz < min_nodes or sz > max_nodes:
                    continue
                # Dedupe any node sets
                chosen_nodes.add(tuple(sorted(sub.nodes())))

        subgraphs = []
        subtens = []
        print('Generating subgraphs...')
        for chosen in tqdm(chosen_nodes):
            sgs = [nx.DiGraph(graphs[i].subgraph(chosen)) for i in range(o)]
            subgraphs.append(sgs)
            subtens.append(np.stack([nx.to_numpy_array(sg) for sg in sgs], axis=0))
        return (subtens, subgraphs)
        # print(f'{len(chosen_nodes)} chosen node sets')
        # print('Subgraphs: ', subgraphs)
        # print('Subten: ', subtens[0].shape)

    def generate_perms(self, sgs, n=50):
        out = []
        nodes = list(sgs[0].nodes())
        for _ in range(n):
            tmp = []
            random.shuffle(nodes)
            mapping = {x: nodes[x] for x in range(len(nodes))}
            for sg in sgs:
                tmp.append(nx.relabel_nodes(sg, mapping))
            out.append(tmp)
        return out

    def permute_subgraphs(self, subgraphs):

        out = []
        with Pool(self.POOL_SIZE) as p:
            for l in tqdm(p.imap_unordered(self.generate_perms, subgraphs, chunksize=5), total=len(subgraphs)):
                for k in l:
                    out.append(k)
                
            # for sgs in tqdm(subgraphs):
            #     nodes = list(sgs[0].nodes())
            #     for _ in range(n):
            #         tmp = []
            #         random.shuffle(nodes)
            #         mapping = {x: nodes[x] for x in range(len(nodes))}
            #         for sg in sgs:
            #             tmp.append(nx.relabel_nodes(sg, mapping))
            #         out.append(tmp)
        return out

    PATCHED_PREFIX = 'patched_'
    GTOOLS_SUFFIX = '_gtools'
    def create_tensors(self, dataset='football_small', path_prefix='/home/ws/unqwo/master-thesis/data', get_single_graph=False, get_raw=False, get_raw_graphs=False, sampling_method='random_walk', **kwargs):
        dataset_name, dataset_size = dataset.rsplit('_', 1)
        use_gtools = False

        max_steps = 10000
        min_nodes, max_nodes = 20, 50

        (ten, T) = self.create_tensor(dataset_name, use_gtools=use_gtools, path_prefix=path_prefix)
        if get_single_graph:
            return ten, T

        # Allow for legacy name of "random"
        if sampling_method == 'egonets' or sampling_method == 'random':
            subtens, subgraphs = self.sample_subten(ten, T, min_nodes=min_nodes, max_nodes=max_nodes)
        elif sampling_method == 'random_walk':
            subtens, subgraphs = self.sample_subten_rw(ten, T, min_nodes=min_nodes, max_nodes=max_nodes, max_steps=max_steps, use_gtools=use_gtools, **kwargs)
        elif sampling_method == 'multigraph_rw':
            subtens, subgraphs = self.sample_multigraph_rw(ten, T, min_nodes=min_nodes, max_nodes=max_nodes, max_steps=max_steps, **kwargs)
        elif sampling_method == 'multigraph_rw_no_induce':
            subtens, subgraphs = self.sample_multigraph_rw(ten, T, min_nodes=min_nodes, max_nodes=max_nodes, use_induced_sg=False, max_steps=max_steps, **kwargs)
        elif sampling_method == 'multilayer_rw':
            subtens, subgraphs = self.sample_multilayer_rw(ten, T, min_nodes=min_nodes, max_nodes=max_nodes, max_steps=max_steps, **kwargs)
        elif sampling_method == 'multilayer_rw_no_induce':
            subtens, subgraphs = self.sample_multilayer_rw(ten, T, min_nodes=min_nodes, max_nodes=max_nodes, use_induced_sg=False, max_steps=max_steps, **kwargs)

        print('subgraphs: ', len(subgraphs))
        if subgraphs:
            print('subgraph type: ', type(subgraphs[0][0]))

        print('Generating subgraph permutations...')
        perm_subgraphs = self.permute_subgraphs(subgraphs)
        print('after perm_subgraphs: ', len(perm_subgraphs))

        print('Converting graphs to tensors...')
        perm_tens = self.graphs2tens(perm_subgraphs)
        print('after graphs2tens: ', len(perm_tens))

        perm_tens_dataset = self.data2geometric(perm_tens)

        return perm_tens_dataset
    
    def data2geometric(self, perm_tens):
        gs = []
        for g in perm_tens:
            hetData = data.HeteroData()
            for j in range(g.shape[0]):
                idx = f'edge_type_{j}'
                edge_list, _ = dense_to_sparse(torch.from_numpy(g[j]))
                hetData['x', idx, 'x'].edge_index = edge_list
            
            gs.append(hetData)
        perm_tens_dataset = CustomDataset(gs)

        return perm_tens_dataset

    def sample_subten_rw(self, tensor, graphs, use_gtools=False, **kwargs):
        print('Use gtools: ', use_gtools)
        if use_gtools:
            return self.gtools_sample_subten_rw(tensor, graphs, **kwargs)
        return self.nx_sample_subten_rw(tensor, graphs, **kwargs)

    def gtools_sample_subten_rw(self, tensor, graphs, min_nodes=50, max_nodes=200, max_steps=10000):
        chosen_nodes = set()
        o = len(graphs)
        sizes = []
        #global shared_graph_data
        #global shared_sizes
        #shared_graph_data = graphs

        print('Sampling from gtools subtensor...')
        for i in tqdm(range(o)):
            print('graph '+str(i))
            #verts = graphs[i].get_vertices()
            verts = [v for v in graphs[i].iterNodes() if (graphs[i].degreeOut(v) > 0)]
            print('number of nodes: '+str(len(verts)))
            np.random.shuffle(verts)
            #verts = list(graphs[i].forNodesInRandomOrder())
            sizes.append(len(verts))

            layerCtr = 0
            for j in verts:
                subset = {j}
                v = j
                ctr = 0
                while (len(subset) < max_nodes) and (ctr < max_steps):
                    #N = graphs[i].get_out_neighbors(v)
                    N = list(graphs[i].iterNeighbors(v))
                    if len(N) == 0:
                        break
                    v = np.random.choice(N)
                    subset.add(v)
                    ctr += 1
                # sub = nx.subgraph(graphs[i], subset)
                sz = len(subset)
                if sz < min_nodes or sz > max_nodes:
                    continue
                else:
                    layerCtr += 1
                # Dedupe any node sets
                chosen_nodes.add(tuple(sorted(subset)))
                if layerCtr >= 10000:
                    break


        #shared_sizes = sizes
        subgraphs = []
        print('Generating subgraphs...')
        print('size of graph: '+str(sys.getsizeof(graphs)))
        #with Pool(28) as p:
        #    for val in tqdm(p.imap_unordered(chosen_filter_helper, chosen_nodes, chunksize=25)):
        #        subgraphs.append(val)
        chosen_nodes = list(chosen_nodes)
        for j in tqdm(range(len(chosen_nodes))):
            newList = []
            for i in range(o):
                sg = nk.graphtools.subgraphFromNodes(graphs[i], chosen_nodes[j], compact=True)
                newList.append(sg)
            #subgraphs.append([nk.graphtools.subgraphFromNodes(graphs[i], chosen) for i in range(len(graphs))])
            subgraphs.append(newList)
        
        return None, subgraphs

    def nx_sample_subten_rw(self, tensor, graphs, min_nodes=50, max_nodes=200, max_steps=10000):
        filtered_sizes = []
        chosen_nodes = set()
        o = tensor.shape[2]
        print('Sampling from subtensor...')
        for i in tqdm(range(o)):
            for j in graphs[i].nodes():
                #sub = nx.ego_graph(graphs[i], j, radius=2)  #replacing this with random walk code
                subset = {j}
                v = j
                ctr = 0
                while (len(subset) < max_nodes) and (ctr < max_steps):
                    N = list(nx.neighbors(graphs[i], v))
                    if len(N)==0:
                        break
                    v = np.random.choice(N)
                    subset.add(v)
                    ctr += 1
                sub = nx.subgraph(graphs[i], subset)

                sz = sub.order()
                if sz < min_nodes or sz > max_nodes:
                    # print('Size does not meet cutoff: ', sz)
                    filtered_sizes.append(sz)
                    continue
                # Dedupe any node sets
                chosen_nodes.add(tuple(sorted(sub.nodes())))

        print('Filtered subgraphs with of size with frequencies: ', Counter(filtered_sizes))
        subgraphs = []
        subtens = []
        print('Generating subgraphs...')
        for chosen in tqdm(chosen_nodes):     #islice(chosen_nodes, 2)):   #change, delete islice
            sgs = [nx.DiGraph(graphs[i].subgraph(chosen)) for i in range(o)]
            subgraphs.append(sgs)
            subtens.append(np.stack([nx.to_numpy_array(sg) for sg in sgs], axis=0))
        return (subtens, subgraphs)
        # print(f'{len(chosen_nodes)} chosen node sets')
        # print('Subgraphs: ', subgraphs)
        # print('Subten: ', subtens[0].shape)

    def sample_multigraph_rw(self, tensor, graphs, min_nodes=50, max_nodes=200, max_steps=10000, use_induced_sg=True):
        chosen_nodes = list()
        chosen_edges = list()
        o = tensor.shape[2]
        filtered_sizes = []

        print('Creating multigraph')
        G = nx.MultiDiGraph()
        for i in tqdm(range(o)):
            G1 = graphs[i].copy()
            G.add_edges_from([(e[0], e[1], i) for e in G1.edges()])
        E = dict()
        for e in G.edges:
            if e[0] not in E:
                E[e[0]] = []
            E[e[0]].append(e)

        print('Sampling from subtensor...')
        for j in G.nodes():
            subset = {j}
            edges = set()
            v = j
            ctr = 0
            while (len(subset) < max_nodes) and (ctr < max_steps):
                if v not in E:
                    break
                E1 = E[v]
                e_ind = np.random.choice(len(E1))
                e = E1[e_ind]
                v = e[1]
                subset.add(v)
                edges.add(e)
                ctr += 1
            sz = len(subset)
            if sz < min_nodes or sz > max_nodes:
                filtered_sizes.append(sz)
                continue
            chosen_nodes.append(tuple(sorted(subset)))
            chosen_edges.append(list(edges))

        print('Filtered subgraphs with of size with frequencies: ', Counter(filtered_sizes))
        subgraphs = []
        subtens = []
        print('Generating subgraphs...')
        for j in tqdm(range(len(chosen_nodes))):
            chosen = chosen_nodes[j]
            if use_induced_sg:
                sgs = [nx.DiGraph(graphs[i].subgraph(chosen)) for i in range(o)]
            else:
                sgs = []
                for i in range(o):
                    temp = nx.DiGraph()
                    temp.add_nodexamplees_from(chosen)
                    sgs.append(temp)
                for e in chosen_edges[j]:
                    tens_ind = e[2]
                    u = e[0]
                    v = e[1]
                    sgs[tens_ind].add_edge(u, v)
            subgraphs.append(sgs)
            subtens.append(np.stack([nx.to_numpy_array(sg) for sg in sgs], axis=0))
        return (subtens, subgraphs)

    def sample_multilayer_rw(self, tensor, graphs, min_nodes=50, max_nodes=200, max_steps=10000, \
                            use_induced_sg=True, p_switch=0.1):
        chosen_nodes = list()
        chosen_edges = list()
        o = tensor.shape[2]

        print('Sampling from subtensor...')
        for i in tqdm(range(o)):
            for j in graphs[i].nodes():
                subset = {j}
                edges = set()
                v = j
                layer = i
                ctr = 0
                while (len(subset) < max_nodes) and (ctr < max_steps):
                    if np.random.rand() < p_switch:
                        L = set(range(o))
                        L.remove(layer)
                        layer = np.random.choice(list(L))
                        continue
                    N = list(nx.neighbors(graphs[layer], v))
                    if len(N) > 0:
                        v_old = v
                        v = np.random.choice(N)
                        subset.add(v)
                        edges.add((v_old, v, layer))
                    ctr += 1
                sz = len(subset)
                if sz < min_nodes or sz > max_nodes:
                    print('Size does not meet cutoff:', sz)
                    continue
                chosen_nodes.append(tuple(sorted(subset)))
                chosen_edges.append(list(edges))

        subgraphs = []
        subtens = []
        print('Generating subgraphs...')
        for j in tqdm(range(len(chosen_nodes))):
            chosen = chosen_nodes[j]
            if use_induced_sg:
                sgs = [nx.DiGraph(graphs[i].subgraph(chosen)) for i in range(o)]
            else:
                sgs = []
                for i in range(o):
                    temp = nx.DiGraph()
                    temp.add_nodes_from(chosen)
                    sgs.append(temp)
                for e in chosen_edges[j]:
                    tens_ind = e[2]
                    u = e[0]
                    v = e[1]
                    sgs[tens_ind].add_edge(u, v)
            subgraphs.append(sgs)
            subtens.append(np.stack([nx.to_numpy_array(sg) for sg in sgs], axis=0))
        return (subtens, subgraphs)

class Qm9_ds_deliverer(Ds_deliverer):
    def get_special_ds(self) -> data.Dataset:
        graph_dataset = datasets.QM9(root='/home/ws/unqwo/master-thesis/data')
        max_node_type = 9
        max_edge_type = 4
        
        return graph_dataset

class Zinc_ds_deliverer(Ds_deliverer):
    def get_special_ds(self) -> data.Dataset:
        graph_dataset = datasets.ZINC(root='/home/ws/unqwo/master-thesis/data')
        graph_dataset_short = graph_dataset[:100]
        max_node_type = 27
        max_edge_type = 3
        data_list = [data for data in graph_dataset_short]
        for i, g in tqdm(enumerate(data_list)):
            g.x = one_hot(g.x.squeeze(), num_classes=max_node_type + 1).float()
            g.edge_attr = one_hot(g.edge_attr, num_classes=max_edge_type + 1).float()
        graph_dataset_short = CustomDataset(data_list)
        return graph_dataset_short
    
class Waxman_ds_deliverer(Ds_deliverer):
    def get_special_ds(self) -> data.Dataset:
        num_graphs = 100
        data_list = []
        for i in tqdm(range(num_graphs)):
            n = random.randint(15, 25)
            graph = nx.waxman_graph(n=n, alpha=0.4, beta=0.4)
            edge_index = torch.tensor(list(graph.edges()), dtype=torch.long).t().contiguous()
            edge_index = to_undirected(edge_index)         # if undirected, then it converges to fully connected with simila

            node_feature_matrix = np.array([value['pos'] for pos, value in graph.nodes().items()])
            node_feature_tensor = torch.tensor(node_feature_matrix, dtype=torch.float32)
            # rel = torch.cdist(node_feature_tensor, node_feature_tensor)
            # rel = torch.where((rel < 0.1) & (rel > 0))
            # rel_list = torch.stack((rel[0], rel[1]))
            artificial_features = torch.ones_like(node_feature_tensor)
            node_feature_tensor = torch.cat((node_feature_tensor, artificial_features), dim=-1)
            data_list.append(data.Data(x=node_feature_tensor, edge_index=edge_index))

        graph_dataset_short = CustomDataset(data_list)
        return graph_dataset_short

class Cora_ds_deliverer(Ds_deliverer):
    def get_special_ds(self) -> data.Dataset:
        num_graphs = 100
        dataset = datasets.Planetoid(root='/tmp/Cora', name='Cora')
        self.data_obj = dataset[0]

        self.G = to_networkx(self.data_obj, to_undirected=True)
        subgraphs = [self.perform_random_walk(random.randint(0, self.data_obj.num_nodes - 1)) for _ in range(num_graphs)]

        graph_dataset = CustomDataset(subgraphs)
        return graph_dataset

    def perform_random_walk(self, start_node, num_steps=10):
        walk = [start_node]
        current_node = start_node
        for _ in range(num_steps):
            neighbors = list(self.G.neighbors(current_node))
            if not neighbors:
                break
            current_node = random.choice(neighbors)
            walk.append(current_node)
        
        sub_nodes = torch.tensor(list(set(walk)))
        sub_edge_index, _ = subgraph(sub_nodes, self.data_obj.edge_index, relabel_nodes=True)
        sub_x = self.data_obj.x[sub_nodes]
        
        return data.Data(x=sub_x, edge_index=sub_edge_index)

class Protein_ds_deliverer(Ds_deliverer):
    def get_special_ds(self) -> data.Dataset:
        graph_dataset = datasets.TUDataset(root='/home/ws/unqwo/master-thesis/data', name='PROTEINS')
        graph_dataset_short = graph_dataset
        return graph_dataset_short

from torch_geometric.utils import to_dense_adj
if __name__ == '__main__':
    herz = Ds_deliverer.get_ds(name = 'waxman')
    d = herz[0]
    print(d)
    [print(d[edge_type].edge_index) for edge_type in d.edge_types]
    adj = to_dense_adj(d.edge_index, edge_attr=d.edge_attr)
    print(adj[:,0])

