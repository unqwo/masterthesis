import torch
from torch.autograd import grad as torch_grad
import torch.nn.functional as F


class Losses():
    def _consistency_term(real_data, D):
        M=0.1
        d1, d_1 = D(real_data)
        d2, d_2 = D(real_data)

        # why max is needed when norm is positive?
        consistency_term = (d1 - d2).norm(2, dim=1) + 0.1 * \
            (d_1 - d_2).norm(2, dim=1) - M
        return consistency_term.mean()
    
    def _gradient_penalty(real_data, generated_data, D, device = 'cpu'):
        gp_weight=10.0
        batch_size = real_data.size()[0]

        # Calculate interpolation
        alpha = torch.rand(batch_size, 1, 1, 1)
        alpha = alpha.expand_as(real_data)
        alpha = alpha.to(device)
        interpolated = alpha * real_data.data + (1 - alpha) * generated_data.data
        interpolated.requires_grad_(True)
        interpolated = interpolated.to(device)

        # Calculate probability of interpolated examples
        prob_interpolated = D(interpolated)[0]

        # Calculate gradients of probabilities with respect to examples
        gradients = torch_grad(outputs=prob_interpolated, inputs=interpolated,
                               grad_outputs=torch.ones(prob_interpolated.size()).to(device),
                               create_graph=True, retain_graph=True)[0]

        # Gradients have shape (batch_size, num_channels, img_width, img_height),
        # so flatten to easily take norm per example in batch
        
        gradients = gradients.view(batch_size, -1)

        # Derivatives of the gradient close to 0 can cause problems because of
        # the square root, so manually calculate norm and add epsilon
        gradients_norm = torch.sqrt(torch.sum(gradients ** 2, dim=1) + 1e-12)

        # Return gradient penalty
        return gp_weight * ((gradients_norm - 1) ** 2).mean()
    
    @staticmethod
    def gradient_penalty(y, x, device):
        """Compute gradient penalty: (L2_norm(dy/dx) - 1)**2."""
        weight = torch.ones(y.size()).to(device)
        dydx = torch.autograd.grad(outputs=y,
                                   inputs=x,
                                   grad_outputs=weight,
                                   retain_graph=True,
                                   create_graph=True,
                                   only_inputs=True)[0]

        dydx = dydx.view(dydx.size(0), -1)
        dydx_l2norm = torch.sqrt(torch.sum(dydx**2, dim=1))
        return torch.mean((dydx_l2norm-1)**2)

    @staticmethod
    def MPGVAE_loss(x, edge_attr_cube, mu, logvar):
        BCE_x = F.binary_cross_entropy_with_logits(x[0], x[1], reduction='mean')
        BCE_edge = F.binary_cross_entropy_with_logits(edge_attr_cube[0], edge_attr_cube[1], reduction='mean')
        KLD = 0.5 * torch.sum(logvar.exp() + mu.pow(2) - 1 - logvar, dim=1)
        KLD = KLD.mean()
        return BCE_x + BCE_edge + KLD
    
    @staticmethod
    def SNDVAE_loss(z, G_pred, G_label , beta):
        z_mean_g, z_std_g, z_mean_s, z_std_s, z_mean_sg, z_std_sg = z
        _, preds_edge, preds_spatial, preds_node = G_pred
        labels_edge, labels_node, labels_spatial = G_label

        num_nodes = labels_node.shape[1]
        preds_edge = preds_edge.view(-1, num_nodes, num_nodes, 2)
        labels_edge = labels_edge.view(-1, num_nodes, num_nodes, 1)
        labels_edge = torch.cat([1 - labels_edge, labels_edge], dim=-1)  
        
        node_cost = F.mse_loss(preds_node, labels_node)
        spatial_cost = F.mse_loss(preds_spatial, labels_spatial)
        adj_cost = F.cross_entropy(preds_edge, labels_edge, reduction='mean')    #labels_edge.argmax(dim=-1)?

        kl_s = -0.5 * torch.mean(1 + 2 * z_std_s - z_mean_s.pow(2) - (2 * z_std_s).exp())
        kl_g = -0.5 * torch.mean(1 + 2 * z_std_g - z_mean_g.pow(2) - (2 * z_std_g).exp())
        kl_sg = -0.5 * torch.mean(1 + 2 * z_std_sg - z_mean_sg.pow(2) - (2 * z_std_sg).exp())
        
        kl = kl_s + kl_g + kl_sg
        mse_loss = adj_cost + node_cost + spatial_cost
        
        total_loss = mse_loss + beta * kl
        
        return total_loss, mse_loss, kl, spatial_cost, adj_cost, node_cost, kl_g, kl_s, kl_sg
    
    @staticmethod
    def VGAE_loss(mu, logstd, neg_edge_index_hat, pos_edge_index_hat):
        pos_loss = -torch.log(pos_edge_index_hat + 1e-15).mean()
        neg_loss = -torch.log(1 - neg_edge_index_hat + 1e-15).mean()

        kl_loss = 1/mu.size(0) * Losses.kl_loss(mu, logstd)

        return pos_loss + neg_loss + kl_loss
    
    @staticmethod
    def kl_loss(mu, logstd):

        return -0.5 * torch.mean(torch.sum(1 + 2 * logstd - mu**2 - logstd.exp()**2, dim=1))