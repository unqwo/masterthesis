from abc import ABC, abstractmethod
import torch
import torch.nn as nn
from layers import *
from torch_geometric.nn import MessagePassing, Set2Set
from torch_geometric.nn import GATConv, GCNConv
from torch_geometric.utils import softmax

class Encoder(ABC):
    def __init__(self) -> None:
        pass
    def forward(self):
        pass

class TenGAN_Encoder(nn.Module, Encoder):
    def __init__(self, config_model):
        super(TenGAN_Encoder, self).__init__()
        dropout=0.5
        self.num_nodes = config_model['slice_size']
        self.slices = config_model['tensor_slices']

        self.gcns = nn.ModuleList([
            nn.ModuleList([
                TenGAN_GraphConvolution(self.num_nodes, self.num_nodes),
                nn.LeakyReLU(inplace=True),
                TenGAN_GraphConvolution(self.num_nodes, self.num_nodes),
                nn.LeakyReLU(inplace=True)
            ]) for __ in range(self.slices)
        ])

        self.final_layer = nn.Linear(self.num_nodes ** 2, 1)
        self.sig = nn.Sigmoid()

    def forward(self, ten):
        if torch.cuda.is_available():
            I = torch.eye(self.num_nodes).cuda()
        else:
            I = torch.eye(self.num_nodes)
        outs = []

        for k in range(self.slices):
            adj = ten[:, k, :, :]
            x = self.gcns[k][1](self.gcns[k][0](I, adj))
            x = self.gcns[k][3](self.gcns[k][2](I, x))
            outs.append(x)

        stacked = torch.stack(outs, dim=1) \
            .view(ten.size(0), self.slices, self.num_nodes, self.num_nodes)
        pooled = nn.functional.max_pool3d(stacked, kernel_size=(self.slices, 1, 1))
        pooled = self.sig(pooled)
        x = pooled.view(pooled.size(0), -1)

        return self.final_layer(x), x

class MolGAN_Encoder(nn.Module, Encoder):
    """Discriminator network with PatchGAN."""
    def __init__(self, config_model):
        super(MolGAN_Encoder, self).__init__()
        conv_dim = config_model['d_conv_dim']
        m_dim = config_model['num_node_features']
        b_dim = config_model['num_edge_features']
        dropout = config_model['dropout']

        graph_conv_dim, aux_dim, linear_dim = conv_dim
        # discriminator
        self.gcn_layer = MolGAN_GraphConvolution(m_dim, graph_conv_dim, b_dim, dropout)
        self.agg_layer = MolGAN_GraphAggregation(graph_conv_dim[-1], aux_dim, m_dim, dropout)

        # multi dense layer
        layers = []
        for c0, c1 in zip([aux_dim]+linear_dim[:-1], linear_dim):
            layers.append(nn.Linear(c0,c1))
            layers.append(nn.Dropout(dropout))
        self.linear_layer = nn.Sequential(*layers)

        self.output_layer = nn.Linear(linear_dim[-1], 1)

    def forward(self, adj, hidden, node, activatation=nn.Sigmoid()):
        adj = adj[:,:,:,1:].permute(0,3,1,2)
        annotations = torch.cat((hidden, node), -1) if hidden is not None else node
        h = self.gcn_layer(annotations, adj)
        annotations = torch.cat((h, hidden, node) if hidden is not None else (h, node), -1)
        h = self.agg_layer(annotations, torch.tanh)
        h = self.linear_layer(h)

        # Need to implemente batch discriminator #
        ##########################################

        output = self.output_layer(h)
        output = activatation(output) if activatation is not None else output

        return output, h

class MPGVAE_Encoder(MessagePassing, Encoder):
    def __init__(self, config_model):
        node_features= config_model['num_node_features']
        edge_features= config_model['num_edge_features']
        hidden_dims = config_model['encoder_hidden_dims']
        latent_dim = config_model['latent_dim']
        super(MPGVAE_Encoder, self).__init__(aggr='add')
        
        self.node_embeddings = torch.nn.ModuleList([
            torch.nn.Linear(node_features if i == 0 else hidden_dims[i - 1], hidden_dims[i])
            for i in range(len(hidden_dims))
        ])
        
        self.edge_embeddings = torch.nn.ModuleList([
            torch.nn.Linear(edge_features if i == 0 else hidden_dims[i - 1], hidden_dims[i])
            for i in range(len(hidden_dims))
        ])
        
        self.grus = torch.nn.ModuleList([
            torch.nn.GRUCell(hidden_dims[i], hidden_dims[i])
            for i in range(len(hidden_dims))
        ])

        self.attn_weights = nn.ModuleList([
            nn.Linear(hidden_dims[i], 1)
            for i in range(len(hidden_dims))
        ])
        
        self.set2set = Set2Set(hidden_dims[-1], processing_steps=6)
        self.fc_mu = torch.nn.Linear(2 * hidden_dims[-1], latent_dim)
        self.fc_logvar = torch.nn.Linear(2 * hidden_dims[-1], latent_dim)
        
    def forward(self, x, edge_index, edge_attr, batch, ptr):
        for i, (node_emb, edge_emb, attn_weight, gru) in enumerate(zip(self.node_embeddings, self.edge_embeddings, self.attn_weights, self.grus)):
            x = node_emb(x)
            edge_attr = edge_emb(edge_attr)
            m = self.message(x, edge_index, edge_attr)
            edge_attr = m
            aggregated_messages = self.aggregate(x, m, edge_index, attn_weight, ptr)
            x = self.update(x, aggregated_messages, gru)
        
        graph_embedding = self.set2set(x, batch)
        mu = self.fc_mu(graph_embedding)
        logvar = self.fc_logvar(graph_embedding)
        
        return x, mu, logvar

    def message(self, x, edge_index, edge_attr):
        row, col = edge_index
        x_i = x[row]
        x_j = x[col]
        m = torch.tanh(edge_attr + x_i + x_j)
        return m #validated

    
    def aggregate(self, x, messages, edge_index, attn_weight, ptr):
        row, col = edge_index
        attn_coeff = softmax(attn_weight(messages),row, ptr=ptr)
        weighted_messages = attn_coeff * messages
        aggregated_messages = torch.zeros_like(x)
        aggregated_messages = aggregated_messages.scatter_add_(0, row.unsqueeze(-1).expand(-1, messages.size(-1)), weighted_messages)
        return aggregated_messages #validated

    def update(self, x, aggregated_messages, gru):
        return gru(aggregated_messages, x)
    
class SNDVAE_Encoder(nn.Module, Encoder):
    def __init__(self, config_model):
        super(SNDVAE_Encoder, self).__init__()
        spatial_dim = config_model['spatial_dim']
        num_node_features= config_model['num_node_features']
        vertexes = config_model['vertexes']

        self.g_conv_hidden = config_model['g_conv_hidden']
        self.g_hidden_size = config_model['g_hidden_size']
        self.g_latent_size = config_model['g_latent_size']

        self.s_channels = config_model['s_channels']
        self.s_hidden_size = config_model['s_hidden_size']
        self.s_latent_size = config_model['s_latent_size']
        self.s_kernel_size = config_model['s_kernel_size']
        self.s_strides = config_model['s_strides']

        self.sg_conv_hidden = config_model['sg_conv_hidden']
        self.sg_hidden_size = config_model['sg_hidden_size']
        self.sg_latent_size = config_model['sg_latent_size']


        self.graph_conv_layers = nn.ModuleList([
            SNDVAE_GraphConvolution(num_node_features-spatial_dim if i == 0 else num_node_features-spatial_dim + self.g_conv_hidden[i-1], self.g_conv_hidden[i])
            for i in range(len(self.g_conv_hidden))
            ])
        self.spatial_conv_layers = nn.ModuleList([
            Conv1D(in_channels, out_channels, filter_size=s_kernel, stride=s_stride, padding=2)
            for in_channels, out_channels, s_kernel, s_stride in zip([spatial_dim] + self.s_channels[:-1], self.s_channels, self.s_kernel_size, self.s_strides)
            ])

        self.spatial_graph_conv_layers = nn.ModuleList([
            SpatialGraphConvolutionLayer(in_features, hidden_sizes=hiddens, rel_dim=1)
            for in_features, hiddens in zip([num_node_features-spatial_dim] + np.array(self.sg_conv_hidden)[:-1,-1].tolist(), self.sg_conv_hidden)
            ])

        # Batch-Norm
        self.g_bn_g = nn.ModuleList([BatchNorm(dim) for dim in self.g_conv_hidden])
        self.g_bn_s = nn.ModuleList([BatchNorm(dim) for dim in self.s_channels])
        self.g_bn_sg = nn.ModuleList([BatchNorm(dim) for dim in np.array(self.sg_conv_hidden)[:,-1].tolist()])

        self.encoder_s = BatchNorm(self.s_channels[-1])
        self.encoder_g = BatchNorm(self.g_conv_hidden[-1] + num_node_features - spatial_dim)
        self.encoder_sg = BatchNorm(self.sg_conv_hidden[-1][-1])

        # Linear layers
        self.g_lin = Linear((self.g_conv_hidden[-1] + num_node_features - spatial_dim) *vertexes, self.g_hidden_size)
        self.g2mu = Linear(self.g_hidden_size, self.g_latent_size)
        self.g2std = Linear(self.g_hidden_size, self.g_latent_size)

        self.s_lin = Linear(self.s_channels[-1]*vertexes, self.s_hidden_size)
        self.s2mu = Linear(self.s_hidden_size, self.s_latent_size)
        self.s2std = Linear(self.s_hidden_size, self.s_latent_size)

        self.sg_lin = Linear(self.sg_conv_hidden[-1][-1]*vertexes, self.sg_hidden_size)
        self.sg2mu = Linear(self.sg_hidden_size, self.sg_latent_size)
        self.sg2std = Linear(self.sg_hidden_size, self.sg_latent_size)

        #Relus
        self.s_relu = nn.ReLU()
        self.sg_leaky_relu = nn.LeakyReLU()

    def forward(self, node_feature, node_feature_sg, inputs_3d, adj, adj_sg, rel_sg):

        # Graph embeddings
        g = node_feature    # B*N*(F-spatial_dim)
        for layer, bn in zip(self.graph_conv_layers, self.g_bn_g):
            g = bn(layer(g, adj).transpose(1,2)).transpose(1,2)
            g = torch.cat([g, node_feature], dim=-1)
        g = self.encoder_g(g.transpose(1,2)).transpose(1,2)
        g = g.reshape(g.size(0), -1)    # B x (N*(H-spatial_dim))
        g = self.g_lin(g)               # B x g_latent_size

        # Spatial embeddings
        h = inputs_3d   # B*N*S
        for layer, bn in zip(self.spatial_conv_layers, self.g_bn_s):
            h = bn(layer(h.transpose(1, 2))).transpose(1, 2)
            h = self.s_relu(h)
        s = self.encoder_s(h.transpose(1, 2)).transpose(1, 2)
        s = s.reshape(s.size(0), -1)
        s = self.s_lin(s)

        # Spatial-network joint embeddings
        sg = node_feature_sg  
        for layer, bn in zip(self.spatial_graph_conv_layers, self.g_bn_sg):
            sg = bn(layer(sg, adj_sg, rel_sg).transpose(1, 2)).transpose(1, 2)
            sg = self.sg_leaky_relu(sg)
        sg = self.encoder_sg(sg.transpose(1, 2)).transpose(1, 2)
        sg = sg.reshape(sg.size(0), -1)
        sg = self.sg_lin(sg)

        z_mean_g = self.g2mu(g)
        z_std_g = self.g2std(g)  
        z_mean_s = self.s2mu(s)
        z_std_s = self.s2std(s)
        z_mean_sg = self.sg2mu(sg)
        z_std_sg = self.sg2std(sg)

        return z_mean_g, z_std_g, z_mean_s, z_std_s, z_mean_sg, z_std_sg
    
class VGAE_Encoder(nn.Module, Encoder):
    def __init__(self, config_model):
        super(VGAE_Encoder, self).__init__()
        self.num_node_features = config_model['num_node_features']
        self.hidden_dim = config_model['hidden_dim']
        self.latent_dim = config_model['latent_dim']
  
        super(VGAE_Encoder, self).__init__()
        self.gcn_shared = GCNConv(self.num_node_features, self.hidden_dim)
        self.gcn_mu = GCNConv(self.hidden_dim, self.latent_dim)
        self.gcn_logvar = GCNConv(self.hidden_dim, self.latent_dim)

    def forward(self, x, edge_index):
        x = F.relu(self.gcn_shared(x, edge_index))
        mu = self.gcn_mu(x, edge_index)
        logvar = self.gcn_logvar(x, edge_index)

        return mu, logvar
