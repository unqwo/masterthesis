class Configuration_Model():
    def get_config(name):
        conf = {'TenGAN': Configuration_Model.config_model_TenGAN,
                'MolGAN': Configuration_Model.config_model_MolGAN,
                'MPGVAE': Configuration_Model.config_model_MPGVAE,
                'SNDVAE': Configuration_Model.config_model_SNDVAE,
                'VGAE':   Configuration_Model.config_model_VGAE,
                }
        return conf[name]

    config_model_TenGAN = {   # number of epochs of training
                            'n_permutations': 5020,  # number of permutations of the graph
                            'gen_lr': 0.001,  # adam: learning rate
                            'disc_lr': 0.0001,  # learning rate for discriminator
                            # (b1, b2): decay of first order momentum of gradient & first order momentum of gradient
                            'betas': (0.5, 0.999),
                            'n_cpu': 32,  # number of cpu threads to use during batch generation
                            'latent_dim': 100,  # dimensionality of the latent space
                            'rank': 30,  # rank used for bilinear size
                            'gen_layer_size': 128,  # size of layers in generator
                            'disc_layer_size': 1024,  # size of layers in discriminator
                            'sample_interval': 800,  # interval between graph sampling
                            'save_interval': 250,  # interval between model saving
                            'print_interval': 100,  # interval between printing loss info
                            'plot_interval': 200,  # interval between plotting
                            'loss_interval': 10,  # interval between loss sampling
                            'model_name': 'TenGAN',
                            'slice_size': 50,  # graph slice size
                            'fully_random': False,
                            'comment': '',
                            # generator class name, used by ModelZoo
                            'gen_class': 'NewCPTensorGenerator',
                            # discriminator class name, used by ModelZoo
                            'disc_class': 'NewLayeredMultiviewDiscriminator',
                            'dataset': 'football_small',
                            'tensor_slices': 6,
                            'rank_lambda': 0,
                            'penalty_type': 'fro',
                            'n_graph_sample_batches': 10,
                            'rank_penalty_method': 'A', # A, B, or C
                            'eval_every': 0,
                            'sampling_method': 'random_walk',
                            'eval_method': 'multiview',
    }

    config_model_MolGAN = { 'z_dim': 8,
                            'g_conv_dim': [128, 256, 512],
                            'd_conv_dim': [[128, 64], 128, [128, 64]],
                            'g_repeat_num': 6,
                            'd_repeat_num': 6,
                            'lambda_cls': 1.0,
                            'lambda_rec': 10.0,
                            'lambda_gp': 10.0,
                            'post_method': 'softmax',
                            'dropout': 0.0,
                            }
    
    config_model_MPGVAE = { 'encoder_hidden_dims': [32, 64, 64, 128],
                            'decoder_hidden_dims': [64, 64, 32, 32],
                            'latent_dim': 18
                            }

    config_model_SNDVAE = { "spatial_dim": 2,
                            "spatial_conv_layers": 3,
                            "s_channels": [10, 10, 20],
                            "s_kernel_size": [5, 5, 5],
                            "s_strides": [1, 1, 1],
                            "s_hidden_size": 100,
                            "s_latent_size": 100,
                            "graph_conv_layers": 2,
                            "g_conv_hidden": [10, 20],
                            "g_hidden_size": 100,
                            "g_latent_size": 100,
                            "spatial_graph_conv_layers": 2,
                            "sg_conv_hidden": [[20, 20, 20], [50, 50, 50]],
                            "sg_hidden_size": 500,
                            "sg_latent_size": 500,
                            "spatial_deconv_layers": 3,
                            "s_d_channel": [50, 20, 10],
                            "s_d_kernel_size": [5, 5, 5],
                            "s_d_strides": [1, 1, 1],
                            "graph_deconv_layers": 2,
                            "n_d_channel": [50, 20, 10],
                            "n_d_kernel_size": [5, 5, 5],
                            "n_d_strides": [1, 1, 1],
                            "d_hidden_size": 20,
                            "e_d_hidden": [50, 20, 10],
                            "node_h_size": 50,
                           
                           }
    
    config_model_VGAE = {   'hidden_dim': 32,
                            'latent_dim': 16,
                            }
    
class Configuration_Train():
    def get_config(name):
        conf = {'TenGAN': Configuration_Train.config_train_TenGAN,
                'MolGAN': Configuration_Train.config_train_MolGAN,
                'MPGVAE': Configuration_Train.config_train_MPGVAE,
                'SNDVAE': Configuration_Train.config_train_SNDVAE,
                'VGAE':   Configuration_Train.config_train_VGAE,
                }
        return conf[name]
    
    config_train_TenGAN = {
                            'batch_size': 50,  # size of the batches
                            'gen_lr': 0.001,  # adam: learning rate
                            'disc_lr': 0.0001,
                            'critic_iterations': 1, # update generator every critic_iterations
                            'generator_iterations': 5, # update discriminator every generator_iterations
                            'uses_graphtool': False,
                            'max_eval_rank': 40,  # max eval rank for tensor-based eval,
                            'tensor_eval_samples': 50,  # number of samples for tensor-based eval
                            'cache_data': True, # whether or not to cache the dataset

                            'rank_lambda': 0,
                            'penalty_type': 'fro',
                            'n_graph_sample_batches': 10,
                            'rank_penalty_method': 'A', # A, B, or C
                            'eval_every': 0,
                            'eval_method': 'multiview',
                            'save_interval': 250,  # interval between model saving
                            'epoch_print_every' : 3,
                            'print_every' : 150,
                            'optimizer': 'RMSprop',


    }

    config_train_MolGAN = { 'batch_size': 16,
                            'num_iters': 10,
                            'num_iters_decay': 100000,
                            'g_lr': 0.0001,
                            'd_lr': 0.0001,
                            'n_critic': 2,
                            'beta1': 0.5,
                            'beta2': 0.999,
                            'resume_iters': None,
                            # Test configuration.
                            'test_iters': 200000,
                            # Miscellaneous.
                            'num_workers': 1,
                            'mode': 'train',
                            'use_tensorboard': False,
                            # Directories.
                            'mol_data_dir': '/home/ws/unqwo/Downloads/MolGAN-pytorch-master/data/gdb9_9nodes.sparsedataset',
                            'log_dir': '/home/ws/unqwo/Downloads/MolGAN-pytorch-master/molgan/logs',
                            'model_save_dir': '/home/ws/unqwo/Downloads/MolGAN-pytorch-master/molgan/models',
                            'sample_dir': '/home/ws/unqwo/Downloads/MolGAN-pytorch-master/molgan/samples',
                            'result_dir': '/home/ws/unqwo/Downloads/MolGAN-pytorch-master/molgan/results',
                            # Step size.
                            'log_step': 10,
                            'sample_step': 1000,
                            'model_save_step': 10000,
                            'lr_update_step': 1000,
                            'optimizer': 'Adam',
                            }

    config_train_MPGVAE = { 'batch_size': 1,
                            'lr': 0.001,
                            'optimizer': 'Adam',
                            }
    
    config_train_SNDVAE = { 'batch_size':50,  
                            'lr': 0.001,
                            'optimizer': 'Adam',
                            #"epochs": 2000,
                            "dropout": 1,
                            "decoder_batch_size": 50,
                            "sg_batch_size": 5,
                            "sg_decoder_batch_size": 5,
                            "dataset_path": "../dataset/",
                            "num_feature": 1,
                            "verbose": 1,
                            "test_count": 10,
                            "model": "feedback",
                            "seeded": 1,
                            "connected_split": 0,
                            "type": "test_reconstruct",
                            "if_traverse": 1,
                            "visualize_length": 5,
                            "dataset": "synthetic1",
                            "C_max": 100,
                            "C_stop_iter": 1e2,
                            "gamma": 100,
                            "C_step": 20,
                            "sampling_num": 10,
                            "dim": None,
                            "group_type": None,
                            "beta": 1,
                            "log_step": 100,
                           }
    
    config_train_VGAE = {   'batch_size': 10,
                            'optimizer': 'Adam',
                            'lr': 0.01
                            }
