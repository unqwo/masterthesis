import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from layers import *
from torch_geometric.nn import MessagePassing, Set2Set, InnerProductDecoder
import torch_geometric.data as data # type: ignore
from opt_einsum import contract
import torch_geometric.utils as pyg_utils

class Decoder():
    def __init__(self) -> None:
        pass
    def forward(self):
        pass

class TenGAN_Decoder(nn.Module, Decoder):
    def __init__(self, config_model):
        super(TenGAN_Decoder, self).__init__()
        self.num_nodes = config_model['slice_size']
        self.rank = config_model['rank']
        self.layer_size = config_model['gen_layer_size']
        self.latent_dim = config_model['latent_dim']
        self.num_views = config_model['tensor_slices']
        self.extra_dim = True

        shared_layers = [
            nn.Linear(self.latent_dim, self.layer_size),
            nn.ReLU(inplace=True),
            # New block
            nn.Linear(self.layer_size, self.layer_size * 2),
            nn.BatchNorm1d(self.layer_size * 2),
            nn.ReLU(inplace=True),
            # New block
            nn.Linear(self.layer_size * 2, self.layer_size * 4),
            nn.BatchNorm1d(self.layer_size * 4),
            nn.ReLU(inplace=True)
        ]

        output_layers = [
            [
                nn.Linear(self.layer_size * 4, self.layer_size * 2),
                nn.ReLU(),
                nn.Linear(self.layer_size * 2, self.num_nodes * self.rank),
                nn.Sigmoid()
                # nn.BatchNorm1d(layer_size * 4),
            ] for _ in range(2)
        ]
        view_layer = [
            nn.Linear(self.layer_size * 4, self.layer_size * 2),
            nn.ReLU(),
            nn.Linear(self.layer_size * 2, self.num_views * self.rank),
            nn.Sigmoid()
            # nn.BatchNorm1d(layer_size * 4),
        ]

        self.shared = nn.Sequential(*shared_layers)
        self.output1 = nn.Sequential(*output_layers[0])
        self.output2 = nn.Sequential(*output_layers[1])
        self.output3 = nn.Sequential(*view_layer)
        self.output_factors = False

    
    def set_factor_output(self, new_val):
        self.output_factors = new_val
        return True

    def forward(self, noise):
        batch_sz = noise.shape[0]
        S = self.shared(noise)
        A = self.output1(S).view(batch_sz, self.num_nodes, self.rank)
        B = self.output2(S).view(batch_sz, self.num_nodes, self.rank)
        C = self.output3(S).view(batch_sz, self.num_views, self.rank)
        out = contract('faz,fbz,fcz->fabc', A, B, C, backend='torch')
        # print('out shape: ', out.shape)
        out = out.permute(0, 3, 1, 2)
        # print('out (permuted) shape: ', out.shape)
        if self.output_factors:
            return (out, (A, B, C))
        else:
            return out

class MolGAN_Decoder(nn.Module, Decoder):
    """Generator network."""
    def __init__(self, config_model):
        super(MolGAN_Decoder, self).__init__()
        self.vertexes = config_model['vertexes']
        self.edges = config_model['num_edge_features']
        self.nodes = config_model['num_node_features']
        self.z_dim = config_model['z_dim']
        self.conv_dims = config_model['g_conv_dim']
        self.dropout = config_model['dropout']


        layers = []
        for c0, c1 in zip([self.z_dim]+self.conv_dims[:-1], self.conv_dims):
            layers.append(nn.Linear(c0, c1))
            layers.append(nn.Tanh())
            layers.append(nn.Dropout(p=self.dropout, inplace=True))
        self.layers = nn.Sequential(*layers)

        self.edges_layer = nn.Linear(self.conv_dims[-1], self.edges * self.vertexes * self.vertexes)
        self.nodes_layer = nn.Linear(self.conv_dims[-1], self.vertexes * self.nodes)
        self.dropoout = nn.Dropout(p=self.dropout)

    def forward(self, x):
        output = self.layers(x)
        edges_logits = self.edges_layer(output)\
                       .view(-1,self.edges,self.vertexes,self.vertexes)
        edges_logits = (edges_logits + edges_logits.permute(0,1,3,2))/2
        edges_logits = self.dropoout(edges_logits.permute(0,2,3,1))

        nodes_logits = self.nodes_layer(output)
        nodes_logits = self.dropoout(nodes_logits.view(-1,self.vertexes,self.nodes))

        return edges_logits, nodes_logits

class MPGVAE_Decoder(MessagePassing, Decoder):
    def __init__(self, config_model):
        self.node_features = config_model['num_node_features']
        self.edge_features = config_model['num_edge_features']
        self.hidden_dims = config_model['decoder_hidden_dims']
        self.h_dim =config_model['encoder_hidden_dims'][-1]
        self.latent_dim = config_model['latent_dim']
        super(MPGVAE_Decoder, self).__init__(aggr='add')
        
        self.node_embeddings = torch.nn.ModuleList([
            torch.nn.Linear(self.h_dim if i == 0 else self.hidden_dims[i - 1], self.hidden_dims[i])
            for i in range(len(self.hidden_dims))
        ])
        
        self.edge_embeddings = torch.nn.ModuleList([
            torch.nn.Linear(self.h_dim if i == 0 else self.hidden_dims[i - 1], self.hidden_dims[i])
            for i in range(len(self.hidden_dims))
        ])
        
        self.grus = torch.nn.ModuleList([
            torch.nn.GRUCell(self.hidden_dims[i], self.hidden_dims[i])
            for i in range(len(self.hidden_dims))
        ])

        self.attn_weights = nn.ModuleList([
            nn.Linear(self.hidden_dims[i], 1)
            for i in range(len(self.hidden_dims))
        ])
        
        self.node_out = nn.Linear(self.hidden_dims[-1], self.node_features)
        self.edge_out = nn.Linear(self.hidden_dims[-1], self.edge_features)

        self.z_to_hidden = nn.Linear(self.latent_dim, self.h_dim)
        self.initial_rnn = nn.RNNCell(self.h_dim, self.h_dim)
    
    def forward(self, z, num_nodes):
        self.num_nodes = num_nodes
        b = self.read_in(z)
        x, edge_index, edge_attr, ptr, batch = b.x, b.edge_index, b.edge_attr, b.ptr, b.batch
        for i, (node_emb, edge_emb, attn_weight, gru) in enumerate(zip(self.node_embeddings, self.edge_embeddings, self.attn_weights, self.grus)):
            x = node_emb(x)
            edge_attr = edge_emb(edge_attr)
            m = self.message(x, edge_index, edge_attr)
            edge_attr = m
            aggregated_messages = self.aggregate(x, m, edge_index, attn_weight, ptr)
            x = self.update(x, aggregated_messages, gru)
        
        x, edge_attr = self.read_out(x=x, edge_index=edge_index, edge_attr=edge_attr, b=b)
        graph = x, edge_index, edge_attr, ptr, batch

        return graph
    
    def read_in(self, z):
        batch = []
        for i, num_n in enumerate(self.num_nodes):
            hz = torch.sigmoid(self.z_to_hidden(z[i]))
            #hz_repeated = hz.expand(num_n, -1)
            node_list = []
            for j in range(num_n):
                if j == 0:
                    node_list.append(self.initial_rnn(hz))
                else:
                    node_list.append(self.initial_rnn(hz, node_list[j-1]))
            
            x_0 = torch.stack(node_list, dim=0)

            zzt = torch.mm(x_0, x_0.t())
            z_norm = torch.norm(x_0)
            edge_index = torch.sigmoid(zzt / z_norm)
            diag_zero = torch.ones_like(edge_index)-torch.eye(edge_index.size(0)).to(device=edge_index.device)
            rand_zero = torch.bernoulli(torch.full((edge_index.size(0), edge_index.size(0)), 0.2)).to(device=edge_index.device)
            rand_zero = rand_zero * rand_zero.transpose(0,1)
            edge_index = edge_index * (diag_zero) * rand_zero
            edge_index = torch.round(edge_index).to_sparse().indices()

            e_0 = torch.zeros((edge_index.size(1), self.h_dim)).to(edge_index.device)
            batch.append(data.Data(x=x_0, edge_index=edge_index, edge_attr=e_0))

        batch = data.Batch.from_data_list(batch)
        return batch

    def message(self, x, edge_index, edge_attr):
        row, col = edge_index
        x_i = x[row]
        x_j = x[col]
        return torch.tanh(edge_attr + x_i + x_j)

    
    def aggregate(self, x, messages, edge_index, attn_weight, ptr):
        row, col = edge_index
        attn_coeff = pyg_utils.softmax(attn_weight(messages),row, ptr=ptr)
        weighted_messages = attn_coeff * messages
        aggregated_messages = torch.zeros_like(x)
        aggregated_messages = aggregated_messages.scatter_add_(0, row.unsqueeze(-1).expand(-1, messages.size(-1)), weighted_messages)
        return aggregated_messages #validated

    def update(self, x, aggregated_messages, gru):
        return gru(aggregated_messages, x)

    def read_out(self, x, edge_index, edge_attr, b):
        x_out = torch.softmax(self.node_out(x), dim=-1)
        #cube = to_dense_adj(edge_index, edge_attr=edge_attr, batch=b.batch)
        #c_T = cube.transpose(1, 2)
        #e_out = (cube + c_T) / 2
        undirected_edge_index, e_out = pyg_utils.to_undirected(edge_index, edge_attr, reduce="add")
        e_out /= 2
        #e_list = [dense_to_sparse(element) for element in e_out]
        e_out = torch.softmax(self.edge_out(e_out), dim=-1)
        return x_out, e_out

class SNDVAE_Decoder(nn.Module, Decoder):
    def __init__(self, config_model):
        super(SNDVAE_Decoder, self).__init__()
        self.n_samples = config_model['vertexes']      #num of nodes per graph
        self.node_h_size = config_model['node_h_size']  #hidden feature length
        self.spatial_dim = config_model['spatial_dim']
        self.num_node_features= config_model['num_node_features']

        self.g_latent_size = config_model['g_latent_size']
        self.s_latent_size = config_model['s_latent_size']
        self.sg_latent_size = config_model['sg_latent_size']

        self.n_d_channels = config_model['n_d_channel']
        self.n_d_kernel_size = config_model['n_d_kernel_size']
        self.n_d_strides = config_model['n_d_strides']
        self.e_d_hidden = config_model['e_d_hidden']
        self.s_d_channels = config_model['s_d_channel']
        self.s_d_kernel_size = config_model['s_d_kernel_size']
        self.s_d_strides = config_model['s_d_strides']
        
        # Define linear transformations
        self.d_sg_lin1 = nn.Linear(self.sg_latent_size, self.n_samples * self.node_h_size)
        self.d_s_lin1 = nn.Linear(self.s_latent_size, self.n_samples * self.node_h_size)
        self.d_g_lin1 = nn.Linear(self.g_latent_size, self.n_samples * self.node_h_size)
        self.dec2feat = nn.Linear(self.n_d_channels[-1], self.num_node_features -self.spatial_dim)
        self.d_e_lin2 = nn.Linear(self.e_d_hidden[-1], 2)
        self.d_s_lin2 = nn.Linear(self.s_d_channels[-1], self.spatial_dim)

        # Define deconvolution layers for graph, node, spatial feature generation
        self.node_deconv = nn.ModuleList([
            Conv1D(in_channels, out_channels, filter_size=n_d_kernel, stride=n_d_stride, padding='same')
            for in_channels, out_channels, n_d_kernel, n_d_stride in zip([self.node_h_size * 2] + self.n_d_channels[:-1], self.n_d_channels, self.n_d_kernel_size, self.n_d_strides)
        ])
        
        # e2e layer
        self.e2e = nn.ModuleList([
            EdgeToEdgeConv2D(input_channels=in_channels, output_dim=out_channels, kernel_size=self.n_samples, padding='same')
            for in_channels, out_channels in zip([self.node_h_size * 4] + self.e_d_hidden[:-1], self.e_d_hidden)
        ])

        self.spatial_deconv = nn.ModuleList([
            Conv1D(in_channels, out_channels, filter_size=s_d_kernel, stride=s_d_stride, padding='same')
            for in_channels, out_channels, s_d_kernel, s_d_stride in zip([self.node_h_size * 2] + self.s_d_channels[:-1], self.s_d_channels, self.s_d_kernel_size, self.s_d_strides)
        ])

        # Batch normalization layers
        self.d_bn_n = nn.ModuleList([nn.BatchNorm1d(dim) for dim in self.n_d_channels])
        self.d_bn_e = nn.ModuleList([BatchNorm2D(dim) for dim in [self.node_h_size*4] + self.e_d_hidden[:-1]])
        self.d_bn_s = nn.ModuleList([nn.BatchNorm1d(dim) for dim in self.s_d_channels])

        self.decoder_node = BatchNorm(self.n_d_channels[-1])
        self.decoder_adj = BatchNorm2D(self.e_d_hidden[-1])

    def forward(self, z_s, z_sg, z_g):
        # Reshape and linear transformations
        z_sg = self.d_sg_lin1(z_sg).view(-1, self.n_samples, self.node_h_size)  #BxNxh
        z_s = self.d_s_lin1(z_s).view(-1, self.n_samples, self.node_h_size)     #BxNxh
        z_g = self.d_g_lin1(z_g).view(-1, self.n_samples, self.node_h_size)     #BxNxh
        #z_sg = torch.mean(z_sg, dim=1) #graph-info saved in 'one additional' node

        # Generate node features
        z_sg_g = torch.cat((z_sg, z_g), dim=-1)
        node_z_n = z_sg_g
        for conv_layer, bn_layer in zip(self.node_deconv, self.d_bn_n):
            node_z_n = bn_layer(conv_layer(node_z_n.transpose(1,2))).transpose(1,2)
        generated_node_feat = torch.sigmoid(self.dec2feat(self.decoder_node(node_z_n.reshape(node_z_n.size(0)*self.n_samples, -1))))
        generated_node_feat = generated_node_feat.view(-1, self.n_samples, self.num_node_features - self.spatial_dim)

        # Generate adjacency matrix
        adj_z_n1 = z_sg_g.unsqueeze(2).expand(-1, -1, self.n_samples, -1)
        adj_z_n2 = z_sg_g.unsqueeze(1).expand(-1, self.n_samples, -1, -1)
        adj_z_n = torch.cat((adj_z_n1, adj_z_n2), dim=3)
        for conv_layer, bn_layer in zip(self.e2e, self.d_bn_e):
            adj_z_n = bn_layer(adj_z_n.transpose(1,3))
            adj_z_n = conv_layer(F.relu(adj_z_n)).transpose(1,3)
        generated_adj_prob_origin = self.d_e_lin2(F.relu(self.decoder_adj(adj_z_n.transpose(1,3)).transpose(1,3)).reshape(adj_z_n.size(0)*self.n_samples*self.n_samples, -1))
        diag = (torch.ones((self.n_samples)) - torch.eye(self.n_samples)).unsqueeze(0).expand(adj_z_n.size(0), -1, -1).to(generated_adj_prob_origin.device)
        generated_adj_prob1 = diag * generated_adj_prob_origin.view(-1, self.n_samples, self.n_samples, 2)[:,:,:,1]             #B*N*N
        generated_adj_prob0 = diag * generated_adj_prob_origin.view(-1, self.n_samples, self.n_samples, 2)[:,:,:,0] + (1-diag)  #B*N*N
        generated_adj_prob = torch.cat((generated_adj_prob0.view(-1, self.n_samples, self.n_samples, 1), generated_adj_prob1.view(-1, self.n_samples, self.n_samples, 1)), dim=-1)
        generated_adj_prob = F.softmax(generated_adj_prob, dim=-1)
        generated_adj_prob = generated_adj_prob*generated_adj_prob.transpose(1,2)
        generated_adj = torch.argmax(generated_adj_prob, dim=-1) #B*N*N

        # Decode spatial information
        spatial_h = torch.cat((z_sg, z_s), dim=-1)
        for conv_layer, bn_layer in zip(self.spatial_deconv, self.d_bn_s):
            spatial_h = bn_layer(conv_layer(spatial_h.transpose(1,2))).transpose(1,2)
        generated_spatial = torch.sigmoid(self.d_s_lin2(spatial_h.reshape(spatial_h.size(0)*self.n_samples, -1)))
        generated_spatial = generated_spatial.view(-1, self.n_samples, self.spatial_dim)

        return generated_adj, generated_adj_prob, generated_spatial, generated_node_feat
    
class VGAE_Decoder(nn.Module, Decoder):
    def __init__(self, config_model):
        super(VGAE_Decoder, self).__init__()
        self.latent_dim = config_model['latent_dim']

        #Version
        self.lin = nn.Linear(self.latent_dim, self.latent_dim)
    
    def forward(self, z, edge_index, test=False, version=1, sigmoid: bool=True):
        if not test:    # for training
            if version==1:
                value = (z[edge_index[0]] * z[edge_index[1]]).sum(dim=1)

            else:
                z_cur = self.lin(z)
                value = (z_cur[edge_index[0]] * z[edge_index[1]]).sum(dim=1)
            
            out = torch.sigmoid(value) if sigmoid else value
        
        else:           # for testing
            batch = edge_index      #method overload edge_index here only container
            graph_list = []
            z_cur = self.lin(z)

            for b in range(batch.max().item()):
                if version==1:
                    m = z[batch == b]
                    matrix = torch.matmul(z[batch == b], z[batch == b].transpose(0,1))
                else:
                    matrix = torch.matmul(z_cur[batch == b], z[batch == b].transpose(0,1))
                matrix = torch.sigmoid(matrix) if sigmoid else matrix
                matrix = torch.round(matrix)
                edge_list,_ = pyg_utils.dense_to_sparse(matrix)
                edge_list,_ = pyg_utils.remove_self_loops(edge_list)
                graph = data.Data(x=z[batch == b], edge_index=edge_list)
                graph_list.append(graph)
            
            out = graph_list

        return out
