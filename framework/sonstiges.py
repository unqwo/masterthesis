import numpy as np
import torch
import torch_geometric.datasets as datasets
import torch_geometric.data as data
import torch_geometric.transforms as transforms
from torch_geometric.data import HeteroData
import matplotlib.pyplot as plt
import networkx as nx
from torch_geometric.utils.convert import to_networkx, to_scipy_sparse_matrix

def probe1():
    # Anzahl der Knoten und Kanten festlegen
    num_nodes = 5
    num_edges = 10

    # Zufällige Knotenfeatures (zum Beispiel 3 Features pro Knoten)
    x = torch.randn((num_nodes, 3), dtype=torch.float)

    # Zufällige Kantenindizes
    edge_index = torch.tensor(np.random.randint(0, num_nodes, (2, num_edges)), dtype=torch.long)

    # Zufällige Kantenfeatures (zum Beispiel 2 Features pro Kante)
    edge_attr = torch.randn((num_edges, 2), dtype=torch.float)

    ys = torch.rand((5)).round().long()

    # Erstelle das Data-Objekt
    graph = data.Data(x=x, edge_index=edge_index)

    print(graph)

    for prop in graph:
        print(prop)

    adj_matrix = to_scipy_sparse_matrix(graph.edge_index)

    # Konvertiere die sparse matrix zu einer dichten matrix (optional)
    adj_matrix_dense = adj_matrix.toarray()

def probe2():
    def create_heterodata_example(num_views, num_nodes):
        data = HeteroData()
        for view in range(num_views):
            edge_index = torch.randint(0, num_nodes, (2, 45))  # Beispielhafte Adjazenzmatrix
            data[f'edge_type_{view}'].edge_index = edge_index
        return data

    # Beispiel: Erstellen eines HeteroData-Objekts mit 6 Views
    num_views = 6
    num_nodes = 100
    data = create_heterodata_example(num_views, num_nodes)
    print(data.metadata()[0])
    adj_matrices = [data[f'{view}'].edge_index for view in data.metadata()[0]]
    print(data)
    print(adj_matrices)
    #print(data.edge_items[0])
def probe3():
    for i in range(0,10,3):
        print(i)
probe3()