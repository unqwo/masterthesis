from models import *

class ModelZoo:
    

    def __init__(self):
        self.models = {
            'TenGAN': TenGAN,
            'MolGAN': MolGAN,
            'MPGVAE': MPGVAE,
            'SNDVAE': SNDVAE,
            'VGAE': VGAE,
            #'GDSS': GDSS
        }

    def get_model(self, model_name, config_model, device):
        '''Given a model name, returns the model class'''
        if model_name not in self.models:
            raise Exception(
                f'Unknown model specified. Valid options are: {self.models.keys()}')
        model = self.models[model_name](config_model, device)
        model.build_model()
        return model

    def has_model(self, model_name):
        '''Given a model name, return whether or not it exists'''
        return model_name in self.models
