import torch
import torch.nn as nn
import numpy as np

class Sampler():
    def __init__(self) -> None:
        z = 0

class TenGAN_Sampler():
    def sample_latent(self, num_samples, latent_dim):
        return torch.randn((num_samples, latent_dim))

class MolGAN_Sampler():
    def sample_latent(self, batch_size, z_dim):
        return torch.randn((batch_size, z_dim))

class MPGVAE_Sampler():
    def sample_latent(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std
    def sample_latent_test(self, batch_size, z_dim):
        return torch.randn((batch_size, z_dim))

class SNDVAE_Sampler():
    def sample_latent(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std
    def sample_latent_test(self, batch_size, z_dim):
        return torch.randn((batch_size, z_dim))

class VGAE_Sampler():
    def sample_latent(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std
    def sample_latent_test(self, batch_size, z_dim, stats):
        indices = stats[0] + stats[1]*torch.randn((batch_size))
        indices = np.maximum(indices.int(), 2)
        batch = torch.cat([torch.full((x,), i) for i, x in enumerate(indices)])
        z = torch.randn((batch.size(0), z_dim))
        return z, batch
