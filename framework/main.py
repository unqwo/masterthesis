from zoo import *
from dsloader import Ds_deliverer
from Configurations import *
import Evaluation as eva
import torch_geometric.loader as loaders # type: ignore
from torch_geometric.data import Data



device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(f'{device} is enabled')

ds_name = 'zinc'
model_name = 'VGAE'

config_model = Configuration_Model.get_config(model_name)
config_train = Configuration_Train.get_config(model_name)

print('---------------Dataset---------------')
#load data in data variables
data_set = Ds_deliverer.get_ds(name = ds_name)[:1]

#probe
# perm_tens = np.zeros((2, 6, 50, 50))
# from torch_geometric.data import HeteroData
# gs = []
# for g in perm_tens:
#     hetData = data.HeteroData()
#     for j in range(g.shape[0]):
#         idx = f'edge_type_{j}'
#         hetData[idx].edge_index = g[j]
    
#     gs.append(hetData)
# data_set = CustomDataset(gs)

batch_size = config_train['batch_size']
data_loader = loaders.DataLoader(data_set, batch_size=batch_size)
#add data-specific model configurations
if isinstance(data_set[0], Data):
    config_model['num_node_features'] = data_set[0].x.size(1) if data_set[0].x is not None else 0
    config_model['num_edge_features'] = data_set[0].edge_attr.size(1) if data_set[0].edge_attr is not None else 0
    if ds_name in ['zinc', 'qm9', 'waxman', 'cora']:
        max_nodes = 0
        graph_sizes = []
        for datas in tqdm(data_set):
            num_nodes = datas.x.size(0)
            graph_sizes.append(num_nodes) 
            if num_nodes > max_nodes:
                max_nodes = num_nodes
        config_model['vertexes'] = max_nodes
        graph_sizes = np.array(graph_sizes)
        config_model['graph_size_mean'] = graph_sizes.mean()
        config_model['graph_size_var'] = graph_sizes.var()


# build model and get model
zoo = ModelZoo()
model = zoo.get_model(model_name, config_model, device).to(device)

#train model
optim_strat = config_train['optimizer']
model.set_optimizers(optim_strat = optim_strat, config_train = config_train)

print('-------------------------------Training-------------------------------')
model.train(data_loader, epochs=5000, config_train = config_train)

#test model
num_graphs = 100
data_set_generated = model.test(num_graphs=num_graphs)
benchmarks = eva.evaluator(data_set, data_set_generated, num_graphs)
print(benchmarks)

rounds = 0
for r in range(rounds):
    model.train(data_loader, epochs=4000, config_train = config_train)

    # #test model
    num_graphs = 100
    data_set_generated = model.test(num_graphs=num_graphs)
    benchmarks = eva.evaluator(data_set, data_set_generated, num_graphs)
    print(benchmarks)
    
end = 0