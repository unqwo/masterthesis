from abc import ABC, abstractmethod
import torch
import torch.nn as nn
from torch.utils.data.sampler import RandomSampler
from torch.utils.data import Dataset
import torch.utils.data as utils
from torch_geometric.utils import to_dense_adj, remove_self_loops, add_self_loops, batched_negative_sampling
from torch_geometric.loader import DataLoader
from torch_geometric.data import HeteroData
import numpy as np
from Losses import *
from Decoder import *
from Encoder import *
from Sampler import *
from dsloader import *
from tqdm import tqdm
import time
import datetime
import torch.nn.functional as F
import copy
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree


class BaseModel(nn.Module, ABC):
    def __init__(self, config_model, device) -> None:
        super(BaseModel, self).__init__()
        self.config_model = config_model
        self.device= device
        self.to_log = []
    def build_model(self):
        self.encoders = self.build_encoder()
        self.sampler = self.build_sampler()
        self.decoders = self.build_decoder()

        for encoder in self.encoders: encoder.to(self.device)
        for decoder in self.decoders: decoder.to(self.device)
    def build_encoder(self) -> Encoder:
        pass
    def build_sampler(self) -> Sampler:
        pass
    def build_decoder(self) -> Decoder:
        pass
    def set_optimizers(self) -> None:
        self.optimizer_dic = {'RMSprop': torch.optim.RMSprop,
                              'Adam': torch.optim.Adam}
    def train(self) -> None:
        pass 
    def test(self) -> None:
        pass

class TenGAN(BaseModel):
    def __init__(self, config_model, device) -> None:
        super(TenGAN, self).__init__(config_model, device)
        
    def build_encoder(self):
        return [TenGAN_Encoder(self.config_model)]
    def build_decoder(self):
        return [TenGAN_Decoder(self.config_model)]
    def build_sampler(self) -> Sampler:
        return TenGAN_Sampler()
    def set_optimizers(self, optim_strat = 'RMSprop', config_train = None) -> list:
        super().set_optimizers()
        optimizer_G = self.optimizer_dic[optim_strat](self.decoders[0].parameters(), lr=config_train['gen_lr'])
        optimizer_D = self.optimizer_dic[optim_strat](self.encoders[0].parameters(), lr=config_train['disc_lr'])
        self.optimizers = [optimizer_D, optimizer_G]
    
    def train(self, data_loader, epochs, config_train):
        self.encoder = self.encoders[0]
        self.decoder = self.decoders[0]
        self.epoch_print_every=config_train['epoch_print_every']
        self.print_every=config_train['print_every']
        #checkpoint_path=SAVE_PATH,
        self.critic_iterations=config_train['critic_iterations']
        self.generator_iterations=config_train['generator_iterations']
        self.checkpoint_every=config_train['save_interval']
        self.rank_lambda=config_train['rank_lambda']
        self.penalty_type=config_train['penalty_type']
        #wandb=wandb,
        self.rank_penalty_method=config_train['rank_penalty_method']
        self.n_graph_sample_batches=config_train['n_graph_sample_batches']
        self.batch_size=config_train['batch_size']
        self.eval_every=config_train['eval_every']
        self.eval_method=config_train['eval_method']
        self.given_raw_graphs=config_train['uses_graphtool']

        self.num_steps = 0
        self.num_nodes =self.config_model['slice_size']

        #load data in train_dataset, add zero padding
        data_loader = self.geometric2tensors(data_loader)

        print('Progress epochs')
        for epoch in tqdm(range(epochs)):
            for i, data in enumerate(data_loader):
                self.num_steps += 1
                if self.num_steps % self.generator_iterations == 0:
                    self._critic_train_iteration(data[0])
                # Only update generator every |critic_iterations| iterations
                if self.num_steps % self.critic_iterations == 0:
                    self._generator_train_iteration(data[0])
                break
        print(self.to_log)

    def test(self, num_graphs):
        latent_samples = self.sampler.sample_latent(num_graphs, self.decoder.latent_dim) # evtl. Klasse Variable
        latent_samples = latent_samples.to(self.device)
        generated_data = self.decoder(latent_samples).detach().cpu().numpy()

        data_set_generated = self.tensor2geometric(generated_data)

        return data_set_generated
    
    ############################################### helper functions #####################################################################################
    def geometric2tensors(self, data_loader):
        #load data in train_dataset, add zero padding
        data_set = data_loader.dataset
        train_dataset = []
        for data in tqdm(data_set):
            max_size = self.num_nodes
            padded_tensors = []
            for edge_type in data.edge_types:
                adj = to_dense_adj(data[edge_type].edge_index)
                pad_size = max_size - adj.size(1)
                if pad_size < 0:
                    raise Exception(f'graphs too big, increase slice_size')
                padded_adj = torch.nn.functional.pad(adj, (0, pad_size, 0, pad_size))
                padded_tensors.append(padded_adj)
            ten = torch.stack(padded_tensors).squeeze(1)    
            train_dataset.append(ten)
        train_dataset = torch.stack(train_dataset).permute(1,2,3,0)

        class BasicDataset (Dataset):
            def __init__(self, data):
                self.cube = data

            def __len__(self):
                return self.cube.shape[-1]

            def __getitem__(self, idx):
                return (self.cube[:, :, :, idx], 0)
        train_dataset = BasicDataset(train_dataset)
        rand_sampler = RandomSampler(train_dataset)
        data_loader = utils.DataLoader(train_dataset, batch_size=data_loader.batch_size, sampler=rand_sampler)

        return data_loader
    
    def tensor2geometric(self, perm_tens):
        gs = []
        for g in perm_tens:
            hetData = data.HeteroData()
            for j in range(g.shape[0]):
                idx = f'edge_type_{j}'
                edge_list, _ = dense_to_sparse(torch.from_numpy(g[j]))
                edge_list = edge_list[edge_list[:, 0] != edge_list[:, 1]]
                hetData['x', idx, 'x'].edge_index = edge_list
            
            gs.append(hetData)
        perm_tens_dataset = CustomDataset(gs)

        return perm_tens_dataset

    def _critic_train_iteration(self, data):
        batch_size = data.size()[0]
        latent_samples = self.sampler.sample_latent(batch_size, self.decoder.latent_dim)
        latent_samples = latent_samples.to(self.device)

        # real and generated data
        data = data.to(self.device)
        generated_data = self.decoder(latent_samples)

        # Calculate probabilities on real and generated data
        discr_real = self.encoder(data)[0]
        discr_generated = self.encoder(generated_data)[0]

        gradient_penalty = Losses._gradient_penalty(real_data=data, generated_data=generated_data, D=self.encoder, device= self.device) # weight clipping - take interpolated input
        consistency_term = Losses._consistency_term(real_data=data, D=self.encoder) #for similar input penalize huge differences in outputs

        discr_opt = self.optimizers[0]
        discr_opt.zero_grad()
        discr_loss = discr_generated.mean() - discr_real.mean() + gradient_penalty + consistency_term
        if self.num_steps % 10 == 0: 
            self.to_log.append(f'Discriminator loss: {discr_loss}')
        discr_loss.backward()
        discr_opt.step()

    def _generator_train_iteration(self, data):
        gen_opt = self.optimizers[1]
        gen_opt.zero_grad()

        batch_size = data.size()[0]
        latent_samples = self.sampler.sample_latent(batch_size, self.decoder.latent_dim) # evtl. Klasse Variable
        latent_samples = latent_samples.to(self.device)
        
        data = data.to(self.device)
        generated_data = self.decoder(latent_samples)

        # Calculate loss and optimize
        discr_generated = self.encoder(generated_data)[0]
        gen_loss = - discr_generated.mean()
        if self.num_steps % 10 == 0: 
            self.to_log.append(f'Generator loss: {gen_loss}')
        gen_loss.backward()
        gen_opt.step()

class MolGAN(BaseModel):
    def __init__(self, config_model, device) -> None:
        super(MolGAN, self).__init__(config_model, device)
        self.z_dim = config_model['z_dim']
        self.g_conv_dim = config_model['g_conv_dim']
        self.d_conv_dim = config_model['d_conv_dim']
        self.g_repeat_num = config_model['g_repeat_num']
        self.d_repeat_num = config_model['d_repeat_num']
        self.lambda_cls = config_model['lambda_cls']
        self.lambda_rec = config_model['lambda_rec']
        self.lambda_gp = config_model['lambda_gp']
        self.post_method = config_model['post_method']
        self.dropout = config_model['dropout']
        self.device= device
        self.bond_num_types =config_model['num_edge_features']
        self.atom_num_types = config_model['num_node_features'] 
        self.vertexes = config_model['vertexes']

    def build_encoder(self):
        D = MolGAN_Encoder(self.config_model)
        V = MolGAN_Encoder(self.config_model)
        return [D,V]
    def build_sampler(self) -> Sampler:
        return MolGAN_Sampler()
    def build_decoder(self) -> Decoder:
        return [MolGAN_Decoder(self.config_model)]
    def set_optimizers(self, optim_strat = 'Adam', config_train = None) -> list:
        super().set_optimizers()
        g_optimizer = self.optimizer_dic[optim_strat](list(self.decoders[0].parameters()), config_train['g_lr'], [config_train['beta1'], config_train['beta2']])
        d_optimizer = self.optimizer_dic[optim_strat](self.encoders[0].parameters(), config_train['d_lr'], [config_train['beta1'], config_train['beta1']])
        self.optimizers = [d_optimizer, g_optimizer]
    def train(self, data_loader, epochs=10, config_train = None):
        if True:
            self.batch_size = config_train['batch_size']
            num_iters = epochs
            num_iters_decay = config_train['num_iters_decay']
            n_critic = config_train['n_critic']
            beta1 = config_train['beta1']
            beta2 = config_train['beta2']
            resume_iters = config_train['resume_iters']
            test_iters = config_train['test_iters']
            num_workers = config_train['num_workers']
            mode = config_train['mode']
            use_tensorboard = config_train['use_tensorboard']
            mol_data_dir = config_train['mol_data_dir']
            log_dir = config_train['log_dir']
            model_save_dir = config_train['model_save_dir']
            sample_dir = config_train['sample_dir']
            result_dir = config_train['result_dir']
            sample_step = config_train['sample_step']
            log_step = config_train['log_step']
            model_save_step = config_train['model_save_step']
            lr_update_step = config_train['lr_update_step']
            # Learning rate cache for decaying.
            g_lr = config_train['g_lr']
            d_lr = config_train['d_lr']

            self.D = self.encoders[0]
            self.V = self.encoders[1]
            self.G = self.decoders[0]
        # Start training from scratch or resume training.
        start_iters = 0
        if resume_iters:
            start_iters = resume_iters
            self.restore_model(resume_iters)

        # Start training.
        print('Start training...')
        start_time = time.time()

        ds = data_loader.dataset
        ds_batched = self.geometric2list(ds)

        for epoch in tqdm(range(epochs)):
            for i, batch in enumerate(ds_batched):

                x_tensor = torch.from_numpy(np.stack(batch[:,0], axis=0)).to(self.device)
                a_tensor = torch.from_numpy(np.stack(batch[:,1], axis=0)).to(self.device)                    

                z = self.sampler.sample_latent(batch.shape[0], self.z_dim).to(self.device).float()

                # =================================================================================== #
                #                             2. Train the discriminator                              #
                # =================================================================================== #

                # Compute loss with real images.
                
                logits_real, features_real = self.D(a_tensor, None, x_tensor)
                d_loss_real = - torch.mean(logits_real)

                # Compute loss with fake images.
                edges_logits, nodes_logits = self.G(z)
                # Postprocess with Gumbel softmax
                (edges_hat, nodes_hat) = self.postprocess((edges_logits, nodes_logits), self.post_method)
                logits_fake, features_fake = self.D(edges_hat, None, nodes_hat)
                d_loss_fake = torch.mean(logits_fake)

                # Compute loss for gradient penalty. -interpolation between real and fake
                eps = torch.rand(logits_real.size(0),1,1,1).to(self.device)
                x_int0 = (eps * a_tensor + (1. - eps) * edges_hat).requires_grad_(True)
                x_int1 = (eps.squeeze(-1) * x_tensor + (1. - eps.squeeze(-1)) * nodes_hat).requires_grad_(True)
                grad0, grad1 = self.D(x_int0, None, x_int1)
                d_loss_gp = Losses.gradient_penalty(grad0, x_int0, self.device) + Losses.gradient_penalty(grad1, x_int1, self.device)


                # Backward and optimize.
                d_loss = d_loss_fake + d_loss_real + self.lambda_gp * d_loss_gp   # WGAN objective
                self.reset_grad()
                d_loss.backward()
                self.optimizers[0].step()

                # Logging.
                loss = {}
                loss['D/loss_real'] = d_loss_real.item()
                loss['D/loss_fake'] = d_loss_fake.item()
                loss['D/loss_gp'] = d_loss_gp.item()
                if (epoch)%log_step == 0 and (i % 10 == 0):
                    #self.to_log.append(f'D/loss_real: {d_loss_real}, D/loss_fake: {d_loss_fake}, D/loss_gp: {d_loss_gp}')
                    self.to_log.append(f'D/loss: {d_loss}')

                # =================================================================================== #
                #                               3. Train the generator                                #
                # =================================================================================== #

                if (i+1) % n_critic == 0:
                    # Z-to-target
                    edges_logits, nodes_logits = self.G(z)
                    # Postprocess with Gumbel softmax
                    (edges_hat, nodes_hat) = self.postprocess((edges_logits, nodes_logits), self.post_method)
                    logits_fake, features_fake = self.D(edges_hat, None, nodes_hat)
                    g_loss_fake = - torch.mean(logits_fake)
                    g_loss = g_loss_fake
       
                    # Backward and optimize.
                    self.reset_grad()
                    g_loss.backward()
                    self.optimizers[1].step()

                    # Logging.
                    loss['G/loss_fake'] = g_loss_fake.item()
                    if (epoch)%log_step == 0 and ((i+1) % 10 == 0):
                         self.to_log.append(f'G/loss_fake: {g_loss_fake}')



                # =================================================================================== #
                #                                 4. Miscellaneous                                    #
                # =================================================================================== #

                # Print out training information.
                if (i+1) % log_step == 0:
                    et = time.time() - start_time
                    et = str(datetime.timedelta(seconds=et))[:-7]
                    log = "Elapsed [{}], Iteration [{}/{}]".format(et, i+1, num_iters)

                    for tag, value in loss.items():
                        log += ", {}: {:.4f}".format(tag, value)
                    #print(log)

                # Decay learning rates.
                if (i+1) % lr_update_step == 0 and (i+1) > (num_iters - num_iters_decay):
                    g_lr -= (g_lr / float(num_iters_decay))
                    d_lr -= (d_lr / float(num_iters_decay))
                    self.update_lr(g_lr, d_lr)
                    print ('Decayed learning rates, g_lr: {}, d_lr: {}.'.format(g_lr, d_lr))
        print(self.to_log)

    def test(self, num_graphs):
        z = self.sampler.sample_latent(num_graphs, self.z_dim).to(self.device).float()

        edges_logits, nodes_logits = self.G(z)
        # Postprocess with Gumbel softmax
        (edges_hat, nodes_hat) = self.postprocess((edges_logits, nodes_logits), self.post_method)

        # num_nodes = torch.randint(8, 50, (num_graphs,))
        # graph = self.decoders[0](latent_samples, num_nodes)
        # x, edge_index, edge_attr, ptr, batch = graph
        # gs = []
        # for i in range(ptr.size(0) - 1):
        #     start, end = ptr[i], ptr[i + 1]
            
        #     x_sub = x[start:end]
        #     mask = (edge_index[0] >= start) & (edge_index[0] < end) & (edge_index[1] >= start) & (edge_index[1] < end)
        #     edge_index_sub = edge_index[:, mask] - start

        #     edge_attr_sub = edge_attr[mask]
        #     homData = data.Data(x=x_sub, edge_index=edge_index_sub, edge_attr=edge_attr_sub)               
        #     gs.append(homData)
        # data_set_generated = CustomDataset(gs)
        #return data_set_generated
    ############################################### helper functions #####################################################################################
    def geometric2list(self, ds):
        ds_batched = []
        for j in range(int(np.ceil(len(ds)/self.batch_size))):
            batch = []
            for i in range(min(len(ds)-j*self.batch_size, self.batch_size)):
                dat = ds[j*self.batch_size + i]
                x, edge_index, edge_attr = dat.x, dat.edge_index, dat.edge_attr
                edge_attr = to_dense_adj(edge_index, edge_attr=edge_attr).squeeze()
                init_x = torch.zeros((self.config_model['vertexes'], self.config_model['num_node_features']))
                init_edge_attr = torch.zeros((self.config_model['vertexes'], self.config_model['vertexes'], self.config_model['num_edge_features']))
                init_x[:x.size(0),:x.size(1)] = x
                init_edge_attr[:edge_attr.size(0), :edge_attr.size(1), :edge_attr.size(2)] = edge_attr
                batch.append([init_x, init_edge_attr])
            ds_batched.append(np.array(batch))
        return ds_batched

    def preprocess(self, batch):
        adj_list = []
        x_list = []
        for data in batch:
            print(data)
            adj = to_dense_adj(data.edge_index, edge_attr=data.edge_attr)
            adj_list.append(adj)
            x_list.append(data.x)

        a_tensor = torch.cat(adj_list, dim=0)
        x_tensor = torch.cat(x_list, dim=0)
        return a_tensor, x_tensor

    def update_lr(self, g_lr, d_lr):
        """Decay learning rates of the generator and discriminator."""
        for param_group in self.optimizers[0].param_groups:
            param_group['lr'] = g_lr
        for param_group in self.optimizers[1].param_groups:
            param_group['lr'] = d_lr

    def reset_grad(self):
        """Reset the gradient buffers."""
        self.optimizers[0].zero_grad()
        self.optimizers[1].zero_grad()

    def denorm(self, x):
        """Convert the range from [-1, 1] to [0, 1]."""
        out = (x + 1) / 2
        return out.clamp_(0, 1)

    def label2onehot(self, labels, dim):
        """Convert label indices to one-hot vectors."""
        out = torch.zeros(list(labels.size())+[dim]).to(self.device)
        out.scatter_(len(out.size())-1,labels.unsqueeze(-1),1.)
        return out

    def classification_loss(self, logit, target, dataset='CelebA'):
        """Compute binary or softmax cross entropy loss."""
        if dataset == 'CelebA':
            return F.binary_cross_entropy_with_logits(logit, target, size_average=False) / logit.size(0)
        elif dataset == 'RaFD':
            return F.cross_entropy(logit, target)

    def sample_z(self, batch_size):
        return np.random.normal(0, 1, size=(batch_size, self.z_dim))

    def postprocess(self, inputs, method, temperature=1.):

        def listify(x):
            return x if type(x) == list or type(x) == tuple else [x]

        def delistify(x):
            return x #if len(x) > 1 else x[0]

        if method == 'soft_gumbel':
            softmax = [F.gumbel_softmax(e_logits.contiguous().view(-1,e_logits.size(-1))
                       / temperature, hard=False).view(e_logits.size())
                       for e_logits in listify(inputs)]
        elif method == 'hard_gumbel':
            softmax = [F.gumbel_softmax(e_logits.contiguous().view(-1,e_logits.size(-1))
                       / temperature, hard=True).view(e_logits.size())
                       for e_logits in listify(inputs)]
        else:
            softmax = [F.softmax(e_logits / temperature, -1)
                       for e_logits in listify(inputs)]

        return [delistify(e) for e in (softmax)]

class MPGVAE(BaseModel):
    def __init__(self, config_model, device):
        super(MPGVAE, self).__init__(config_model, device)
    def build_encoder(self) -> Encoder:
        return [MPGVAE_Encoder(self.config_model)]
    def build_sampler(self) -> Sampler:
        return MPGVAE_Sampler()
    def build_decoder(self) -> Decoder:
        return [MPGVAE_Decoder(self.config_model)]
    def set_optimizers(self, optim_strat, config_train) -> None:
        super().set_optimizers()
        opt_enc = self.optimizer_dic[optim_strat](self.encoders[0].parameters(), lr=config_train['lr'])
        opt_dec = self.optimizer_dic[optim_strat](self.decoders[0].parameters(), lr=config_train['lr'])
        self.optimizers = [opt_enc, opt_dec]

    def forward(self, data):
        x, edge_index, edge_attr, batch = data.x, data.edge_index, data.edge_attr, data.batch
        
        # Encoding
        h_T, mu, logvar = self.encoders[0](x, edge_index, edge_attr, batch)
        z = self.sampler(mu, logvar)

        graph = self.decoders[0](z, num_nodes=x.size(0))
        
        return graph, mu, logvar
    
    def train(self, data_loader, epochs=10, config_train = None):
        for epoch in tqdm(range(epochs)):
            for i, data in enumerate(data_loader):
                data = data.to(self.device)
                [optimizer.zero_grad() for optimizer in self.optimizers]
                x, edge_index, edge_attr, batch, ptr = data.x, data.edge_index, data.edge_attr, data.batch, data.ptr
                #edge_attr = to_dense_adj(edge_index, edge_attr=edge_attr).squeeze()
                #edge_index = to_dense_adj(edge_index)
                # Encoding
                h_T, mu, logvar = self.encoders[0](x, edge_index, edge_attr, batch, ptr)
                z = self.sampler.sample_latent(mu, logvar)

                graph = self.decoders[0](z, num_nodes=batch.bincount())

                x_recon, edge_index_recon, edge_attr_recon, ptr_recon, batch_recon = graph
                edge_attr_recon_cube = to_dense_adj(edge_index_recon, edge_attr=edge_attr_recon, batch=batch_recon)
                real_edge_attr_cube = to_dense_adj(edge_index, edge_attr=edge_attr, batch=batch)

                loss = Losses.MPGVAE_loss([x_recon, x], [edge_attr_recon_cube, real_edge_attr_cube] , mu, logvar)
                if i % 100 == 0 and epoch%10==0:
                    self.to_log.append(f'L:{loss.item()}')
                loss.backward()
                [optimizer.step() for optimizer in self.optimizers]

        print(self.to_log)
        v=0

    def test(self, num_graphs):
        latent_samples = self.sampler.sample_latent_test(num_graphs, self.decoders[0].latent_dim) # evtl. Klasse Variable
        latent_samples = latent_samples.to(self.device)
        num_nodes = torch.randint(8, 50, (num_graphs,))
        graph = self.decoders[0](latent_samples, num_nodes)
        x, edge_index, edge_attr, ptr, batch = graph
        gs = []
        for i in range(ptr.size(0) - 1):
            start, end = ptr[i], ptr[i + 1]
            
            x_sub = x[start:end]
            mask = (edge_index[0] >= start) & (edge_index[0] < end) & (edge_index[1] >= start) & (edge_index[1] < end)
            edge_index_sub = edge_index[:, mask] - start

            edge_attr_sub = edge_attr[mask]
            homData = data.Data(x=x_sub, edge_index=edge_index_sub, edge_attr=edge_attr_sub)               
            gs.append(homData)
        data_set_generated = CustomDataset(gs)
        return data_set_generated
            
class SNDVAE(BaseModel):
    def __init__(self, config_model, device):
        super(SNDVAE, self).__init__(config_model, device)
    def build_encoder(self) -> Encoder:
        return [SNDVAE_Encoder(self.config_model)]
    def build_sampler(self) -> Sampler:
        return SNDVAE_Sampler()
    def build_decoder(self) -> Decoder:
        return [SNDVAE_Decoder(self.config_model)]
    def set_optimizers(self, optim_strat, config_train) -> None:
        super().set_optimizers()
        opt_enc = self.optimizer_dic[optim_strat](self.encoders[0].parameters(), lr=config_train['lr'])
        opt_dec = self.optimizer_dic[optim_strat](self.decoders[0].parameters(), lr=config_train['lr'])
        self.optimizers = [opt_enc, opt_dec]
    def train(self, data_loader, epochs=10, config_train = None):
        # train specifics
        self.config_train = config_train
        self.num_nodes = self.config_model['vertexes']
        self.batch_size = config_train['batch_size']

        ds_batched = self.geometric2specific(data_loader)

        #training
        for epoch in tqdm(range(epochs)):
            for i, data in enumerate(ds_batched):
                [optimizer.zero_grad() for optimizer in self.optimizers]

                # Real data
                features, spatial, adj, rel, adj_truth = data[0], data[1], data[2], data[3], data[4] 
                adj_truth = adj_truth.to(self.device)
                adj = adj.to(self.device)
                features = features.to(self.device)
                spatial = spatial.to(self.device)
                rel = rel.to(self.device)    
                feature_truth = features.clone()
                spatial_truth = spatial.clone()           #B x N x spatial_dim
                rel_truth = rel.clone()                   #B x N x N
                
                # extended data
                #features = features.repeat((config_train['sampling_num'], 1, 1))    #B*sampling_num x N x (F-spatial_dim)
                #spatial = spatial.repeat((config_train['sampling_num'], 1, 1))      #B*sampling_num x N x spatial_dim
                #rel = rel.repeat((config_train['sampling_num'], 1, 1, 1))           #B*sampling_num x N x N
                adj = adj_truth

                # Encoding
                z = self.encoders[0](node_feature=feature_truth, node_feature_sg = features, inputs_3d=spatial_truth, adj=adj_truth, adj_sg=adj, rel_sg=rel)

                z_g = self.sampler.sample_latent(z[0], z[1])
                z_s = self.sampler.sample_latent(z[2], z[3])
                z_sg = self.sampler.sample_latent(z[4], z[5])

                # Decoding
                G_pred = self.decoders[0](z_s, z_sg, z_g)

                # Loss
                G_label = adj_truth, feature_truth, spatial_truth
                loss = Losses.SNDVAE_loss(z, G_pred, G_label, config_train['beta'])

                # log
                if epoch%self.config_train['log_step']==0 and i in [0,500]:
                    #self.to_log.append([loss[0].item(),loss[1].item(), loss[2].item()])
                    self.test(20)
                    #self.to_log.append(f'mean_batch_fake: {self.mean_batch_fake}')
                    #print(f'mean_batch_fake: {self.mean_batch_fake}')
                
                if epoch==9800:
                    stop=3
                
                # Step
                loss[0].backward()
                [optimizer.step() for optimizer in self.optimizers]
        
        print(self.to_log)
    
    def test (self, num_graphs):
        self.num_graphs = num_graphs
        z_g = self.sampler.sample_latent_test(num_graphs, self.decoders[0].g_latent_size).to(self.device)
        z_s = self.sampler.sample_latent_test(num_graphs, self.decoders[0].s_latent_size).to(self.device)
        z_sg = self.sampler.sample_latent_test(num_graphs, self.decoders[0].sg_latent_size).to(self.device)

        graph = self.decoders[0](z_s, z_sg, z_g)
        
        data_set_generated = self.specific2geometric(graph)

        return data_set_generated
    ############################################### helper functions ###################################################################
    def geometric2specific(self, data_loader):
        ds = data_loader.dataset
        max_size = self.num_nodes
        mean_batch = 0
        ds_batched = []
        for j in tqdm(range(int(np.ceil(len(ds)/self.batch_size)))): #j indicates which batch
            batch = []
            for i in range(min(len(ds)-j*self.batch_size, self.batch_size)):    #i indicates which data-object in batch
                dat = ds[j*self.batch_size + i]
                x, edge_index, edge_attr = dat.x, dat.edge_index, dat.edge_attr
                feat_raw = x[:, self.config_model['spatial_dim']:]      #N x (F-spatial_dim)
                spatial_raw = x[:, :self.config_model['spatial_dim']]       #N x (spatial_dim)
                adj = to_dense_adj(edge_index).squeeze()

                pad_size_x = max_size - x.size(0)
                pad_size_adj = max_size - adj.size(1)
                if pad_size_x < 0:
                    raise Exception(f'graphs too big, increase slice_size')
                padded_adj = torch.nn.functional.pad(adj, (0,pad_size_adj , 0, pad_size_adj))
                spatial = torch.nn.functional.pad(spatial_raw, (0, 0, 0, pad_size_x))
                feat = torch.nn.functional.pad(feat_raw, (0, 0, 0, pad_size_x))
                #padded_edge_attr = torch.nn.functional.pad(edge_attr, (0, pad_size, 0, pad_size))
                rel = torch.cdist(spatial, spatial)     #N x N
                adj_truth = padded_adj      #N x N

                if edge_index.size(1) > 0:
                    mean_dist_if_pos = torch.sum(rel*padded_adj).item()/edge_index.size(1)
                    mean_batch += mean_dist_if_pos

                new_new_adj = []

                a, b = torch.where(adj_truth.clone())
                # print (a.shape, b.shape)
                edges = torch.cat((a.reshape(-1, 1), b.reshape(-1, 1)), dim=1)
                new_new_adj_sub = []
                raw_edges = copy.deepcopy(edges)
                for i in range(self.config_train['sampling_num']):
                    edges = self.build_spanning_tree_edge(raw_edges, x.shape[0], len(raw_edges))
                    cur_adj = to_dense_adj(edges).squeeze()
                    cur_adj = torch.nn.functional.pad(cur_adj, (0, pad_size_adj, 0, pad_size_adj))
                    new_new_adj_sub.append(cur_adj)
                new_new_adj.append(new_new_adj_sub)
                adj = torch.stack([torch.stack(sub) for sub in new_new_adj]).squeeze()      #sampling_num x N x N

                batch.append([feat, spatial, adj, rel, adj_truth])      #batch_size x 5
            batch = np.array(batch)
            batch = [torch.stack(tuple(el)) for el in batch.T]
            ds_batched.append(batch)      #num_batches  x  5  x  batch_size

        mean_batch /= len(ds)
        self.to_log.append(f'mean_batch_real: {mean_batch}')
        return ds_batched
    
    def specific2geometric(self, graph):
        edge_index, _, s, g = graph
        x = torch.cat([s,g], dim = -1)
        gs = []
        mean_batch = 0
        for i in range(edge_index.size(0)):
            f = edge_index[i]
            edge_list, _ = dense_to_sparse(edge_index[i])
            rel = torch.cdist(s[i], s[i])
            if edge_list.size(1) > 0:
                mean_dist_if_pos = torch.sum(rel*f).item()/edge_list.size(1)
                mean_batch += mean_dist_if_pos
            homData = data.Data(x=x[i], edge_index=edge_list)               
            gs.append(homData)

        mean_batch /= self.num_graphs
        self.mean_batch_fake = mean_batch
        data_set_generated = CustomDataset(gs)
        return data_set_generated
    
    def build_spanning_tree_edge(self, edge_index, num_nodes=None, num_edges=None):

        spanning_edges = self.scipy_spanning_tree(edge_index.numpy(), num_nodes, num_edges)
        
        spanning_edges = torch.tensor(spanning_edges).T

        return spanning_edges.to(torch.int64)
    
    def scipy_spanning_tree(self, edge_index, num_nodes, num_edges):
        row, col = edge_index[:, 0], edge_index[:, 1]
        cgraph = csr_matrix((np.random.random(num_edges) + 1, (row, col)), shape=(num_nodes, num_nodes))
        Tcsr = minimum_spanning_tree(cgraph)
        tree_row, tree_col = Tcsr.nonzero()
        spanning_edges = np.concatenate([[tree_row], [tree_col]]).T
        return spanning_edges

class VGAE(BaseModel):
    def __init__(self, config_model, device) -> None:
        super(VGAE, self).__init__(config_model, device)
        
    def build_encoder(self):
        return [VGAE_Encoder(self.config_model)]
    def build_decoder(self):
        return [VGAE_Decoder(self.config_model)]
    def build_sampler(self) -> Sampler:
        return VGAE_Sampler()
    def set_optimizers(self, optim_strat = 'Adam', config_train = None) -> list:
        super().set_optimizers()
        optimizer_dec = self.optimizer_dic[optim_strat](self.decoders[0].parameters(), lr=config_train['lr'])
        optimizer_enc = self.optimizer_dic[optim_strat](self.encoders[0].parameters(), lr=config_train['lr'])
        self.optimizers = [optimizer_dec, optimizer_enc]

    def train(self, data_loader, epochs, config_train=None):
        data_loader = self.hetero2homo(data_loader)
        for epoch in tqdm(range(epochs)):
            for i, data in enumerate(data_loader):
                data = data.to(self.device)
                [optimizer.zero_grad() for optimizer in self.optimizers]
                x, pos_edge_index, batch = data.x.clone(), data.edge_index.clone(), data.batch.clone()

                mu, logvar = self.encoders[0](x, pos_edge_index)
                z = self.sampler.sample_latent(mu, logvar)

                # No self-loops in negative samples
                all_edge_index_tmp, _ = remove_self_loops(pos_edge_index)
                all_edge_index_tmp, _ = add_self_loops(all_edge_index_tmp)
                neg_edge_index = batched_negative_sampling(all_edge_index_tmp, batch, pos_edge_index.size(1)*2)
                # full_matrix = torch.cartesian_prod(torch.arange(0, x.size(0)), torch.arange(0, x.size(0))).t().to(self.device)
                # full_edges,_ = remove_self_loops(full_matrix)
                # matches = (full_edges[0, :, None] == pos_edge_index[0]) & (full_edges[1, :, None] == pos_edge_index[1])
                # match_any = matches.any(dim=1)
                # non_matches = ~match_any
                # neg_edge_index = full_edges[:, non_matches]
                if epoch<0:
                    pos_edge_index_hat = self.decoders[0](z, pos_edge_index, test=False, version=1)     # if sigmoid, then challenging because try to achieve extremly high values, but KL divergence hold back
                    neg_edge_index_hat = self.decoders[0](z, neg_edge_index, test=False, version=1)
                else:
                    pos_edge_index_hat = self.decoders[0](z, pos_edge_index, test=False, version=2)
                    neg_edge_index_hat = self.decoders[0](z, neg_edge_index, test=False, version=2)

                pos_rounded = torch.round(pos_edge_index_hat)
                neg_rounded = torch.round(neg_edge_index_hat)

                loss = Losses.VGAE_loss(mu, logvar, neg_edge_index_hat, pos_edge_index_hat)

                if i % 100 == 0 and epoch%10==0:
                    self.to_log.append(f'L:{loss.item()}')
                    if np.isnan(loss.item()):
                        g = 0
                loss.backward()
                [optimizer.step() for optimizer in self.optimizers]

        print(self.to_log)
        v=0

    def test(self, num_graphs):
        latent_samples, batch = self.sampler.sample_latent_test(num_graphs, self.decoders[0].latent_dim, stats=(self.config_model['graph_size_mean'], self.config_model['graph_size_var'])) 
        latent_samples = latent_samples.to(self.device)
        graph_list = self.decoders[0](z=latent_samples, edge_index=batch, test=True)    # here edge_index only container for batch
        data_set_generated = CustomDataset(graph_list)
        return data_set_generated

    def hetero2homo(self, data_loader):
        class CustomDataset_loc(Dataset):
            def __init__(self, hetero_dataset, node_type, edge_type):
                super(CustomDataset_loc, self).__init__()
                self.hetero_dataset = hetero_dataset
                self.edge_type = edge_type
                self.node_type = node_type

            def len(self):
                return len(self.hetero_dataset)

            def get(self, idx):
                hetero_data = self.hetero_dataset[idx]
                edge_index = hetero_data[self.edge_type].edge_index
                x = hetero_data[self.node_type].x  
                return data.Data(edge_index=edge_index)
        
        data_set = data_loader.dataset
        if isinstance(data_set[0], HeteroData):
            f = data_set[0]
            data_set = CustomDataset_loc(data_set,data_set[0].node_types[0], data_set[0].edge_types[0])
            data_loader = DataLoader(data_set)

        return data_loader



        

