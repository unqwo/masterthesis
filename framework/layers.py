import math
import numpy as np
import torch
import torch.nn as nn
from torch.nn.parameter import Parameter
from torch.nn.modules.module import Module
import torch.nn.functional as F

class MolGAN_GraphConvolution(Module):
    def __init__(self, in_features, out_feature_list, b_dim, dropout):
        super(MolGAN_GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_feature_list = out_feature_list

        self.linear1 = nn.Linear(in_features, out_feature_list[0])
        self.linear2 = nn.Linear(out_feature_list[0], out_feature_list[1])

        self.dropout = nn.Dropout(dropout)

    def forward(self, input, adj, activation=None):
        # input : 16x9x9
        # adj : 16x4x9x9

        hidden = torch.stack([self.linear1(input) for _ in range(adj.size(1))], 1)
        hidden = torch.einsum('bijk,bikl->bijl', (adj, hidden))
        hidden = torch.sum(hidden, 1) + self.linear1(input)
        hidden = activation(hidden) if activation is not None else hidden
        hidden = self.dropout(hidden)

        output = torch.stack([self.linear2(hidden) for _ in range(adj.size(1))], 1)
        output = torch.einsum('bijk,bikl->bijl', (adj, output))
        output = torch.sum(output, 1) + self.linear2(hidden)
        output = activation(output) if activation is not None else output
        output = self.dropout(output)
        return output

class MolGAN_GraphAggregation(Module):

    def __init__(self, in_features, out_features, m_dim, dropout):
        super(MolGAN_GraphAggregation, self).__init__()
        self.sigmoid_linear = nn.Sequential(nn.Linear(in_features+m_dim, out_features),
                                            nn.Sigmoid())
        self.tanh_linear = nn.Sequential(nn.Linear(in_features+m_dim, out_features),
                                         nn.Tanh())
        self.dropout = nn.Dropout(dropout)

    def forward(self, input, activation):
        i = self.sigmoid_linear(input)
        j = self.tanh_linear(input)
        output = torch.sum(torch.mul(i,j), 1)
        output = activation(output) if activation is not None\
                 else output
        output = self.dropout(output)

        return output

class TenGAN_GraphConvolution(Module):
    def __init__(self, in_features, out_features, bias=True, device='cuda'):
        super(TenGAN_GraphConvolution, self).__init__()
        cuda = (device == 'cuda' and torch.cuda.is_available())

        self.cuda = cuda
        self.in_features = in_features
        self.out_features = out_features

        FloatTensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
        self.weight = Parameter(FloatTensor(in_features, out_features))
        if bias:
            self.bias = Parameter(FloatTensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, adj = None):
        if adj is None:
#             print('Adj: {}'.format(input.shape))
            support = self.weight
            adj = input # evtl float not necessary
        else:
            support = torch.matmul(input, self.weight)  # use broadcasting
        output = torch.matmul(adj, support)
#         output = torch.spmm(adj, support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'

class Layer(Module):
    def __init__(self, name=None):
        super(Layer, self).__init__()

    def forward(self, inputs):
        return inputs

class SNDVAE_GraphConvolution(nn.Module): #validated
    def __init__(self, input_dim, output_dim, dropout=0., act=F.relu):
        super(SNDVAE_GraphConvolution, self).__init__()
        self.weights = nn.Parameter(torch.randn(input_dim, output_dim) * 0.02) #stddev=0.02
        self.dropout = dropout
        self.act = act

    def forward(self, x, adj):
        x = F.dropout(x, p=self.dropout, training=self.training)
        x = torch.matmul(x, self.weights)
        x = torch.matmul(adj, x)
        return self.act(x)

def dropout_sparse(x, keep_prob):
    # In PyTorch, create a dropout mask that matches the sparse tensor's format
    mask = torch.rand(x._nnz()) < keep_prob
    return x._indices()[mask], x._values()[mask] * (1.0 / keep_prob), x.size()


class SpatialGraphConvolutionLayer(nn.Module):
    def __init__(self, input_dim, hidden_sizes, rel_dim, bias_start=0.0):
        super(SpatialGraphConvolutionLayer, self).__init__()
        self.hidden_size = hidden_sizes
        self.matrix1 = nn.Parameter(torch.randn(input_dim * 3 + rel_dim * 2 + 1, hidden_sizes[0]) * 0.02)
        self.bias1 = nn.Parameter(torch.full((hidden_sizes[0],), bias_start))
        self.matrix2 = nn.Parameter(torch.randn(input_dim * 2 + hidden_sizes[0] + rel_dim, hidden_sizes[1]) * 0.02)
        self.bias2 = nn.Parameter(torch.full((hidden_sizes[1],), bias_start))
        self.matrix3 = nn.Parameter(torch.randn(input_dim + hidden_sizes[1], hidden_sizes[2]) * 0.02)
        self.bias3 = nn.Parameter(torch.full((hidden_sizes[2],), bias_start))

        self.lrelu = nn.LeakyReLU()

    def forward(self, input_, adj, rel):
        num_batch, num_node, input_dim = input_.shape
        rel_dim = 1

        rel_ij = rel.view(num_batch, num_node, num_node, 1, -1).expand(-1, -1, -1, num_node, -1)
        rel_jk = rel.view(num_batch, 1, num_node, num_node, -1).expand(-1, num_node, -1, -1, -1)
        dis_ik = rel.view(num_batch, num_node, 1, num_node, -1).expand(-1, -1, num_node, -1, -1)
        adj_3d = torch.mul(
            adj.view(num_batch, num_node, num_node, 1).expand(-1, -1, -1, num_node),
            adj.view(num_batch, 1, num_node, num_node).expand(-1, num_node, -1, -1)
        )
        
        # what happens with batch_size*sampling_num?
        # Expand and repeat tensors to match shapes for operations
        node_feat_x = input_.view(num_batch, num_node, 1, 1, -1).expand(-1, -1, num_node, num_node, -1)
        node_feat_y = input_.view(num_batch, 1, num_node, 1, -1).expand(-1, num_node, -1, num_node, -1)
        node_feat_z = input_.view(num_batch, 1, 1, num_node, -1).expand(-1, num_node, num_node, -1, -1)

        # Concatenate features for matmul operations
        m3 = torch.cat([node_feat_x, node_feat_y, node_feat_z, rel_ij, rel_jk, dis_ik], dim=-1)  # B*N*N*N*h'
        m3 = m3.view(-1, input_dim * 3 + rel_dim * 2 + 1)
        m3 = torch.matmul(self.lrelu(m3), self.matrix1) + self.bias1  # B*N*N*N*h'' 
        m3 = m3.view(num_batch, num_node, num_node, num_node, self.hidden_size[0])                        
        m3 = m3.permute(0, 1, 2, 4, 3)  # B*N*N*h''*N
        m3_sum = torch.matmul(m3, adj_3d.view(num_batch, num_node, num_node, num_node, 1))  # B*N*N*h''*1
        m3_sum = m3_sum.view(num_batch, num_node, num_node, -1)  # B*N*N*h''


        node_feat_x = input_.view(num_batch, num_node, 1, -1).expand(-1, -1, num_node, -1)
        node_feat_y = input_.view(num_batch, 1, num_node, -1).expand(-1, num_node, -1, -1)


        m2 = torch.cat([node_feat_x, node_feat_y, rel.view(num_batch, num_node, num_node, -1), m3_sum], dim=3)  # B*N*N*h'
        m2 = m2.view(-1, input_dim * 2 + self.hidden_size[0] + rel_dim)
        m2 = torch.matmul(self.lrelu(m2), self.matrix2) + self.bias2
        m2 = m2.view(num_batch, num_node, num_node, self.hidden_size[1])
        m2 = m2.permute(0, 1, 3, 2)
        m2_sum = torch.matmul(m2, adj.view(num_batch, num_node, num_node, 1))  # B*N*h*1
        m2_sum = m2_sum.view(num_batch, num_node, self.hidden_size[1])

        m1 = torch.cat([input_, m2_sum], dim=2)
        m1 = m1.view(-1, input_dim + self.hidden_size[1])
        m1 = torch.matmul(self.lrelu(m1), self.matrix3) + self.bias3
        output = m1.view(num_batch, num_node, -1)

        return output

class N2G(Layer):
    """Aggregate node representation to graph level."""
    def __init__(self, input_dim, batch_size, dropout=0., act=F.relu):
        super(N2G, self).__init__()
        self.input_dim = input_dim
        self.batch_size = batch_size
        self.dropout = dropout
        self.act = act
        # Initialize weight with a Glorot initializer equivalent
        self.weights = nn.Parameter(torch.Tensor(batch_size, input_dim, 20))
        nn.init.xavier_uniform_(self.weights)

        # Diagonal mask to apply weights only on the diagonal elements of the input
        self.register_buffer('diag_mask', torch.eye(input_dim).unsqueeze(0).repeat(batch_size, 1, 1))

    def forward(self, x):
        x = F.dropout(x, self.dropout, training=self.training)
        # Apply diagonal mask to weights
        weighted = torch.matmul(self.weights, x) * self.diag_mask
        return self.act(weighted)

class G2N(Layer):
    """Assign graph representation to node level."""
    def __init__(self, input_dim, batch_size, dropout=0., act=F.relu):
        super(G2N, self).__init__()
        self.dropout = dropout
        self.act = act
        # Initialize weight with a Glorot initializer equivalent
        self.weights = nn.Parameter(torch.Tensor(batch_size, 20, input_dim))
        nn.init.xavier_uniform_(self.weights)

    def forward(self, x):
        x = F.dropout(x, self.dropout, training=self.training)
        output = torch.matmul(self.weights, x)
        return self.act(output)

class InnerProductDecoder(nn.Module):
    def __init__(self, dropout=0., activation=torch.sigmoid):
        super(InnerProductDecoder, self).__init__()
        self.dropout = dropout
        self.activation = activation

    def forward(self, inputs):
        x = torch.matmul(inputs, inputs.transpose(-2, -1))
        return x

class Conv2D(nn.Module):
    def __init__(self, input_channels, output_dim, kernel_height, stride_height=1, stride_width=1):
        super(Conv2D, self).__init__()
        self.conv = nn.Conv2d(input_channels, output_dim, (1, kernel_height), stride=(stride_height, stride_width))

    def forward(self, x):
        return self.conv(x)
    
class Conv1D(nn.Module):
    def __init__(self, input_channels, output_dim, filter_size, stride=(1,1), padding='same'):
        super(Conv1D, self).__init__()
        self.conv = nn.Conv1d(input_channels, output_dim, filter_size, stride=stride, padding=padding)

    def forward(self, x):
        return self.conv(x)

class EdgeToEdgeConv2D(nn.Module):
    def __init__(self, input_channels, output_dim, kernel_size, stride=(1,1), padding='same'):
        super(EdgeToEdgeConv2D, self).__init__()
        self.conv1 = nn.Conv2d(input_channels, output_dim, (1, kernel_size), stride=stride, padding=padding)
        #conv2_weight = self.conv1.weight.data.permute(1, 0, 2, 3).contiguous()
        self.conv2 = nn.Conv2d(input_channels, output_dim, kernel_size=(kernel_size, kernel_size), stride=stride, padding=padding)

    def forward(self, x):
        c1 = self.conv1(x)
        c2 = self.conv2(x)
        return  c1 + c2

class EdgeToNodeConv2D(nn.Module):
    def __init__(self, input_channels, output_dim, kernel_height, stride=1, padding='valid'):
        super(EdgeToNodeConv2D, self).__init__()
        self.conv = nn.Conv2d(input_channels, output_dim, (1, kernel_height), stride=stride, padding=padding)

    def forward(self, x):
        return self.conv(x)

class NodeToGraphConv2D_Adj(nn.Module):
    def __init__(self, input_channels, output_dim):
        super(NodeToGraphConv2D_Adj, self).__init__()
        self.conv = nn.Conv2d(input_channels, output_dim, (1, 1))  # Adjust kernel size as needed

    def forward(self, x):
        return self.conv(x)

class DeconvE2N(nn.Module):
    def __init__(self, input_channels, output_dim, kernel_height, stride=1, padding='valid'):
        super(DeconvE2N, self).__init__()
        self.deconv1 = nn.ConvTranspose2d(input_channels, output_dim, (1, kernel_height), stride=stride, padding=0)
        self.deconv2 = nn.ConvTranspose2d(input_channels, output_dim, (kernel_height, 1), stride=stride, padding=0)
        self.bias = nn.Parameter(torch.zeros(output_dim))

    def forward(self, x):
        x1 = self.deconv1(x)
        x2 = self.deconv2(x.transpose(2, 3))
        return torch.add(x1 + self.bias, x2 + self.bias)  
    
class DeconvN2G(nn.Module):
    def __init__(self, input_channels, output_dim):
        super(DeconvN2G, self).__init__()
        self.deconv = nn.ConvTranspose2d(input_channels, output_dim, (1, 1))  # Adjust kernel size as needed

    def forward(self, x):
        return self.deconv(x)

class DeconvN2N(nn.Module):
    def __init__(self, input_channels, output_dim, kernel_height, stride=1):
        super(DeconvN2N, self).__init__()
        self.deconv = nn.ConvTranspose2d(input_channels, output_dim, (1, kernel_height), stride=stride)

    def forward(self, x):
        return self.deconv(x)

class DeconvE2E(nn.Module):
    def __init__(self, input_channels, output_dim, kernel_size, stride=1, padding='valid'):
        super(DeconvE2E, self).__init__()
        self.deconv1 = nn.ConvTranspose2d(input_channels, output_dim, (1, kernel_size), stride=stride, padding=0)
        self.deconv2 = nn.ConvTranspose2d(input_channels, output_dim, (kernel_size, 1), stride=stride, padding=0)
        self.bias = nn.Parameter(torch.zeros(output_dim))

    def forward(self, x):
        input_1 = torch.sum(x, dim=1, keepdim=True).permute(0, 1, 3, 2)
        input_2 = torch.sum(x, dim=2, keepdim=True)

        out1 = self.deconv1(input_1)
        out2 = self.deconv2(input_2)

        out1 = out1 + self.bias.view(1, -1, 1, 1)
        out2 = out2 + self.bias.view(1, -1, 1, 1)
        
        return (out1 + out2) / 2

class Linear(nn.Module):
    def __init__(self, input_size, output_size, stddev=0.02, bias_start=0.0, with_w=False):
        super(Linear, self).__init__()

        self.linear = nn.Linear(input_size, output_size)
        nn.init.normal_(self.linear.weight, mean=0.0, std=stddev)
        nn.init.constant_(self.linear.bias, bias_start)

    def forward(self, x):
        output = self.linear(x)
        return output

class BatchNorm(nn.Module):
    def __init__(self, num_features, epsilon=1e-5, momentum=0.9):
        super(BatchNorm, self).__init__()
        self.batch_norm = nn.BatchNorm1d(num_features=num_features, eps=epsilon, momentum=momentum)

    def forward(self, x):
        return self.batch_norm(x)

class BatchNorm2D(nn.Module):
    def __init__(self, num_features, epsilon=1e-5, momentum=0.9):
        super(BatchNorm2D, self).__init__()
        self.batch_norm = nn.BatchNorm2d(num_features=num_features, eps=epsilon, momentum=momentum)

    def forward(self, x):
        return self.batch_norm(x)
    
def glorot_init(tensor):
    if isinstance(tensor, nn.Parameter):
        tensor = tensor.data
    input_dim = tensor.size(0)
    output_dim = tensor.size(1)
    init_range = np.sqrt(6.0 / (input_dim + output_dim))
    torch.nn.init.uniform_(tensor, -init_range, init_range)