# Scalable Generation of Heterogeneous Spatio-Temporal Graphs: Bridging Graph Neural Networks and Traffic Scenario Generation

**Felix Häusle, 2025**

## Structure
* **`framework`:** Initial, unfinished draft of a unified framework for loading and evaluation generative graph models
* **`traffic_model`:** The actual graph diffusion model presented in this work

## Usage
### Training
Change config file: in folder `config` different datasets have been tested, each has its own file. Desired file should carry the name `config_train.yaml` (rename it correspondingly).

#### Config: Training latent VAE
Config file must be adapted to:
```python
model.type = 'latent_variationalautoencoder'
model.from_checkpoint = False
train.training = True
train.name = 'train'
```

#### Config: Training latent diffusion model
Config file must be adapted to:
```python
model.type = 'latent_diffusionmodel'
model.from_checkpoint = False
train.training = True
train.name = 'train'
```

**Important: before training a latent diffusionmodel, a latent VAE is necessary to be trained.**

#### Start training
```bash
python3 traffic_model/main.py
```

### Evaluation
#### Config: Evaluate latent diffusion model
```python
model.type = 'latent_diffusionmodel'
model.from_checkpoint = True
train.training = False
train.name = 'test'
```

**Important: both VAE and Diffusion model need to be saved in the folder `checkpoints`

#### Run evaluation
```bash
python3 traffic_model/main.py
```
### Checkpoints
Checkpoints from training runs during the thesis are located at `/data/models/thesis_unqwo/checkpoints`.

## To Do
* [ ] Replace all absolute paths (`/home/ws/unqwo/*`) to match your project match
    * Input data from `master-thesis/data` and `masterthesis/traffic_model/datasets` were moved to `/data`
    * Checkpoints were moved from `/home/ws/unqwo/checkpoints` to `/data/models/thesis_unqwo`.